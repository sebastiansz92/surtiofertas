import {
  AddCarProductRequestData,
  ShoppingCartDataState,
} from '../../../entity/structure/category-product.model';

export interface ShoppingCartInterface {
  addProductToCar(request: AddCarProductRequestData): void;
  getAllProductsInCar(): Promise<any>;
  setShopppinCartData(data: ShoppingCartDataState): void;
  removeProductInCar(productId: number): Promise<any>;
  removeAllProductsInCar(): Promise<any>;
}
