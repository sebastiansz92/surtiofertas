import {
  BusinessCategoryDataFormated,
  CategorySupperTabs,
  GroupedProductsInBusinessCategoryFormated,
} from '../../../entity/structure/category-product.model';

export interface CategoryProductInterface {
  getProductsGroupedBySupperCategory(): Promise<any>;
  // addOrRemoveFavoriteProduct(product: AddOrRemoveFavoriteRequest): Promise<any>;
  // getFavoriteProductList(): Promise<any>;
  updateTabsBusinessCategory(tabsData: CategorySupperTabs[]): void;
  updateCategorySupperData(
    businessCategoryData: BusinessCategoryDataFormated[]
  ): void;
  updateOffsetSectionData(offsetData: Array<any>): void;
  getAllCategoriesProduct(): void;
  getProductsByCategory(category: number): Promise<any>;
  getProductsBySearch(search: string): Promise<any>;
  updateProductsByCategory(
    businessCategoryData: GroupedProductsInBusinessCategoryFormated[]
  ): void;
  updateProductsBySearch(
    businessCategorySearchData: GroupedProductsInBusinessCategoryFormated[]
  ): void;
  setInputValue(key: string, value: string): void;
}
