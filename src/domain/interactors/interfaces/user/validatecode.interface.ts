import {RecoveryRequest} from '../../entity/structure/recovery.model';
import {ValidateCodeRequest} from '../../entity/structure/validate-code.model';

export interface ValidateCodeInterface {
  setFocusStyle(iconBg: string, input: string): void;
  validateCodeUser(user: ValidateCodeRequest): void;
  setErrorValidation(error: string | undefined): void;
  setValueInput(key: string, value: string): void;
  recoveryUser(request: RecoveryRequest): void;
  asignNextPinInputFocus(nextIndex: number, pinValueString: string): void;
}
