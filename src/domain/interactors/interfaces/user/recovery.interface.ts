import {RecoveryRequest} from '../../entity/structure/recovery.model';

export interface RecoveryInterface {
  setFocusStyle(iconBg: string, input: string): void;
  recoveryUser(user: RecoveryRequest): void;
  setErrorValidation(error: string | undefined): void;
  setValueInput(key: string, value: string): void;
}
