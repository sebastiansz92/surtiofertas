import {SignupRequest} from '../../entity/structure/signup.model';

export interface SingupInterface {
  setFocusStyle(iconBg: string, input: string): void;
  signupUser(user: SignupRequest): void;
  setErrorValidation(error: string | undefined): void;
  setValueInput(key: string, value: string): void;
}
