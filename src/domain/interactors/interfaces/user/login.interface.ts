import {LoginRequest} from '../../../entity/structure/user.model';

export interface LoginInterface {
  setFocusStyle(iconBg: string, input: string): void;
  loginUser(user: LoginRequest): void;
  setErrorValidation(error: string | undefined): void;
  setInputValue(key: string, value: string): void;
  setUserToken(token: string | undefined): void;
}
