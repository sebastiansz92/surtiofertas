import {UpdatePasswordRequest} from '../../entity/structure/update-password.model';

export interface UpdatePasswordInterface {
  setFocusStyle(iconBg: string, input: string): void;
  updatePasswordUser(updateInfo: UpdatePasswordRequest): void;
  setErrorValidation(error: string | undefined): void;
  setInputValue(key: string, value: string): void;
}
