import {GroupedProductsInBusinessCategoryFormated} from '../../../entity/structure/category-product.model';
import {
  CancelInvoiceRequest,
  GenerateInvoiceRequest,
} from '../../../entity/structure/invoice.model';

export interface InvoiceInterface {
  generateInvoice(invoiceRequest: GenerateInvoiceRequest): void;
  getPendingInvoicesByUser(): void;
  getRecentOrdersProductsByUser(): void;
  updateRecentOrdersProductsByUser(
    recentOrdersProductsByUserData: GroupedProductsInBusinessCategoryFormated[]
  ): void;
  preInvoice(): void;
  getInvoiceDetailsByOrder(orderId: number): void;
  cancelInvoice(invoiceRequest: CancelInvoiceRequest): void;
}
