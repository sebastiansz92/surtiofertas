import {LatLng} from 'react-native-maps';
import {AddressValues} from '../../../entity/structure/address.model';

export interface AddressInterface {
  getDefaultUserAddress(): void;
  getAddressesByUser(): void;
  setDefaultUserAddress(addressId: number): void;
  setDefaultAddress(position: LatLng | undefined): void;
  resetCurrentState(): void;
  resetErrorValidationState(): void;
  updateAddress(address: AddressValues): void;
  setErrorValidation(error: string): void;
  disabledAddress(id:number):void;
}
