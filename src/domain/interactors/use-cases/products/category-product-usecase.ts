import {
  BusinessCategoryData,
  BusinessCategoryDataFormated,
  CategorySupperTabs,
} from './../../../entity/structure/category-product.model';
import {HTTP_STATUS} from '../../../../data/constants/api.constants';
import {CategoryProductInterface} from '../../interfaces/product/category-product.interface';

export interface CategoryProductInteractor {
  getProductsGroupedBySupperCategory(): void;
  getAllCategoriesProduct(): void;
  setSelectedTab(index: number, categoryTabs: CategorySupperTabs[]): void;
  setOffsetData(offsetData: Array<any>): void;
  getProductsByCategory(category: number): void;
  getProductsBySearch(search: string): void;
  onChangeInputText(key: string, value: string): void;
}

export function createCategoryProductInteractor(
  categoryProductInterface: CategoryProductInterface
): CategoryProductInteractor {
  return {
    async getProductsGroupedBySupperCategory(): Promise<void> {
      const responseWsData = await categoryProductInterface.getProductsGroupedBySupperCategory();
      if (responseWsData.payload.status === HTTP_STATUS.OK) {
        const tabsData = responseWsData.payload.responseData.map(
          (elm: BusinessCategoryData, index: number) => {
            return {
              title: elm.supperCategoryName,
              data: [],
              index: index,
              selected: index === 0 ? true : false,
            };
          }
        );
        categoryProductInterface.updateTabsBusinessCategory(tabsData);

        const categorySupperData: BusinessCategoryDataFormated[] = responseWsData.payload.responseData.map(
          (elm: BusinessCategoryData) => {
            return {
              offset: 0,
              supperCategoryName: elm.supperCategoryName,
              productsGroupedBy: Object.keys(elm.productsGroupedBy).map(
                (category) => {
                  return {
                    categoryName: category,
                    categoryContent: elm.productsGroupedBy[category],
                  };
                }
              ),
            };
          }
        );
        categoryProductInterface.updateCategorySupperData(categorySupperData);
      }
    },

    setSelectedTab(position: number, categoryTabs: CategorySupperTabs[]): void {
      categoryTabs = categoryTabs.map((elm, index) => {
        return {
          ...elm,
          selected: index === position ? true : false,
        };
      });
      categoryProductInterface.updateTabsBusinessCategory(categoryTabs);
    },

    setOffsetData(offsetData: Array<any>) {
      categoryProductInterface.updateOffsetSectionData(offsetData);
    },
    getAllCategoriesProduct(): void {
      categoryProductInterface.getAllCategoriesProduct();
    },
    async getProductsByCategory(category: number): Promise<void> {
      const responseWsData = await categoryProductInterface.getProductsByCategory(
        category
      );
      if (responseWsData.payload.status === HTTP_STATUS.OK) {
        const categoryKeys = Object.keys(responseWsData.payload.responseData);
        const categoryData = formatResponseCategory(
          categoryKeys,
          responseWsData
        );
        categoryProductInterface.updateProductsByCategory(categoryData);
      }
    },
    async getProductsBySearch(search: string): Promise<void> {
      const responseWsData = await categoryProductInterface.getProductsBySearch(
        search
      );
      if (responseWsData.payload.status === HTTP_STATUS.OK) {
        const categoryKeys = Object.keys(responseWsData.payload.responseData);
        const categorySearchData = formatResponseCategory(
          categoryKeys,
          responseWsData
        );
        categoryProductInterface.updateProductsBySearch(categorySearchData);
      }
    },
    onChangeInputText(key: string, value: string): void {
      categoryProductInterface.setInputValue(key, value);
    },
  };
}

const formatResponseCategory = (
  categoryKeys: Array<any>,
  responseWsData: any
) => {
  return categoryKeys.map((elm) => {
    return {
      categoryName: elm,
      categoryContent: responseWsData.payload.responseData[elm],
    };
  });
};
