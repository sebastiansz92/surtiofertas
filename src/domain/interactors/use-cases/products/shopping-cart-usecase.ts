import store from '../../../../data/redux/store';
import ProductEntity from '../../../entity/model/ProductEntity';
import {
  AddCarProductRequestData,
  ProductDetails,
} from '../../../entity/structure/category-product.model';
import {ShoppingCartInterface} from '../../interfaces/product/shopping-cart.interface';

const productEntity = new ProductEntity();

export interface ShoppingCartInteractor {
  addProductToCar(request: AddCarProductRequestData): void;
  getAllProductsInCar(): Promise<void>;
  removeProductInCar(productId: number): Promise<void>;
  removeAllProductsInCar(): void;
}

export function createShoppingCartInteractor(
  shoppingCartInterface: ShoppingCartInterface
): ShoppingCartInteractor {
  return {
    addProductToCar(request: AddCarProductRequestData): void {
      shoppingCartInterface.addProductToCar(request);
    },

    async removeAllProductsInCar(): Promise<void> {
      const actionResult = await shoppingCartInterface.removeAllProductsInCar();
      if (actionResult.payload) {
        shoppingCartInterface.setShopppinCartData({
          subtotalProductsInCar: 0,
          products: [],
        });
      }
    },

    async getAllProductsInCar(): Promise<void> {
      let response = await shoppingCartInterface.getAllProductsInCar();
      if (response.payload) {
        let products: ProductDetails[] = response.payload.data;
        let subtotalProductsInCar = productEntity.getSubtotalValue(products);
        shoppingCartInterface.setShopppinCartData({
          subtotalProductsInCar,
          products,
        });
      }
    },

    async removeProductInCar(productId: number): Promise<void> {
      const actionResult = await shoppingCartInterface.removeProductInCar(
        productId
      );
      if (actionResult.payload) {
        let productsInShoppingCart = store
          .getState()
          .shoppingCart?.shoppingCartData?.products.map((elm) => {
            return elm;
          });
        productsInShoppingCart?.splice(
          productsInShoppingCart?.map((elm) => elm.id).indexOf(productId),
          1
        );
        let subtotalProductsInCar = productsInShoppingCart
          ? productEntity.getSubtotalValue(productsInShoppingCart)
          : 0;
        productsInShoppingCart &&
          shoppingCartInterface.setShopppinCartData({
            subtotalProductsInCar,
            products: productsInShoppingCart,
          });
      }
    },
  };
}
