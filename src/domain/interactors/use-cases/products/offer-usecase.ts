import {OfferInterface} from '../../interfaces/product/offer.interface';
export interface OfferInteractor {
  getMarketOffers(): void;
}

export function createOfferInteractor(
  offerInterface: OfferInterface
): OfferInteractor {
  return {
    getMarketOffers(): void {
      offerInterface.getMarketOffers();
    },
  };
}
