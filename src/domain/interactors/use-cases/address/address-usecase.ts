import {AddressInterface} from '../../interfaces/address/address.interface';
import {LatLng} from 'react-native-maps';
import AutoCompletePlaces from '../../../../data/address/AutocompletePlaces.data';
import {AddressValues} from '../../../entity/structure/address.model';
import ValidatorEntity from '../../../entity/model/ValidatorEntity';
import AddressEntity from '../../../entity/model/AddressEntity';

const validator = new ValidatorEntity();

const addressEntity = new AddressEntity(validator);

const geolocationApi = new AutoCompletePlaces();

export interface AddressInteractor {
  getDefaultUserAddress(): void;
  getAddressesByUser(): void;
  setDefaultAddressByUser(addressId: number): void;
  getCurrentPosition(): void;
  getAddressByCoords(position: LatLng): void;
  resetCurrentState(): void;
  updateAddress(address: AddressValues): void;
  resetErrorValidationState(): void;
  disabledAddress(id: number | undefined): void;
}

export function createAddressInteractor(
  addressInterface: AddressInterface
): AddressInteractor {
  return {
    getDefaultUserAddress(): void {
      addressInterface.getDefaultUserAddress();
    },
    async getAddressesByUser(): Promise<any> {
      addressInterface.getAddressesByUser();
    },
    setDefaultAddressByUser(addressId: number): void {
      addressInterface.setDefaultUserAddress(addressId);
    },
    async getCurrentPosition(): Promise<void> {
      const resultQueryPosition = await geolocationApi
        .getCurrentPosition()
        .then((result) => {
          return result;
        });
      if (resultQueryPosition.status === 'OK') {
        addressInterface.setDefaultAddress(resultQueryPosition.position);
      }
    },
    getAddressByCoords(position: LatLng): void {
      addressInterface.setDefaultAddress(position);
    },

    resetCurrentState(): void {
      addressInterface.resetCurrentState();
    },

    updateAddress(address: AddressValues): void {
      const error = addressEntity.validateAddressForm(address);
      if (error) {
        addressInterface.setErrorValidation(error);
      } else {
        if (address.details === '') {
          delete address.details;
        }
        addressInterface.updateAddress(address);
      }
    },

    resetErrorValidationState(): void {
      addressInterface.resetErrorValidationState();
    },

    disabledAddress(id: number): void {
      if (id) {
        addressInterface.disabledAddress(id);
      }
    },
  };
}
