import {
  CancelInvoiceRequest,
  GenerateInvoiceRequest,
} from '../../../entity/structure/invoice.model';
import {InvoiceInterface} from '../../interfaces/invoice/invoice.interface';

export interface InvoiceInteractor {
  generateInvoice(invoiceRequest: GenerateInvoiceRequest): void;
  getPendingInvoicesByUser(): void;
  getRecentOrdersProductsByUser(): void;
  preInvoice(): void;
  getInvoiceDetailsByOrder(orderId: number): void;
  cancelInvoice(invoiceRequest: CancelInvoiceRequest): void;
}

export function createInvoiceInteractor(
  invoiceInterface: InvoiceInterface
): InvoiceInteractor {
  return {
    generateInvoice(invoiceRequest: GenerateInvoiceRequest): void {
      invoiceInterface.generateInvoice(invoiceRequest);
    },
    getPendingInvoicesByUser(): void {
      invoiceInterface.getPendingInvoicesByUser();
    },
    getRecentOrdersProductsByUser(): void {
      invoiceInterface.getRecentOrdersProductsByUser();
    },

    preInvoice(): void {
      invoiceInterface.preInvoice();
    },

    getInvoiceDetailsByOrder(orderId: number): void {
      invoiceInterface.getInvoiceDetailsByOrder(orderId);
    },
    cancelInvoice(invoiceRequest: CancelInvoiceRequest): void {
      invoiceInterface.cancelInvoice(invoiceRequest);
    },
  };
}
