import StorageData from '../../../../data/StorageData';
import LoginEntity from '../../../entity/model/LoginEntity';
import ValidatorEntity from '../../../entity/model/ValidatorEntity';
import {LoginRequest} from '../../../entity/structure/user.model';
import {LoginInterface} from '../../interfaces/user/login.interface';

const asyncStorage = new StorageData();
const validator = new ValidatorEntity();
const loginEntity = new LoginEntity(validator, asyncStorage);

export interface LoginInteractor {
  loginUser(user: LoginRequest): void;
  setFocusStyle(key: string): void;
  setInputIconColorOnBlur(
    key: string,
    currentInputValue: string | undefined
  ): void;
  onChangeInputText(key: string, value: string): void;
  resetErrorValidations(): void;
  getCurrentUserToken(): Promise<string | undefined>;
}

export function createLoginInteractor(
  loginInterface: LoginInterface
): LoginInteractor {
  return {
    loginUser(user: LoginRequest) {
      const error = loginEntity.validateLoginForm(user);
      if (error) {
        loginInterface.setErrorValidation(error);
      } else {
        loginInterface.loginUser(user);
      }
    },
    setFocusStyle(key: string): void {
      loginInterface.setFocusStyle('#005fab', key);
    },
    setInputIconColorOnBlur(
      key: string,
      currentInputValue: string | undefined
    ): void {
      if (currentInputValue === undefined || currentInputValue === '') {
        loginInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      } else {
        loginInterface.setFocusStyle('#005fab', `${key}IconBg`);
      }
    },
    onChangeInputText(key: string, value: string): void {
      loginInterface.setInputValue(key, value);
      if (value !== '' && value !== undefined) {
        loginInterface.setFocusStyle('#005fab', `${key}IconBg`);
      } else {
        loginInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      }
    },
    resetErrorValidations(): void {
      loginInterface.setErrorValidation(undefined);
    },
    getCurrentUserToken(): Promise<string | undefined> {
      const userToken = loginEntity.getUserToken();

      userToken.then((user) => {
        loginInterface.setUserToken(user);
      });
      return userToken;
    },
  };
}
