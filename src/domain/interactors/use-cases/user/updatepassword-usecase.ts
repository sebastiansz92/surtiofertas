import UpdatePasswordEntity from '../../../entity/model/UpdatePasswordEntity';
import ValidatorEntity from '../../../entity/model/ValidatorEntity';
import {UpdatePasswordRequest} from '../../../entity/structure/update-password.model';
import {UpdatePasswordInterface} from '../../interfaces/user/updatepassword.interface';

const validator = new ValidatorEntity();
const updatePasswordEntity = new UpdatePasswordEntity(validator);

export interface UpdatePasswordInteractor {
  updatePasswordUser(user: UpdatePasswordRequest): void;
  setFocusStyle(key: string): void;
  setInputIconColorOnBlur(
    key: string,
    currentInputValue: string | undefined
  ): void;
  onChangeInputText(key: string, value: string): void;
  resetErrorValidations(): void;
}

export function createUpdatePasswordInteractor(
  updatePasswordInterface: UpdatePasswordInterface
): UpdatePasswordInteractor {
  return {
    updatePasswordUser(updatePasswordRequest: UpdatePasswordRequest) {
      const error = updatePasswordEntity.validateUpdatePasswordForm(
        updatePasswordRequest
      );
      if (error) {
        updatePasswordInterface.setErrorValidation(error);
      } else {
        updatePasswordInterface.updatePasswordUser(updatePasswordRequest);
      }
    },
    setFocusStyle(key: string): void {
      updatePasswordInterface.setFocusStyle('#005fab', key);
    },
    setInputIconColorOnBlur(
      key: string,
      currentInputValue: string | undefined
    ): void {
      if (currentInputValue === undefined || currentInputValue === '') {
        updatePasswordInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      } else {
        updatePasswordInterface.setFocusStyle('#005fab', `${key}IconBg`);
      }
    },
    onChangeInputText(key: string, value: string): void {
      updatePasswordInterface.setInputValue(key, value);
      if (value !== '' && value !== undefined) {
        updatePasswordInterface.setFocusStyle('#005fab', `${key}IconBg`);
      } else {
        updatePasswordInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      }
    },
    resetErrorValidations(): void {
      updatePasswordInterface.setErrorValidation(undefined);
    },
  };
}
