import RecoveryEntity from '../../../entity/model/RecoveryEntity';
import ValidatorEntity from '../../../entity/model/ValidatorEntity';
import {RecoveryRequest} from '../../../entity/structure/recovery.model';
import {RecoveryInterface} from '../../interfaces/user/recovery.interface';

const validator = new ValidatorEntity();
const recoveryEntity = new RecoveryEntity(validator);

export interface RecoveryInteractor {
  recoveryUser(user: RecoveryRequest): void;
  setFocusStyle(key: string): void;
  setInputIconColorOnBlur(
    key: string,
    currentInputValue: string | undefined
  ): void;
  onChangeInputText(key: string, value: string): void;
  resetErrorValidations(): void;
}

export function createRecoveryInteractor(
  recoveryInterface: RecoveryInterface
): RecoveryInteractor {
  return {
    recoveryUser(user: RecoveryRequest): void {
      const error = recoveryEntity.validateRecoveryForm(user);
      if (error) {
        recoveryInterface.setErrorValidation(error);
      } else {
        //metodo
        recoveryInterface.recoveryUser(user);
      }
    },
    setFocusStyle(key: string): void {
      recoveryInterface.setFocusStyle('#005fab', key);
    },
    setInputIconColorOnBlur(
      key: string,
      currentInputValue: string | undefined
    ): void {
      if (currentInputValue === undefined || currentInputValue === '') {
        recoveryInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      } else {
        recoveryInterface.setFocusStyle('#005fab', `${key}IconBg`);
      }
    },
    onChangeInputText(key: string, value: string): void {
      recoveryInterface.setValueInput(key, value);
      if (value !== '' && value !== undefined) {
        recoveryInterface.setFocusStyle('#005fab', `${key}IconBg`);
      } else {
        recoveryInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      }
    },
    resetErrorValidations(): void {
      recoveryInterface.setErrorValidation(undefined);
    },
  };
}
