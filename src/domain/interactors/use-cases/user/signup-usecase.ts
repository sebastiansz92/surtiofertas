import SignupEntity from '../../../entity/model/SignupEntity';
import ValidatorEntity from '../../../entity/model/ValidatorEntity';
import {SingupInterface} from '../../interfaces/user/signup.interface';

const validator = new ValidatorEntity();
const signupEntity = new SignupEntity(validator);

export interface SignupInteractor {
  signupUser(
    email: string,
    password: string,
    phone: string,
    name: string,
    passwordConfirm: string
  ): void;
  setFocusStyle(key: string): void;
  setInputIconColorOnBlur(
    key: string,
    currentInputValue: string | undefined
  ): void;
  onChangeInputText(key: string, value: string): void;
  resetErrorValidations(): void;
}

export function createSignupInteractor(
  signupInterface: SingupInterface
): SignupInteractor {
  return {
    signupUser(
      email: string,
      password: string,
      phone: string,
      name: string,
      passwordConfirm: string
    ): void {
      const error = signupEntity.validateSignupForm(
        email,
        password,
        phone,
        name,
        passwordConfirm
      );
      if (error) {
        signupInterface.setErrorValidation(error);
      } else {
        signupInterface.signupUser({
          email: email,
          password: password,
          phone: phone,
          fullname: name,
        });
      }
    },
    setFocusStyle(key: string): void {
      signupInterface.setFocusStyle('#005fab', key);
    },
    setInputIconColorOnBlur(
      key: string,
      currentInputValue: string | undefined
    ): void {
      if (currentInputValue === undefined || currentInputValue === '') {
        signupInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      } else {
        signupInterface.setFocusStyle('#005fab', `${key}IconBg`);
      }
    },
    onChangeInputText(key: string, value: string): void {
      signupInterface.setValueInput(key, value);
      if (value !== '' && value !== undefined) {
        signupInterface.setFocusStyle('#005fab', `${key}IconBg`);
      } else {
        signupInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      }
    },
    resetErrorValidations(): void {
      signupInterface.setErrorValidation(undefined);
    },
  };
}
