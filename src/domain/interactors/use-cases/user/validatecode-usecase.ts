import ValidateCodeEntity from '../../../entity/model/ValidateCodeEntity';
import ValidatorEntity from '../../../entity/model/ValidatorEntity';
import {RecoveryRequest} from '../../../entity/structure/recovery.model';
import {
  PinValue,
  ValidateCodeRequest,
} from '../../../entity/structure/validate-code.model';
import {ValidateCodeInterface} from '../../interfaces/user/validatecode.interface';

const validator = new ValidatorEntity();
const validateCodeEntity = new ValidateCodeEntity(validator);

export interface ValidateCodeInteractor {
  validateCodeUser(request: ValidateCodeRequest): void;
  sendAgainCode(request: RecoveryRequest): void;
  setFocusStyle(key: string): void;
  setInputIconColorOnBlur(
    key: string,
    currentInputValue: string | undefined
  ): void;
  onChangeInputText(key: string, value: string): void;
  resetErrorValidations(): void;
  onChangePinValueIndex(
    value: string,
    pinIndex: number,
    pinValue: PinValue
  ): void;
}

export function createValidateCodeInteractor(
  validateCodeInterface: ValidateCodeInterface
): ValidateCodeInteractor {
  return {
    validateCodeUser(request: ValidateCodeRequest): void {
      const error = validateCodeEntity.validateValidateCodeForm(request);
      if (error) {
        validateCodeInterface.setErrorValidation(error);
      } else {
        validateCodeInterface.validateCodeUser(request);
      }
    },
    sendAgainCode(request: RecoveryRequest): void {
      validateCodeInterface.recoveryUser(request);
    },
    setFocusStyle(key: string): void {
      validateCodeInterface.setFocusStyle('#005fab', key);
    },
    setInputIconColorOnBlur(
      key: string,
      currentInputValue: string | undefined
    ): void {
      if (currentInputValue === undefined || currentInputValue === '') {
        validateCodeInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      } else {
        validateCodeInterface.setFocusStyle('#005fab', `${key}IconBg`);
      }
    },
    onChangeInputText(key: string, value: string): void {
      validateCodeInterface.setValueInput(key, value);
      if (value !== '' && value !== undefined) {
        validateCodeInterface.setFocusStyle('#005fab', `${key}IconBg`);
      } else {
        validateCodeInterface.setFocusStyle('#bdbdbd', `${key}IconBg`);
      }
    },
    resetErrorValidations(): void {
      validateCodeInterface.setErrorValidation(undefined);
    },
    onChangePinValueIndex(
      value: string,
      pinIndex: number,
      pinValue: any
    ): void {
      validateCodeInterface.setValueInput(
        `pin${pinIndex}Value`,
        value.charAt(value.length - 1)
      );

      let pinValueString = '';
      let pinValueKeys = Object.keys(pinValue);
      for (const pinValueKey of pinValueKeys) {
        if (pinValue[pinValueKey] === '') {
          pinValue[pinValueKey] = value;
          pinValueString = pinValueString + pinValue[pinValueKey];
          break;
        } else {
          pinValueString = pinValueString + pinValue[pinValueKey];
        }
      }

      validateCodeInterface.asignNextPinInputFocus(
        pinIndex === 3 ? 0 : pinIndex + 1,
        pinValueString
      );
    },
  };
}
