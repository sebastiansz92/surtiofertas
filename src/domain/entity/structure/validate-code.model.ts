export interface ValidateCodeRequest {
  OTPCode: string;
}

export interface FocusStyleParam {
  iconBg: string;
  input: string;
}

export interface PinValue {
  pin0Value: string;
  pin1Value: string;
  pin2Value: string;
  pin3Value: string;
}

export interface ResponseOTPValidate {
  status: number;
  message: string;
  userId: string | undefined;
}
