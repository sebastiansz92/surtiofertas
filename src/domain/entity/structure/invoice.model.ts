import {AddressValues} from './address.model';
import {ProductDetails} from './category-product.model';
import {DateObjectFormat} from './common.model';
import {UserDetails} from './user.model';

export interface GenerateInvoiceRequest {
  deliveryMethodId: number;
  paymentMethodId: number;
}

export interface GenerateInvoiceResponse {
  responseData: any;
  status: number;
}

export interface DeliveryMethod {
  description: string;
  id: number;
  isDefault: boolean;
  name: string;
  price: number;
}

export interface PaymentMethod {
  description: string;
  id: number;
  isDefault: boolean;
  name: string;
}

export interface InvoiceProducts {
  finalPrice: number;
  id: number;
  ivapercent: string;
  product: ProductDetails;
  productQuantity: number;
}

export interface InvoiceState {
  createdByUser: number;
  createdDate: DateObjectFormat;
  current: boolean;
  id: number;
  lastModifiedByUser: string;
  lastModifiedDate: DateObjectFormat;
  state: InvoiceStateEnum;
}

export enum InvoiceStateEnum {
  RECEIVED = 'RECIBIDO',
  ACCEPTED = 'ACEPTADO',
  PACKING = 'EMPACANDO',
  DISPATCHING = 'DESPACHADO',
  DELIVERED = 'ENTREGADO',
}

export interface InvoiceDetails {
  address: AddressValues;
  createdByUser: string;
  createdDate: DateObjectFormat;
  deliveryCost: number;
  deliveryMethod: DeliveryMethod;
  id: number;
  invoiceDetails: InvoiceProducts[];
  lastModifiedByUser: string;
  lastModifiedDate: DateObjectFormat;
  paymentMethod: PaymentMethod;
  states: InvoiceState[];
  subtotal: number;
  taxes: number;
  user: UserDetails;
}

export interface PreInvoiceResponse {
  address?: string;
  deliveryCost?: number;
  deliveryMethods?: DeliveryMethod[];
  paymentMethods?: PaymentMethod[];
  products?: ProductDetails[];
  subtotal?: number;
  taxes?: number;
}

export interface CancelInvoiceRequest {
  invoiceId: number;
  event: string;
  comment: string;
}
