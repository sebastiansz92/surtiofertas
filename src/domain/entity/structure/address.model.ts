import {LatLng} from 'react-native-maps';

export interface GPSPosition {
  position?: LatLng;
  status: string;
  error?: string;
}

export interface AddressDecodeResponse {
  responseData: AddressValues;
  status: number;
}

export interface AddressValues {
  address?: string;
  details?: string;
  position?: LatLng;
  latitude?: number;
  longitude?: number;
  id?: number;
  isActive?: boolean;
  isDefault?: boolean;
  name?: string;
  direction?: string;
}

export interface AddressResponse {
  responseData: AddressValues;
  status?: number;
}
