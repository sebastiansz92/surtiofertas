export interface RecoveryRequest {
  username: string;
}

export interface FocusStyleParam {
  iconBg: string;
  input: string;
}
