export interface UpdatePasswordRequest {
  newPassword: string;
  repeatPasword: string;
  userId: number;
}
