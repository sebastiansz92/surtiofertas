import {Pageable} from './common.model';

export interface CategorySupperTabs {
  title: string;
  index: number;
  data: number[];
  selected: boolean;
}

export interface BusinessCategoryData {
  supperCategoryName: string;
  productsGroupedBy: GroupedProductsInBusinessCategory;
}

export interface GroupedProductsInBusinessCategory {
  [category: string]: CategoryDataProducts;
}

export interface CategoryDataProducts {
  content: ProductDetails[];
  pageable: Pageable;
  total: number;
}

export interface ProductDetails {
  baseprice?: number;
  categoryProductName?: string;
  categorySupperId?: number;
  categorySupperName?: string;
  description?: string;
  id?: number;
  image_url?: string;
  ivapercent?: string;
  name?: string;
  price?: number;
  priceOffered?: any;
  sku?: string;
  slug?: string;
  stock?: number;
  subcategoryProduct?: SubcategoryProduct;
  subcategoryProductName?: string;
  weight?: string;
  quantity?: number;
}

export interface SubcategoryProduct {
  id: number;
  name: string;
  description: string;
  categoryProduct: CategoryProduct;
}

export interface CategoryProduct {
  categoryimage64: string;
  description: string;
  id: number;
  name: string;
}

export interface BusinessCategoryDataFormated {
  supperCategoryName: string;
  productsGroupedBy: GroupedProductsInBusinessCategoryFormated[];
  offset: number;
}

export interface GroupedProductsInBusinessCategoryFormated {
  categoryName: string;
  categoryContent: CategoryDataProducts;
}

export interface AddCarProductRequestData {
  product: ProductDetails;
  quantity: number;
  rewrite?: boolean;
}

export interface ShoppingCartDataState {
  subtotalProductsInCar: number;
  products: ProductDetails[];
}
