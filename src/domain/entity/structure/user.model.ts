import {DateObjectFormat} from './common.model';

export interface LoginRequest {
  username: string;
  password: string;
}

export interface DialogMessage {
  title: {
    text: string;
    color: string;
  };
  message: string;
  button: {
    text: string;
    color: string;
  };
  pop: number;
}

export interface UserDetails {
  createdByUser: string;
  createdDate: DateObjectFormat;
  email: string;
  fullname: string;
  id: number;
  lastLoginDate: DateObjectFormat;
  lastModifiedByUser: string;
  lastModifiedDate: DateObjectFormat;
  password: string;
  phone: string;
}
