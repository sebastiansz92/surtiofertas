import {DateObjectFormat} from './common.model';

export interface ReduxOfferReponse {
  responseData: OffersResponse;
  status: number;
}

export interface OffersResponse {
  total: number;
  content: Array<OfferContent>;
}

export interface OfferContent {
  id: number;
  name: string;
  description: string;
  type: string;
  launchDate: DateObjectFormat;
  expirationDate: DateObjectFormat;
  imageUrl: string;
  discountValue: number;
  cumulative: boolean;
  productsOfferedGroupedBySubcategory: any;
}
