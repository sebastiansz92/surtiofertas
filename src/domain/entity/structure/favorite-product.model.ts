export interface AddOrRemoveFavoriteRequest {
  productId: number | undefined;
}
