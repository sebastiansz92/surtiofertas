export interface SignupRequest {
  email: string;
  password: string;
  phone: string;
  fullname: string;
}

export interface FocusStyleParam {
  iconBg: string;
  input: string;
}
