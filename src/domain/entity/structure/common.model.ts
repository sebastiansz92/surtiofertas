export interface KeyValue {
  key: string;
  value: string;
}

export interface DateObjectFormat {
  date: DateFormat;
  time: TimeFormat;
}

export interface DateFormat {
  year: number;
  month: number;
  day: number;
}

export interface TimeFormat {
  hour: number;
  minute: number;
  second: number;
  nano: number;
}

export interface Pageable {
  page: number;
  size: number;
  sort: {
    orders: Array<any>;
  };
}
