import {FORM_PATTERNS} from '../../../data/constants/forms.constants';
import {ERROR} from '../../../data/constants/messages.responses';
import ValidatorEntity from './ValidatorEntity';
import {RecoveryRequest} from '../structure/recovery.model';

class RecoveryEntity {
  private _validator: ValidatorEntity;
  constructor(validator: ValidatorEntity) {
    this._validator = validator;
  }

  validateRecoveryForm(user: RecoveryRequest): string | undefined {
    if (!user.username || user.username === '') {
      return ERROR.EMAIL_EMPTY;
    } else if (!this._validator.pattern(FORM_PATTERNS.EMAIL, user.username)) {
      return ERROR.EMAIL_INVALID;
    }
    return undefined;
  }
}

export default RecoveryEntity;
