import StorageData from '../../../data/StorageData';
import {FORM_PATTERNS} from '../../../data/constants/forms.constants';
import {ERROR} from '../../../data/constants/messages.responses';
import ValidatorEntity from './ValidatorEntity';
import {LoginRequest} from '../structure/user.model';

class LoginEntity {
  private _validator: ValidatorEntity;
  private _asyncStorage: StorageData;
  constructor(validator: ValidatorEntity, asyncStorage: StorageData) {
    this._validator = validator;
    this._asyncStorage = asyncStorage;
  }

  validateLoginForm(user: LoginRequest): string | undefined {
    if (!user.username || user.username === '') {
      return ERROR.EMAIL_EMPTY;
    } else if (!this._validator.pattern(FORM_PATTERNS.EMAIL, user.username)) {
      return ERROR.EMAIL_INVALID;
    } else if (!user.password || user.password === '') {
      return ERROR.PASSWORD_EMPTY;
    }
    return undefined;
  }

  getUserToken(): Promise<any> {
    return Promise.resolve(this._asyncStorage.getData('userToken'));
  }
}

export default LoginEntity;
