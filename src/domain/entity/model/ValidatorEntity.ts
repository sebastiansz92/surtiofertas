class ValidatorEntity {
  public minLength(min: number, length: number): boolean {
    return length >= min;
  }

  public maxLength(max: number, length: number): boolean {
    return length <= max;
  }

  public pattern(pattern: string, value: string): boolean {
    const regularExpression = new RegExp(pattern);
    return regularExpression.test(value);
  }
}

export default ValidatorEntity;
