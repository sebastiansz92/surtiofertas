import {ProductDetails} from '../structure/category-product.model';

class ProductEntity {
  formatPrice(price: number): string {
    return '$' + price?.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  getSubtotalValue(products: ProductDetails[]): number {
    let subtotalProductsInCar = 0;
    products?.map((product) => {
      if (
        product.quantity &&
        product.stock &&
        product.quantity > product.stock
      ) {
        subtotalProductsInCar =
          subtotalProductsInCar +
          (product.priceOffered ? product.priceOffered * product.stock : 0);
        return {
          ...product,
          quantity: product.stock,
        };
      } else {
        subtotalProductsInCar =
          subtotalProductsInCar +
          (product.priceOffered && product.quantity
            ? product.priceOffered * product.quantity
            : 0);
        return {
          ...product,
        };
      }
    });
    return subtotalProductsInCar;
  }
}

export default ProductEntity;
