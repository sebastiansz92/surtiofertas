import {ERROR} from '../../../data/constants/messages.responses';
import ValidatorEntity from './ValidatorEntity';
import {UpdatePasswordRequest} from '../structure/update-password.model';

class UpdatePasswordEntity {
  private _validator: ValidatorEntity;
  constructor(validator: ValidatorEntity) {
    this._validator = validator;
  }

  validateUpdatePasswordForm(user: UpdatePasswordRequest): string | undefined {
    if (!user.newPassword || user.newPassword === '') {
      return ERROR.PASSWORD_EMPTY;
    } else if (!user.repeatPasword || user.repeatPasword === '') {
      return ERROR.PASSWORD_CONFIRM_EMPTY;
    } else if (user.newPassword !== user.repeatPasword) {
      return ERROR.NO_MATCH_PASSWORD;
    }
    return undefined;
  }
}

export default UpdatePasswordEntity;
