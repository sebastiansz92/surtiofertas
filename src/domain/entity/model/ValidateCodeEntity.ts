import {ERROR} from '../../../data/constants/messages.responses';
import {ValidateCodeRequest} from '../structure/validate-code.model';
import ValidatorEntity from './ValidatorEntity';

class ValidateCodeEntity {
  private _validator: ValidatorEntity;
  constructor(validator: ValidatorEntity) {
    this._validator = validator;
  }

  validateValidateCodeForm(request: ValidateCodeRequest): string | undefined {
    if (!request.OTPCode || request.OTPCode === '') {
      return ERROR.CODE_EMPTY;
    } else if (isNaN(+request.OTPCode)) {
      return ERROR.CODE_INVALID;
    }
    return undefined;
  }
}

export default ValidateCodeEntity;
