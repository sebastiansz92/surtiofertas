import {format} from 'date-fns';
import {utcToZonedTime} from 'date-fns-tz';
import StorageData from '../../../data/StorageData';
import {DateObjectFormat} from '../structure/common.model';

class LayoutEntity {
  private _asyncStorage: StorageData;

  constructor(asyncStorage: StorageData) {
    this._asyncStorage = asyncStorage;
  }

  public getEnableTutorialStatus() {
    return Promise.resolve(this._asyncStorage.getData('enableTutorial'));
  }

  public setEnableTutorialStatus() {
    this._asyncStorage.storeData('enableTutorial', '1');
  }

  public formatDate(inputDate: DateObjectFormat | undefined): string {
    if (inputDate) {
      const date = new Date(
        inputDate.date.year,
        inputDate.date.month - 1,
        inputDate.date.day,
        inputDate.time.hour,
        inputDate.time.minute
      );
      const timeZone = 'UTC';
      const zonedDate = utcToZonedTime(date, timeZone);
      return format(zonedDate, 'MM/dd/yyyy hh:mm aaa');
    }
    return '';
  }
}

export default LayoutEntity;
