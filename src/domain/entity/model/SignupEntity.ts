import {FORM_PATTERNS} from '../../../data/constants/forms.constants';
import {ERROR} from '../../../data/constants/messages.responses';
import ValidatorEntity from './ValidatorEntity';

class SignupEntity {
  private _validator: ValidatorEntity;
  constructor(validator: ValidatorEntity) {
    this._validator = validator;
  }

  validateSignupForm(
    email: string,
    password: string,
    phone: string,
    name: string,
    passwordConfirm: string
  ): string | undefined {
    if (!name || name === '') {
      return ERROR.NAME_EMPTY;
    } else if (!phone || phone === '') {
      return ERROR.CELLPHONE_EMPTY;
    } else if (!this._validator.pattern(FORM_PATTERNS.CELLPHONE, phone)) {
      return ERROR.CELLPHONE_INVALID;
    } else if (!email || email === '') {
      return ERROR.EMAIL_EMPTY;
    } else if (!this._validator.pattern(FORM_PATTERNS.EMAIL, email)) {
      return ERROR.EMAIL_INVALID;
    } else if (!password || password === '') {
      return ERROR.PASSWORD_EMPTY;
    } else if (!passwordConfirm || passwordConfirm === '') {
      return ERROR.PASSWORD_CONFIRM_EMPTY;
    } else if (passwordConfirm !== password) {
      return ERROR.PASSWORD_EQUAL;
    }
    return undefined;
  }
}

export default SignupEntity;
