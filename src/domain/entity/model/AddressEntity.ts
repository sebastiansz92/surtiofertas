import {ERROR} from '../../../data/constants/messages.responses';
import ValidatorEntity from './ValidatorEntity';
import {AddressValues} from '../structure/address.model';

class AddressEntity {
  private _validator: ValidatorEntity;
  constructor(validator: ValidatorEntity) {
    this._validator = validator;
  }

  validateAddressForm(address: AddressValues): string | undefined {
    if (!address.direction || address.direction === '') {
      return ERROR.ADDRESS_EMPTY;
    } else if (!address.name || address.name === '') {
      return ERROR.NAME_EMPTY;
    }
    return undefined;
  }
}

export default AddressEntity;
