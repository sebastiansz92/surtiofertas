import {RecoveryRequest} from '../entity/structure/recovery.model';
import {SignupRequest} from '../entity/structure/signup.model';
import {UpdatePasswordRequest} from '../entity/structure/update-password.model';
import {LoginRequest} from '../entity/structure/user.model';
import {ValidateCodeRequest} from '../entity/structure/validate-code.model';

export interface UserRepository {
  loginUser(user: LoginRequest): Promise<Response>;
  signupUser(user: SignupRequest): Promise<Response>;
  recoveryPassword(user: RecoveryRequest): Promise<Response>;
  validateOTPCode(codeRecovery: ValidateCodeRequest): Promise<Response>;
  updatePassword(passwordInfo: UpdatePasswordRequest): Promise<Response>;
}
