import {LatLng} from 'react-native-maps';

export interface MapsRepository {
  getAddressByCoords(position: LatLng): Promise<Response>;
}
