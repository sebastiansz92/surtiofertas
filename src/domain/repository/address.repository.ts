import {AddressValues} from '../entity/structure/address.model';

export interface AddressRepository {
  getDefaultUserAddress(): Promise<Response>;
  getAddressesByUser(): Promise<Response>;
  setDefaultAddressByUser(addressId: number): Promise<Response>;
  updateAddressByUser(address: AddressValues): Promise<Response>;
  AddAddressByUser(address: AddressValues): Promise<Response>;
  disableAddressByUser(addressId: number): Promise<Response>;
}
