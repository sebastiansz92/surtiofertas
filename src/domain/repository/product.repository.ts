import {ProductDetails} from '../entity/structure/category-product.model';
import {AddOrRemoveFavoriteRequest} from '../entity/structure/favorite-product.model';

export interface ProductsRepository {
  getCurrentOffers(): Promise<Response>;
  getProductsBySupperCategory(): Promise<Response>;
  getAllCategoriesProduct(): Promise<Response>;
  getFavoriteProductsByUser(): Promise<Response>;
  addOrRemoveFavoriteProduct(
    product: AddOrRemoveFavoriteRequest
  ): Promise<Response>;
  validateFavorite(product: AddOrRemoveFavoriteRequest): Promise<Response>;
  getProductsByCategory(category: number): Promise<Response>;
  addProductToCar(product: ProductDetails, rewrite: boolean): Promise<Response>;
  getProductsInCart(): Promise<Response>;
  removeProductInCart(productId: number): Promise<Response>;
  removeAllProductsInCar(): Promise<Response>;
}
