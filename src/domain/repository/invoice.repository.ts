import {
  CancelInvoiceRequest,
  GenerateInvoiceRequest,
} from '../entity/structure/invoice.model';

export interface InvoiceRepository {
  generateInvoice(invoiceRequest: GenerateInvoiceRequest): Promise<Response>;
  getInvoicesByUser(): Promise<Response>;
  preInvoice(): Promise<Response>;
  getInvoiceDetailsById(orderId: number): Promise<Response>;
  cancelInvoice(invoiceRequest: CancelInvoiceRequest): Promise<Response>;
}
