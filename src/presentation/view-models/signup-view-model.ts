import {SignupState} from '../../data/redux/slices/user/signup.slice';
import {SignupInteractor} from '../../domain/interactors/use-cases/signup-usecase';

export interface SignupViewModel {
  email: string;
  emailIconBg: string | undefined;
  password: string;
  passwordIconBg: string | undefined;
  passwordConfirm: string;
  passwordConfirmIconBg: string | undefined;
  phone: string;
  phoneIconBg: string | undefined;
  name: string;
  nameIconBg: string | undefined;
  pendingSignup: boolean;
  errorValidation: string | undefined;
  userLogin: boolean;
  onFocusInput(signupInteractor: SignupInteractor, key: string): void;
  onBlurInput(
    signupInteractor: SignupInteractor,
    key: string,
    value: string | undefined
  ): void;
  onPressButtonSignup(signupInteractor: SignupInteractor): void;
  onChangeInput(
    signupInteractor: SignupInteractor,
    key: string,
    value: string
  ): void;
  resetValidation(signupInteractor: SignupInteractor): void;
}

export function createSignupViewModel(
  signupState: SignupState
): SignupViewModel {
  return {
    get email(): string {
      return signupState.email;
    },

    get emailIconBg(): string | undefined {
      return signupState.emailIconBg;
    },

    get password(): string {
      return signupState.password;
    },

    get passwordIconBg(): string | undefined {
      return signupState.passwordIconBg;
    },
    get passwordConfirm(): string {
      return signupState.passwordConfirm;
    },

    get passwordConfirmIconBg(): string | undefined {
      return signupState.passwordConfirmIconBg;
    },

    get phone(): string {
      return signupState.phone;
    },

    get phoneIconBg(): string | undefined {
      return signupState.phoneIconBg;
    },

    get name(): string {
      return signupState.name;
    },

    get nameIconBg(): string | undefined {
      return signupState.nameIconBg;
    },

    get pendingSignup(): boolean {
      return signupState.pendingSignup;
    },

    get errorValidation(): string | undefined {
      return signupState.errorValidation;
    },

    get userLogin(): boolean {
      return signupState.userLogin;
    },

    onFocusInput(signupInteractor: SignupInteractor, key: string): void {
      signupInteractor.setFocusStyle(`${key}IconBg`);
    },
    onBlurInput(
      signupInteractor: SignupInteractor,
      key: string,
      value: string | undefined
    ): void {
      signupInteractor.setInputIconColorOnBlur(key, value);
    },
    onPressButtonSignup(signupInteractor: SignupInteractor): void {
      signupInteractor.signupUser(
        this.email,
        this.password,
        this.phone,
        this.name,
        this.passwordConfirm
      );
    },
    onChangeInput(
      signupInteractor: SignupInteractor,
      key: string,
      value: string
    ): void {
      signupInteractor.onChangeInputText(key, value);
    },
    resetValidation(signupInteractor: SignupInteractor): void {
      signupInteractor.resetErrorValidations();
    },
  };
}
