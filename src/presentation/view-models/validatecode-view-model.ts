import {validateCodeState} from '../../data/redux/slices/user/validate-code.slice';
import {
  PinValue,
  ResponseOTPValidate,
} from '../../domain/entity/structure/validate-code.model';
import {ValidateCodeInteractor} from '../../domain/interactors/use-cases/validatecode-usecase';

export interface ValidateCodeViewModel {
  code: string;
  errorValidation: string | undefined;
  pin0Value: string;
  pin1Value: string;
  pin2Value: string;
  pin3Value: string;
  pinTarget: number;
  pinValue: string;
  recoveryResponse: ResponseOTPValidate | undefined;
  onFocusInput(
    validateCodeInteractor: ValidateCodeInteractor,
    key: string,
    currentValue: string
  ): void;
  onBlurInput(
    validateCodeInteractor: ValidateCodeInteractor,
    key: string,
    value: string | undefined
  ): void;
  onChangeInput(
    validateCodeInteractor: ValidateCodeInteractor,
    key: string,
    value: string
  ): void;
  onPressButtonValidateCode(
    validateCodeInteractor: ValidateCodeInteractor
  ): void;
  onPressButtonSendAgainCode(
    validateCodeInteractor: ValidateCodeInteractor,
    username: string
  ): void;
  resetValidation(validateCodeInteractor: ValidateCodeInteractor): void;
  getCurrentPinValue(index: number): string | undefined;
  onChangePinValueIndex(
    validateCodeInteractor: ValidateCodeInteractor,
    value: string,
    pinIndex: number
  ): void;
}

export function createValidateCodeViewModel(
  ValidateCodeState: validateCodeState
): ValidateCodeViewModel {
  return {
    get code(): string {
      return ValidateCodeState.code;
    },

    get errorValidation(): string | undefined {
      return ValidateCodeState.errorValidation;
    },

    get pin0Value(): string {
      return ValidateCodeState.pin0Value;
    },
    get pin1Value(): string {
      return ValidateCodeState.pin1Value;
    },
    get pin2Value(): string {
      return ValidateCodeState.pin2Value;
    },
    get pin3Value(): string {
      return ValidateCodeState.pin3Value;
    },

    get pinTarget(): number {
      return ValidateCodeState.pinTarget;
    },

    get pinValue(): string {
      return ValidateCodeState.pinValue;
    },

    get recoveryResponse(): ResponseOTPValidate | undefined {
      return ValidateCodeState.recoveryResponse;
    },

    onFocusInput(
      validateCodeInteractor: ValidateCodeInteractor,
      key: string
    ): void {
      validateCodeInteractor.setFocusStyle(`${key}IconBg`);
    },
    onBlurInput(
      validateCodeInteractor: ValidateCodeInteractor,
      key: string,
      value: string | undefined
    ): void {
      validateCodeInteractor.setInputIconColorOnBlur(key, value);
    },
    onChangeInput(
      validateCodeInteractor: ValidateCodeInteractor,
      key: string,
      value: string
    ): void {
      validateCodeInteractor.onChangeInputText(key, value);
    },
    onPressButtonValidateCode(
      validateCodeInteractor: ValidateCodeInteractor
    ): void {
      validateCodeInteractor.validateCodeUser({OTPCode: this.pinValue});
    },
    onPressButtonSendAgainCode(
      validateCodeInteractor: ValidateCodeInteractor,
      username: string
    ): void {
      validateCodeInteractor.sendAgainCode({username: username});
    },
    resetValidation(validateCodeInteractor: ValidateCodeInteractor): void {
      validateCodeInteractor.resetErrorValidations();
    },

    getCurrentPinValue(index: number): string | undefined {
      switch (index) {
        case 0:
          return this.pin0Value;
        case 1:
          return this.pin1Value;
        case 2:
          return this.pin2Value;
        case 3:
          return this.pin3Value;

        default:
          break;
      }
    },
    onChangePinValueIndex(
      validateCodeInteractor: ValidateCodeInteractor,
      value: string,
      pinIndex: number
    ): void {
      const pinValue: PinValue = {
        pin0Value: this.pin0Value,
        pin1Value: this.pin1Value,
        pin2Value: this.pin2Value,
        pin3Value: this.pin3Value,
      };
      validateCodeInteractor.onChangePinValueIndex(value, pinIndex, pinValue);
    },
  };
}
