import {RecoveryInteractor} from '../../domain/interactors/use-cases/recovery-usecase';
import {RecoveryState} from '../../data/redux/slices/user/recovery.slice';

export interface RecoveryViewModel {
  email: string;
  emailIconBg: string | undefined;
  errorValidation: string | undefined;
  recoveryMessageSend: boolean;
  recoveryMessage: string | undefined;
  onFocusInput(
    recoveryInteractor: RecoveryInteractor,
    key: string,
    currentValue: string
  ): void;
  onBlurInput(
    recoveryInteractor: RecoveryInteractor,
    key: string,
    value: string | undefined
  ): void;
  onChangeInput(
    recoveryInteractor: RecoveryInteractor,
    key: string,
    value: string
  ): void;
  onPressButtonRecovery(recoveryInteractor: RecoveryInteractor): void;
  resetValidation(recoveryInteractor: RecoveryInteractor): void;
}

export function createRecoveryViewModel(
  recoveryState: RecoveryState
): RecoveryViewModel {
  return {
    get email(): string {
      return recoveryState.email;
    },

    get emailIconBg(): string | undefined {
      return recoveryState.emailIconBg;
    },

    get errorValidation(): string | undefined {
      return recoveryState.errorValidation;
    },

    get recoveryMessageSend(): boolean {
      return recoveryState.recoveryMessageSend;
    },

    get recoveryMessage(): string | undefined {
      return recoveryState.recoveryMessage;
    },

    onFocusInput(recoveryInteractor: RecoveryInteractor, key: string): void {
      recoveryInteractor.setFocusStyle(`${key}IconBg`);
    },
    onBlurInput(
      recoveryInteractor: RecoveryInteractor,
      key: string,
      value: string | undefined
    ): void {
      recoveryInteractor.setInputIconColorOnBlur(key, value);
    },
    onChangeInput(
      recoveryInteractor: RecoveryInteractor,
      key: string,
      value: string
    ): void {
      recoveryInteractor.onChangeInputText(key, value);
    },
    onPressButtonRecovery(recoveryInteractor: RecoveryInteractor): void {
      recoveryInteractor.recoveryUser({
        username: this.email,
      });
    },
    resetValidation(recoveryInteractor: RecoveryInteractor): void {
      recoveryInteractor.resetErrorValidations();
    },
  };
}
