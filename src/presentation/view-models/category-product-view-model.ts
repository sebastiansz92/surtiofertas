import {CategoryProductsState} from './../../data/redux/slices/products/category/category.product.slice';
import {createReduxCategoryProductInterface} from '../../data/redux/slices/products/category/redux-category-product-interface';
import store from '../../data/redux/store';
import {
  createCategoryProductInteractor,
  CategoryProductInteractor,
} from '../../domain/interactors/use-cases/products/category-product-usecase';
import {
  BusinessCategoryDataFormated,
  CategorySupperTabs,
  GroupedProductsInBusinessCategoryFormated,
} from '../../domain/entity/structure/category-product.model';

const categoryInterface = createReduxCategoryProductInterface(store.dispatch);
const categoryInteractor = createCategoryProductInteractor(categoryInterface);

export interface CategoryProductViewModel {
  id: number;
  name: string;
  description: string;
  categoryimage64: string;
  categorySupperData: Array<any>;
  allCategoriesProducts: Array<any>;
  businessCategoryData: BusinessCategoryDataFormated[];
  businessTabsData: CategorySupperTabs[];
  scrollOffsetData: any[];
  productsByCategoryData: GroupedProductsInBusinessCategoryFormated[];
  productsByCategorySearchData:
    | GroupedProductsInBusinessCategoryFormated[]
    | undefined;
  search: string;
  searchIconBg: string | undefined;
  getCategorySupper(): void;
  getAllCategoriesProduct(): void;
  setSelectedTab(index: number): void;
  setOffsetData(offsetData: Array<any>): void;
  getProductsByCategory(category: number): void;
  getProductsBySearch(search: string): void;
  onChangeInput(
    categoryInteractor: CategoryProductInteractor,
    key: string,
    value: string
  ): void;
}

export function createCategoryProductViewModel(
  categoryState: CategoryProductsState
): CategoryProductViewModel {
  return {
    get id(): number {
      return categoryState.id;
    },
    get name(): string {
      return categoryState.name;
    },
    get description(): string {
      return categoryState.description;
    },
    get categoryimage64(): string {
      return categoryState.categoryimage64;
    },
    get categorySupperData(): Array<any> {
      return categoryState.categorySupperData;
    },
    get allCategoriesProducts(): Array<any> {
      return categoryState.allCategoriesProducts;
    },

    get businessCategoryData(): BusinessCategoryDataFormated[] {
      return categoryState.businessCategoryData;
    },

    get businessTabsData(): CategorySupperTabs[] {
      return categoryState.businessTabsData;
    },

    get scrollOffsetData(): any[] {
      return categoryState.scrollOffsetData;
    },

    get productsByCategoryData(): GroupedProductsInBusinessCategoryFormated[] {
      return categoryState.productsCategoryData;
    },

    get productsByCategorySearchData():
      | GroupedProductsInBusinessCategoryFormated[]
      | undefined {
      return categoryState.productsCategorySearchData;
    },

    get search(): string {
      return categoryState.search;
    },

    get searchIconBg(): string | undefined {
      return categoryState.searchIconBg;
    },

    getCategorySupper(): void {
      categoryInteractor.getProductsGroupedBySupperCategory();
    },

    getAllCategoriesProduct(): void {
      categoryInteractor.getAllCategoriesProduct();
    },

    setSelectedTab(index: number): void {
      categoryInteractor.setSelectedTab(index, [...this.businessTabsData]);
    },

    setOffsetData(offsetData: Array<any>): void {
      categoryInteractor.setOffsetData(offsetData);
    },

    getProductsByCategory(category: number): void {
      categoryInteractor.getProductsByCategory(category);
    },

    getProductsBySearch(search: string): void {
      categoryInteractor.getProductsBySearch(search);
    },
    onChangeInput(
      categoryInteractorUseCase: CategoryProductInteractor,
      key: string,
      value: string
    ): void {
      categoryInteractorUseCase.onChangeInputText(key, value);
    },
  };
}
