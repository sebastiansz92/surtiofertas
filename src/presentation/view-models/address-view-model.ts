import {AddressState} from '../../data/redux/slices/address/address.slice';
import {createAddressInteractor} from '../../domain/interactors/use-cases/address/address-usecase';
import {createReduxAddressInterface} from '../../data/redux/slices/address/redux-address-interface';
import store from '../../data/redux/store';
import {AddressValues} from '../../domain/entity/structure/address.model';

const addressInterface = createReduxAddressInterface(store.dispatch);
const addressInteractor = createAddressInteractor(addressInterface);

export interface AddressViewModel {
  currentAddressDecode: AddressValues | undefined;
  currentAddressByUser: AddressValues | undefined;
  addressListByUser: AddressValues[];
  defaultAddressWillUpdated: boolean | undefined;
  addressCreatedOrUpdated: boolean | undefined;
  errorValidation: string | undefined;
  getCurrentPosition(): void;
  getAddressByCoords(latitude: number, longitude: number): void;
  getDefaultAddressByUser(): void;
  getAddressesByUser(): void;
  setDefaultAddress(id: number): void;
  resetCurrentState(): void;
  resetErrorValidationState(): void;
  updateAddress(address: any): void;
  disabledAddress(id: number | undefined): void;
}

export function createAddressViewModel(
  addressState: AddressState
): AddressViewModel {
  return {
    get currentAddressDecode(): AddressValues | undefined {
      return addressState.currentAddressDecode;
    },

    get currentAddressByUser(): AddressValues | undefined {
      return addressState.currentAddressByUser;
    },

    get addressListByUser(): AddressValues[] {
      return addressState.addressList;
    },

    get defaultAddressWillUpdated(): boolean | undefined {
      return addressState.defaultAddressWillUpdated;
    },

    get addressCreatedOrUpdated(): boolean | undefined {
      return addressState.addressCreatedOrUpdated;
    },

    get errorValidation(): string | undefined {
      return addressState.errorValidation;
    },

    getCurrentPosition(): void {
      addressInteractor.getCurrentPosition();
    },

    getAddressByCoords(latitude: number, longitude: number): void {
      addressInteractor.getAddressByCoords({latitude, longitude});
    },

    getDefaultAddressByUser(): void {
      addressInteractor.getDefaultUserAddress();
    },

    getAddressesByUser(): void {
      addressInteractor.getAddressesByUser();
    },

    setDefaultAddress(id: number): void {
      addressInteractor.setDefaultAddressByUser(id);
    },

    resetCurrentState(): void {
      addressInteractor.resetCurrentState();
    },

    updateAddress(address: AddressValues): void {
      addressInteractor.updateAddress(address);
    },

    resetErrorValidationState(): void {
      addressInteractor.resetErrorValidationState();
    },

    disabledAddress(id: number | undefined): void {
      addressInteractor.disabledAddress(id);
    },
  };
}
