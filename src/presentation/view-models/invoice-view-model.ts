import store from '../../data/redux/store';
import {InvoiceState} from '../../data/redux/slices/invoice/invoice.slice';
import {createInvoiceInteractor} from '../../domain/interactors/use-cases/invoice/shopping-cart-usecase';
import {createReduxInvoiceInterface} from '../../data/redux/slices/invoice/redux-invoice-interface';
import {
  CancelInvoiceRequest,
  InvoiceDetails,
  PreInvoiceResponse,
} from '../../domain/entity/structure/invoice.model';

const invoiceInterface = createReduxInvoiceInterface(store.dispatch);
const invoiceInteractor = createInvoiceInteractor(invoiceInterface);

export interface InoviceViewModel {
  currentInvoices: InvoiceDetails[];
  recentOrdersByUserData: any[];
  preInvoiceData: PreInvoiceResponse;
  invoiceDetails: InvoiceDetails | undefined;
  generateInvoice(deliveryMethodId: number, paymentMethodId: number): void;
  getPendingInvoicesByUser(): void;
  getRecentOrdersProductsByUser(): void;
  preInvoice(): void;
  getInvoiceDetailsByOrder(orderId: number): void;
  cancelInvoice(invoiceRequest: CancelInvoiceRequest): void;
}

export function createInvoiceViewModel(
  invoiceState: InvoiceState
): InoviceViewModel {
  return {
    get currentInvoices(): InvoiceDetails[] {
      return invoiceState.currentInvoices;
    },
    get recentOrdersByUserData(): any[] {
      return invoiceState.recentOrdersByUserData;
    },
    get preInvoiceData(): PreInvoiceResponse {
      return invoiceState.preInvoiceData;
    },
    get invoiceDetails(): InvoiceDetails | undefined {
      return invoiceState.invoiceDetails;
    },
    generateInvoice(deliveryMethodId: number, paymentMethodId: number): void {
      invoiceInteractor.generateInvoice({
        deliveryMethodId,
        paymentMethodId,
      });
    },
    getPendingInvoicesByUser(): void {
      invoiceInteractor.getPendingInvoicesByUser();
    },
    getRecentOrdersProductsByUser(): void {
      invoiceInteractor.getRecentOrdersProductsByUser();
    },
    preInvoice(): void {
      invoiceInteractor.preInvoice();
    },
    getInvoiceDetailsByOrder(orderId: number): void {
      invoiceInteractor.getInvoiceDetailsByOrder(orderId);
    },
    cancelInvoice(invoiceRequest: CancelInvoiceRequest): void {
      invoiceInteractor.cancelInvoice(invoiceRequest);
    },
  };
}
