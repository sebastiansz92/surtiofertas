import {
  createLoginInteractor,
  LoginInteractor,
} from '../../domain/interactors/use-cases/user/login-usecase';
import {LoginState} from '../../data/redux/slices/user/login.slice';
import {createReduxLoginInterface} from '../../data/redux/slices/user/redux-login-interface';
import store from '../../data/redux/store';

const loginInterface = createReduxLoginInterface(store.dispatch);
const loginUseCase = createLoginInteractor(loginInterface);

export interface LoginViewModel {
  email: string;
  emailIconBg: string | undefined;
  password: string;
  passwordIconBg: string | undefined;
  pendingLogin: boolean;
  errorValidation: string | undefined;
  userToken: string | undefined;
  reloadUserInfo: boolean | undefined;
  onFocusInput(loginInteractor: LoginInteractor, key: string): void;
  onBlurInput(
    loginInteractor: LoginInteractor,
    key: string,
    value: string | undefined
  ): void;
  onChangeInput(
    loginInteractor: LoginInteractor,
    key: string,
    value: string
  ): void;
  onPressButtonLogin(loginInteractor: LoginInteractor): void;
  resetValidation(loginInteractor: LoginInteractor): void;
  getCurrentUserToken(): Promise<string | undefined>;
}

export function createLoginViewModel(loginState: LoginState): LoginViewModel {
  return {
    get email(): string {
      return loginState.email;
    },

    get emailIconBg(): string | undefined {
      return loginState.emailIconBg;
    },

    get password(): string {
      return loginState.password;
    },

    get passwordIconBg(): string | undefined {
      return loginState.passwordIconBg;
    },

    get pendingLogin(): boolean {
      return loginState.pendingLogin;
    },

    get errorValidation(): string | undefined {
      return loginState.errorValidation;
    },

    get userToken(): string | undefined {
      return loginState.userToken;
    },

    get reloadUserInfo(): boolean | undefined {
      return loginState.reloadUserInfo;
    },

    onFocusInput(loginInteractor: LoginInteractor, key: string): void {
      loginInteractor.setFocusStyle(`${key}IconBg`);
    },
    onBlurInput(
      loginInteractor: LoginInteractor,
      key: string,
      value: string | undefined
    ): void {
      loginInteractor.setInputIconColorOnBlur(key, value);
    },
    onChangeInput(
      loginInteractor: LoginInteractor,
      key: string,
      value: string
    ): void {
      loginInteractor.onChangeInputText(key, value);
    },
    onPressButtonLogin(loginInteractor: LoginInteractor): void {
      loginInteractor.loginUser({
        username: this.email,
        password: this.password,
      });
    },
    resetValidation(loginInteractor: LoginInteractor): void {
      loginInteractor.resetErrorValidations();
    },

    async getCurrentUserToken(): Promise<string | undefined> {
      return await loginUseCase.getCurrentUserToken();
    },
  };
}
