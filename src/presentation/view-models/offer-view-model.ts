import {OfferState} from './../../data/redux/slices/products/offer/offer.slice';
import {createOfferInteractor} from '../../domain/interactors/use-cases/products/offer-usecase';
import {OfferContent} from '../../domain/entity/structure/offer.model';
import {createReduxOfferInterface} from '../../data/redux/slices/products/offer/redux-offer-interface';
import store from '../../data/redux/store';

const offerInterface = createReduxOfferInterface(store.dispatch);
const offerInteractor = createOfferInteractor(offerInterface);

export interface OfferViewModel {
  offerProducts: Array<OfferContent>;
  pendingOffer: boolean;
  getMarketOffers(): void;
}

export function createOfferViewModel(offerState: OfferState): OfferViewModel {
  return {
    get offerProducts(): Array<OfferContent> {
      return offerState.offerProducts;
    },

    get pendingOffer(): boolean {
      return offerState.pendingOffer;
    },

    getMarketOffers(): void {
      offerInteractor.getMarketOffers();
    },
  };
}
