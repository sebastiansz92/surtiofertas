import store from '../../data/redux/store';
import {ShoppingCartState} from '../../data/redux/slices/products/cart/shoping-cart.slice';
import {createShoppingCartInteractor} from '../../domain/interactors/use-cases/products/shopping-cart-usecase';
import {
  AddCarProductRequestData,
  ShoppingCartDataState,
} from '../../domain/entity/structure/category-product.model';
import {createReduxShoppingCartInterface} from '../../data/redux/slices/products/cart/redux-shopping-cart-interface';
import {GenerateInvoiceResponse} from '../../domain/entity/structure/invoice.model';

const shoppingCartInterface = createReduxShoppingCartInterface(store.dispatch);
const shoppingCartInteractor = createShoppingCartInteractor(
  shoppingCartInterface
);

export interface ShoppingCartViewModel {
  productWasAddedToCart: boolean | undefined;
  shoppingCartData: ShoppingCartDataState | undefined;
  showProductRemovedMessage: boolean;
  showProductsRemovedMessage: boolean;
  responseGenerateInvoice: GenerateInvoiceResponse | undefined;
  removeAllProductsInCar(): void;
  addProductToCar(request: AddCarProductRequestData): void;
  getAllProductsInCar(): void;
  removeProductInCar(productId: number): void;
}

export function createShoppingCartViewModel(
  shoppingCartState: ShoppingCartState
): ShoppingCartViewModel {
  return {
    get productWasAddedToCart(): boolean | undefined {
      return shoppingCartState.productWasAddedToCart;
    },
    get shoppingCartData(): ShoppingCartDataState | undefined {
      return shoppingCartState.shoppingCartData;
    },
    get showProductRemovedMessage(): boolean {
      return shoppingCartState.showProductRemovedMessage;
    },

    get showProductsRemovedMessage(): boolean {
      return shoppingCartState.showProductsRemovedMessage;
    },

    get responseGenerateInvoice(): GenerateInvoiceResponse | undefined {
      return shoppingCartState.responseGenerateInvoice;
    },
    addProductToCar(request: AddCarProductRequestData): void {
      shoppingCartInteractor.addProductToCar(request);
    },
    getAllProductsInCar(): void {
      shoppingCartInteractor.getAllProductsInCar();
    },
    removeProductInCar(productId: number): void {
      shoppingCartInteractor.removeProductInCar(productId);
    },
    removeAllProductsInCar(): void {
      shoppingCartInteractor.removeAllProductsInCar();
    },
  };
}
