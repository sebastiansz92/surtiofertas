import {UpdatePasswordInteractor} from '../../domain/interactors/use-cases/updatepassword-usecase';
import {UpdatePasswordState} from '../../data/redux/slices/user/updatepassword.slice';

export interface UpdatePasswordViewModel {
  password: string;
  confirmPassword: string;
  passwordIconBg: string | undefined;
  confirmPasswordIconBg: string | undefined;
  errorValidation: string | undefined;
  passwordWasUpdated: boolean;
  passwordUpdateMessage: string | undefined;
  onFocusInput(
    updatePasswordInteractor: UpdatePasswordInteractor,
    key: string,
    currentValue: string
  ): void;
  onBlurInput(
    updatePasswordInteractor: UpdatePasswordInteractor,
    key: string,
    value: string | undefined
  ): void;
  onChangeInput(
    updatePasswordInteractor: UpdatePasswordInteractor,
    key: string,
    value: string
  ): void;
  onPressButtonUpdatePassword(
    updatePasswordInteractor: UpdatePasswordInteractor,
    userId: number
  ): void;
  resetValidation(updatePasswordInteractor: UpdatePasswordInteractor): void;
}

export function createUpdatePasswordViewModel(
  updatePasswordState: UpdatePasswordState
): UpdatePasswordViewModel {
  return {
    get password(): string {
      return updatePasswordState.password;
    },

    get confirmPasswordIconBg(): string | undefined {
      return updatePasswordState.confirmPasswordIconBg;
    },

    get confirmPassword(): string {
      return updatePasswordState.confirmPassword;
    },

    get passwordIconBg(): string | undefined {
      return updatePasswordState.passwordIconBg;
    },

    get errorValidation(): string | undefined {
      return updatePasswordState.errorValidation;
    },

    get passwordWasUpdated(): boolean {
      return updatePasswordState.passwordWasUpdated;
    },

    get passwordUpdateMessage(): string | undefined {
      return updatePasswordState.passwordUpdateMessage;
    },

    onFocusInput(
      updatePasswordInteractor: UpdatePasswordInteractor,
      key: string
    ): void {
      updatePasswordInteractor.setFocusStyle(`${key}IconBg`);
    },
    onBlurInput(
      updatePasswordInteractor: UpdatePasswordInteractor,
      key: string,
      value: string | undefined
    ): void {
      updatePasswordInteractor.setInputIconColorOnBlur(key, value);
    },
    onChangeInput(
      updatePasswordInteractor: UpdatePasswordInteractor,
      key: string,
      value: string
    ): void {
      updatePasswordInteractor.onChangeInputText(key, value);
    },
    onPressButtonUpdatePassword(
      updatePasswordInteractor: UpdatePasswordInteractor,
      userId: number
    ): void {
      updatePasswordInteractor.updatePasswordUser({
        newPassword: this.password,
        repeatPasword: this.confirmPassword,
        userId: +userId,
      });
    },
    resetValidation(updatePasswordInteractor: UpdatePasswordInteractor): void {
      updatePasswordInteractor.resetErrorValidations();
    },
  };
}
