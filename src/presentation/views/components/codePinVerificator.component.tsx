import {useFocusEffect} from '@react-navigation/native';
import React, {useEffect, useRef, useState} from 'react';
import {TextInput, View} from 'react-native';
import {ValidateCodeInteractor} from '../../../domain/interactors/use-cases/validatecode-usecase';
import {ValidateCodeViewModel} from '../../view-models/validatecode-view-model';
import {VALIDATECODE_STYLES} from '../styles/ScreensStyles';

type PinCodeVerificatorProps = {
  validateCodeViewModel: ValidateCodeViewModel;
  validateCodeInteractor: ValidateCodeInteractor;
};

function CodePinVerificator(props: PinCodeVerificatorProps) {
  const {validateCodeInteractor, validateCodeViewModel} = props;

  let pinRef0 = useRef<TextInput>(null);
  let pinRef1 = useRef<TextInput>(null);
  let pinRef2 = useRef<TextInput>(null);
  let pinRef3 = useRef<TextInput>(null);
  const [currentPinInputIndex, setcurrentPinInputIndex] = useState(0);

  useFocusEffect(
    React.useCallback(() => {
      pinRef0.current?.focus();
    }, [])
  );

  useEffect(() => {
    if (validateCodeViewModel.pinTarget !== currentPinInputIndex) {
      updateFocus();
    }
    function updateFocus() {
      switch (validateCodeViewModel.pinTarget) {
        case 0:
          pinRef0.current?.focus();
          break;
        case 1:
          pinRef1.current?.focus();
          break;
        case 2:
          pinRef2.current?.focus();
          break;
        case 3:
          pinRef3.current?.focus();
          break;

        default:
          break;
      }
      setcurrentPinInputIndex(validateCodeViewModel.pinTarget);
    }
  });

  return (
    <View style={VALIDATECODE_STYLES.flex_row}>
      <TextInput
        key="pin0"
        ref={pinRef0}
        keyboardType="number-pad"
        textAlign={'center'}
        style={VALIDATECODE_STYLES.inputStyle}
        onChangeText={(value: string) =>
          validateCodeViewModel.onChangePinValueIndex(
            validateCodeInteractor,
            value,
            0
          )
        }
        value={validateCodeViewModel.getCurrentPinValue(0)}
      />
      <TextInput
        key="pin1"
        ref={pinRef1}
        keyboardType="number-pad"
        textAlign={'center'}
        style={VALIDATECODE_STYLES.inputStyle}
        onChangeText={(value: string) =>
          validateCodeViewModel.onChangePinValueIndex(
            validateCodeInteractor,
            value,
            1
          )
        }
        value={validateCodeViewModel.getCurrentPinValue(1)}
      />
      <TextInput
        key="pin2"
        ref={pinRef2}
        keyboardType="number-pad"
        textAlign={'center'}
        style={VALIDATECODE_STYLES.inputStyle}
        onChangeText={(value: string) =>
          validateCodeViewModel.onChangePinValueIndex(
            validateCodeInteractor,
            value,
            2
          )
        }
        value={validateCodeViewModel.getCurrentPinValue(2)}
      />
      <TextInput
        key="pin3"
        ref={pinRef3}
        keyboardType="number-pad"
        textAlign={'center'}
        style={VALIDATECODE_STYLES.inputStyle}
        onChangeText={(value: string) =>
          validateCodeViewModel.onChangePinValueIndex(
            validateCodeInteractor,
            value,
            3
          )
        }
        value={validateCodeViewModel.getCurrentPinValue(3)}
      />
    </View>
  );
}

export default CodePinVerificator;
