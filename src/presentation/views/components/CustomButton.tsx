import React from 'react';
import {StyleProp, ViewStyle, Pressable, ButtonProps, Text} from 'react-native';
import {GLOBAL_STYLES} from '../styles/common.styles';

type CustomInputProps = {
  children?: any;
  buttonProps: ButtonProps;
  textButton?: string;
  styleButton: StyleProp<ViewStyle>;
  styleText: StyleProp<any>;
  colorText: string;
  colorButton: string;
};

export default function CustomButton(props: CustomInputProps) {
  const {
    buttonProps,
    textButton,
    styleButton,
    styleText,
    colorText,
    colorButton,
  } = props;
  return (
    <Pressable
      {...buttonProps}
      style={[getColorBackground(colorButton), styleButton]}>
      <Text style={[getColor(colorText), styleText]}>{textButton}</Text>
    </Pressable>
  );
}

function getColorBackground(color: string) {
  switch (color) {
    case 'red':
      return GLOBAL_STYLES.backgroundRed;
    case 'blue':
      return GLOBAL_STYLES.backgroundBlue;
    case 'yellow':
      return GLOBAL_STYLES.backgroundYellow;
    case 'grey':
      return GLOBAL_STYLES.backgroundGrey;
    case 'white':
      return GLOBAL_STYLES.backgroundWhite;
    default:
      return GLOBAL_STYLES.backgroundInherit;
  }
}

function getColor(color: string) {
  switch (color) {
    case 'red':
      return GLOBAL_STYLES.red;
    case 'blue':
      return GLOBAL_STYLES.blue;
    case 'yellow':
      return GLOBAL_STYLES.yellow;
    case 'darkGrey':
      return GLOBAL_STYLES.darkGrey;
    default:
      return GLOBAL_STYLES.defaultColor;
  }
}
