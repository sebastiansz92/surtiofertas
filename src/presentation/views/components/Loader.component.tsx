import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {useSelector} from 'react-redux';
import {StoreState} from '../../../data/redux/reducers';
import {LayoutState} from '../../../data/redux/slices/layout.slice';

export default function LoaderComponent() {
  const layoutState: LayoutState = useSelector(
    (state: StoreState) => state.layout
  );
  return (
    <>
      {layoutState.showGlobalLoading && (
        <View style={LOADER_STYLES.container}>
          <Image
            style={LOADER_STYLES.image}
            source={require('../../../../assets/img/layout/loader.gif')}
          />
        </View>
      )}
    </>
  );
}

export const LOADER_STYLES = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,0.9)',
  },
  image: {height: 200, aspectRatio: 1},
});
