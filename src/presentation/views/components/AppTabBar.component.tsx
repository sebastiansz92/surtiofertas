import React from 'react';
import {Platform, Pressable, Text, View} from 'react-native';
import SearchIconBar from '../../../../assets/img/layout/buscar.svg';
import SearchIconBarFocus from '../../../../assets/img/layout/buscarBlue.svg';
import HomeIconBar from '../../../../assets/img/layout/inicio.svg';
import HomeIconBarFocus from '../../../../assets/img/layout/inicioBlue.svg';
import FavoriteIconBar from '../../../../assets/img/layout/favoritos.svg';
import FavoriteIconBarFocus from '../../../../assets/img/layout/favoritosBlue.svg';
import SupportIconBar from '../../../../assets/img/layout/soporte.svg';
import SupportIconBarFocus from '../../../../assets/img/layout/soporteBlue.svg';
import AccountIconBar from '../../../../assets/img/layout/userCuenta.svg';
import AccountIconBarFocus from '../../../../assets/img/layout/userCuentaBlue.svg';
import {APP_BAR_STYLES} from '../styles/app-tab-bar.styles';

export default function AppTabBar({state, descriptors, navigation}) {
  const onLongPress = (route) => {
    navigation.emit({
      type: 'tabLongPress',
      target: route.key,
    });
  };

  const onPress = (route, isFocused) => {
    const event = navigation.emit({
      type: 'tabPress',
      target: route.key,
      canPreventDefault: true,
    });

    if (!isFocused && !event.defaultPrevented) {
      navigation.navigate(route.name);
    }
  };

  const selectIcon = (route, isFocused) => {
    if (route.name === 'Home' && isFocused) {
      return <HomeIconBarFocus />;
    } else if (route.name === 'Home' && !isFocused) {
      return <HomeIconBar />;
    } else if (route.name === 'Favorites' && isFocused) {
      return <FavoriteIconBarFocus />;
    } else if (route.name === 'Favorites' && !isFocused) {
      return <FavoriteIconBar />;
    } else if (route.name === 'Search' && isFocused) {
      return <SearchIconBarFocus />;
    } else if (route.name === 'Search' && !isFocused) {
      return <SearchIconBar />;
    } else if (route.name === 'Support' && isFocused) {
      return <SupportIconBarFocus />;
    } else if (route.name === 'Support' && !isFocused) {
      return <SupportIconBar />;
    } else if (route.name === 'Account' && isFocused) {
      return <AccountIconBarFocus />;
    } else if (route.name === 'Account' && !isFocused) {
      return <AccountIconBar />;
    }
  };

  return (
    <View
      style={
        Platform.OS === 'ios'
          ? APP_BAR_STYLES.iosContainer
          : APP_BAR_STYLES.androidContainer
      }>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;
        return (
          <Pressable
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={() => onPress(route, isFocused)}
            onLongPress={() => onLongPress(route)}
            style={APP_BAR_STYLES.buttonMenu}>
            <View style={APP_BAR_STYLES.buttonIcon}>
              {selectIcon(route, isFocused)}
            </View>
            <Text
              style={[
                APP_BAR_STYLES.textButton,
                {color: isFocused ? '#005fab' : '#bcbcbc'},
              ]}>
              {label}
            </Text>
          </Pressable>
        );
      })}
    </View>
  );
}
