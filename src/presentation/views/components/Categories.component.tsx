import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {ImageBackground, Pressable, Text, View} from 'react-native';
import {ENVIRONMENT} from '../../../data/constants/api.constants';
import {CategoryProductViewModel} from '../../view-models/category-product-view-model';
import {CATEGORIES_STYLES} from '../styles/categories.styles';

type CategoriesProps = {
  category: CategoryProductViewModel;
};

export default function Categories(props: CategoriesProps) {
  const {category} = props;
  const navigation = useNavigation();
  return (
    <Pressable
      onPress={() =>
        navigation.navigate('CategoryDetails', {category: category})
      }
      style={CATEGORIES_STYLES.containerImages}>
      <ImageBackground
        style={CATEGORIES_STYLES.image}
        imageStyle={CATEGORIES_STYLES.borderImage}
        source={{
          uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${category.categoryimage64}`,
        }}
        resizeMode="cover">
        <View style={CATEGORIES_STYLES.containerNameCategory}>
          <View style={CATEGORIES_STYLES.categoryName}>
            <Text style={CATEGORIES_STYLES.imageText}>{category.name}</Text>
          </View>
        </View>
      </ImageBackground>
    </Pressable>
  );
}
