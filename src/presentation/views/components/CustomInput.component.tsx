import React from 'react';
import {
  View,
  TextInput,
  StyleProp,
  ViewStyle,
  TextInputProps,
} from 'react-native';
import {GLOBAL_STYLES} from '../styles/common.styles';

type CustomInputProps = {
  children?: any;
  inputProps: TextInputProps;
  bgColorIcon?: string;
  style?: StyleProp<ViewStyle>;
};

export default function CustomInput(props: CustomInputProps) {
  const {children, inputProps, style, bgColorIcon} = props;
  return (
    <View style={[GLOBAL_STYLES.inputContainer, style]}>
      <View
        style={[
          {backgroundColor: bgColorIcon || '#bdbdbd'},
          GLOBAL_STYLES.iconContainer,
        ]}>
        {children}
      </View>
      <TextInput
        {...inputProps}
        style={[GLOBAL_STYLES.inputStyle, inputProps.style]}
      />
    </View>
  );
}
