import React from 'react';
import {View} from 'react-native';
import {HEADER_LOGO_STYLES} from '../styles/header-logo.styles';
import LogoHeader from '../../../../assets/img/layout/logo-surtiofertas.svg';

export default function HeaderLogo() {
  return (
    <View style={HEADER_LOGO_STYLES.viewContainer}>
      <LogoHeader />
    </View>
  );
}
