import React, {useState} from 'react';
import {Pressable, Text, View} from 'react-native';
import {DETAILPRODUCT_STYLES} from '../styles/ScreensStyles';

type CounterComponentProps = {
  onUpdateAmount(count: number): void;
  limit?: number;
  initAmount?: number;
};

export default function CounterComponent(props: CounterComponentProps) {
  const limit: number | undefined = props.limit;
  const [amount, setamount] = useState(props.initAmount || 1);

  return (
    <View style={DETAILPRODUCT_STYLES.containerAmount}>
      <View style={DETAILPRODUCT_STYLES.counter}>
        <Pressable
          onPress={() => {
            let acomulateAmount = amount - 1;
            amount > 1 && setamount(acomulateAmount);
            acomulateAmount >= 0 && props.onUpdateAmount(acomulateAmount);
          }}>
          <Text style={DETAILPRODUCT_STYLES.minus}>-</Text>
        </Pressable>
        <Text style={DETAILPRODUCT_STYLES.amount}>{amount}</Text>
        <Pressable
          onPress={() => {
            let acomulateAmount = amount + 1;
            if (limit !== undefined && acomulateAmount <= limit) {
              setamount(acomulateAmount);
            } else {
              setamount(acomulateAmount);
            }
            props.onUpdateAmount(acomulateAmount);
          }}>
          <Text style={DETAILPRODUCT_STYLES.plus}>+</Text>
        </Pressable>
      </View>
    </View>
  );
}
