import React from 'react';
import {Image, Text, ToastAndroid, View} from 'react-native';
import {useSelector} from 'react-redux';
import {StoreState} from '../../../data/redux/reducers';
import ProductEntity from '../../../domain/entity/model/ProductEntity';
import {FAVORITES_STYLES} from '../styles/ScreensStyles';
import CustomButton from './CustomButton';
import {createShoppingCartViewModel} from '../../view-models/shopping-cart-view-model';
import {useNavigation} from '@react-navigation/core';

export default function ProductsVerticalComponent(props: any) {
  const {product} = props;
  const navigation = useNavigation();
  const totalProduct = product.subtotal + product.taxes + product.deliveryCost;
  const createDate =
    product.createdDate.date.day +
    '/' +
    product.createdDate.date.month +
    '/' +
    product.createdDate.date.year;
  const productEntity = new ProductEntity();

  const shoppingCartViewModel = createShoppingCartViewModel(
    useSelector((state: StoreState) => state.shoppingCart)
  );

  const addProductsToShoppingCart = (invoiceDetails) => {
    invoiceDetails.map((elm: any) => {
      shoppingCartViewModel.addProductToCar({
        product: elm.product,
        quantity: elm.productQuantity,
      });
    });
    ToastAndroid.show(
      'Los productos de este pedido han sido agregados a tu carrito',
      ToastAndroid.SHORT
    );
    navigation.navigate('ShoppinCartList');
  };
  return (
    <View style={FAVORITES_STYLES.productOrders}>
      <View style={FAVORITES_STYLES.sizeImage}>
        <Image
          style={FAVORITES_STYLES.imageProduct}
          source={{
            uri:
              'https://cdn2.manchesterinklink.com/wp-content/uploads/2020/11/download-86-2048x1536.jpeg',
          }}
        />
      </View>
      <View style={FAVORITES_STYLES.sizeDescription}>
        <View>
          <View style={{flexDirection: 'row'}}>
            <Text style={[FAVORITES_STYLES.price, {flex: 1}]}>
              {productEntity.formatPrice(totalProduct)}
            </Text>
            <Text style={FAVORITES_STYLES.date}>{createDate}</Text>
          </View>

          <Text style={FAVORITES_STYLES.contentVertical}>
            {product.invoiceDetails.length} items
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 5,
          }}>
          <CustomButton
            colorText="blue"
            colorButton="white"
            styleText={FAVORITES_STYLES.btnText}
            styleButton={{
              borderWidth: 1,
              borderColor: '#005EA9',
              width: '100%',
              height: '100%',
              borderRadius: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            textButton="Pedir de nuevo"
            buttonProps={{
              title: 'Pedir de nuevo',
              onPress: () => {
                addProductsToShoppingCart(product.invoiceDetails);
              },
            }}
          />
        </View>
      </View>
    </View>
  );
}
