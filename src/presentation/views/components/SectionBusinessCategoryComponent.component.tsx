import React, {Fragment, useEffect} from 'react';
import {FlatList, Pressable, SectionList, Text, View} from 'react-native';
import {
  BusinessCategoryDataFormated,
  CategorySupperTabs,
  GroupedProductsInBusinessCategoryFormated,
} from '../../../domain/entity/structure/category-product.model';
import {CategoryProductViewModel} from '../../view-models/category-product-view-model';
import {SECTION_BUSINESS_STYLES} from '../styles/categories.styles';
import VirtualizedProductListComponent from './VirtualizedProductListComponent';

type ProductItemsProps = {
  businessCategoryData: BusinessCategoryDataFormated[];
  scrollToSection: number;
  categoryViewModel: CategoryProductViewModel;
  onChangeTabSection(index: number): void;
};

export default function SectionBusinessCategoryComponent(
  props: ProductItemsProps
) {
  const {businessCategoryData, categoryViewModel} = props;

  const Item = ({businessCategory}: any) => {
    const positionItemInArray: number = businessCategoryData
      .map((elm) => elm.supperCategoryName)
      .indexOf(businessCategory.supperCategoryName);
    return (
      <View
        style={[
          SECTION_BUSINESS_STYLES.container,
          {
            marginTop: positionItemInArray === 0 ? 0 : 10,
          },
        ]}>
        <View style={SECTION_BUSINESS_STYLES.categoryNameContainer}>
          <Text style={SECTION_BUSINESS_STYLES.categoryNameText}>
            {businessCategory.supperCategoryName}
          </Text>
        </View>
        {businessCategory.productsGroupedBy.map(
          (elm: GroupedProductsInBusinessCategoryFormated) => {
            return (
              <View
                style={SECTION_BUSINESS_STYLES.productCategoryContainer}
                key={`${businessCategory.supperCategoryName}_${elm.categoryName}`}>
                <Text style={SECTION_BUSINESS_STYLES.productCategoryText}>
                  {elm.categoryName}
                </Text>

                <VirtualizedProductListComponent
                  products={elm.categoryContent.content}
                />
              </View>
            );
          }
        )}
      </View>
    );
  };

  let tabSectionRef: SectionList<number, CategorySupperTabs> | null = null;

  useEffect(() => {
    tabSectionRef &&
      categoryViewModel.businessTabsData.length > 0 &&
      tabSectionRef.scrollToLocation({
        animated: true,
        sectionIndex: categoryViewModel.businessTabsData
          .map((elm) => elm.selected)
          .indexOf(true),
        itemIndex: 0,
      });
  }, [categoryViewModel.businessTabsData, tabSectionRef]);
  return (
    <Fragment>
      <FlatList
        stickyHeaderIndices={[0]}
        contentContainerStyle={SECTION_BUSINESS_STYLES.flatListBusinessCategory}
        data={businessCategoryData}
        ListHeaderComponent={
          <SectionList
            showsHorizontalScrollIndicator={false}
            ref={(ref) => (tabSectionRef = ref)}
            sections={categoryViewModel.businessTabsData}
            horizontal={true}
            keyExtractor={(item, index) => item.toString() + index}
            renderItem={() => <Fragment />}
            renderSectionHeader={({section}) => {
              return (
                <Pressable
                  onPress={() => {
                    categoryViewModel.setSelectedTab(section.index);
                    props.onChangeTabSection(section.index);
                    tabSectionRef &&
                      tabSectionRef.scrollToLocation({
                        animated: true,
                        sectionIndex: section.index,
                        itemIndex: 0,
                      });
                  }}
                  style={[
                    SECTION_BUSINESS_STYLES.sectionHeader,
                    {
                      borderBottomWidth: section.selected ? 3 : 0,
                      borderBottomColor: section.selected ? '#de0209' : 'white',
                    },
                  ]}>
                  <Text
                    style={{
                      color: section.selected ? '#616161' : '#bdbdbd',
                      fontSize: 16,
                      fontWeight: section.selected ? 'bold' : '300',
                    }}>
                    {section.title}
                  </Text>
                </Pressable>
              );
            }}
          />
        }
        renderItem={({item}) => <Item businessCategory={item} />}
        keyExtractor={(item) => item.supperCategoryName}
      />
    </Fragment>
  );
}
