import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  ImageBackground,
  Pressable,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import {ENVIRONMENT} from '../../../data/constants/api.constants';
import Icon from 'react-native-vector-icons/FontAwesome5';

type SliderProps = {
  slideElements: Array<any>;
  screen: string;
  wrap: boolean;
  rounded: boolean;
  stylesSlide?: any;
  enabledControls: boolean;
};

export default function SliderComponent(props: SliderProps) {
  const {
    wrap,
    slideElements,
    screen,
    rounded,
    stylesSlide,
    enabledControls,
  } = props;

  const [currentPage, setCurrentPage] = useState(0);
  const [intervalPages, setIntervalPages] = useState<any>(null);
  const [currentXOffset, setCurrentXOffste] = useState(0);
  const [scrollViewWidth, setScrollViewWidth] = useState(0);
  const {width} = Dimensions.get('window');
  const sliderRef = React.useRef<ScrollView>(null);

  const setSliderPage = (event?: any) => {
    const {x} = event.nativeEvent.contentOffset;
    const newXOffset = event.nativeEvent.contentOffset.x;
    setCurrentXOffste(newXOffset);
    const widthWindow = width - 21;
    const indexOfNextScreen = Math.floor(x / widthWindow);
    if (indexOfNextScreen !== currentPage) {
      setCurrentPage(indexOfNextScreen);
    }
  };

  const leftArrow = () => {
    let eachItemOffset = scrollViewWidth / slideElements.length;
    let _currentXOffset = currentXOffset - eachItemOffset;
    sliderRef.current?.scrollTo({x: _currentXOffset, y: 0, animated: true});
  };

  const rightArrow = () => {
    let eachItemOffset = scrollViewWidth / slideElements.length;
    let _currentXOffset = currentXOffset + eachItemOffset;
    sliderRef.current?.scrollTo({x: _currentXOffset, y: 0, animated: true});
  };

  const initScheduleTaskSlider = () => {
    let page = currentPage;
    const interval = setInterval(() => {
      let offset;

      if (page === slideElements.length - 1) {
        page = 0;
        offset = page * width;
      } else {
        page++;
        offset = page * width - 20;
      }
      setCurrentPage(page);
      sliderRef.current?.scrollTo({x: offset, y: 0, animated: true});
    }, 6000);
    setIntervalPages(interval);
  };

  useEffect(() => {
    setTimeout(() => {
      wrap && initScheduleTaskSlider();
    }, 0.1);
    return () => clearInterval(intervalPages);
  }, []);
  return (
    <View style={SLIDE_STYLES.flex_1}>
      {enabledControls && currentPage > 0 && (
        <Pressable style={SLIDE_STYLES.leftControl} onPress={leftArrow}>
          <Icon name="arrow-left" size={20} color="#005EA8" />
        </Pressable>
      )}
      <ScrollView
        ref={sliderRef}
        horizontal={true}
        scrollEventThrottle={16}
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
        onContentSizeChange={(w, h) => setScrollViewWidth(w)}
        onScroll={(event: any) => {
          setSliderPage(event);
        }}
        style={SLIDE_STYLES.flex_1}>
        {slideElements.map((elm, index) => {
          return screen === 'welcome' ? (
            <View key={index} style={stylesSlide || ''}>
              <ImageBackground
                style={SLIDE_STYLES.full}
                imageStyle={{borderRadius: rounded ? 7 : 0}}
                source={elm.image}
              />
            </View>
          ) : (
            <View key={index} style={stylesSlide || ''}>
              <ImageBackground
                style={SLIDE_STYLES.full}
                imageStyle={{borderRadius: rounded ? 7 : 0}}
                source={{
                  uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${elm.imageUrl}`,
                }}
                resizeMode="cover"
              />
            </View>
          );
        })}
      </ScrollView>
      {enabledControls && currentPage < slideElements.length - 1 && (
        <Pressable style={SLIDE_STYLES.rightControl} onPress={rightArrow}>
          <Icon name="arrow-right" size={20} color="#005EA8" />
        </Pressable>
      )}
      <View style={SLIDE_STYLES.controlsContainer}>
        {slideElements.map((elm, index) => {
          return (
            <View
              key={`control${index}`}
              style={
                index === currentPage
                  ? SLIDE_STYLES.controlItem
                  : SLIDE_STYLES.controlItemActive
              }
            />
          );
        })}
      </View>
    </View>
  );
}

const SLIDE_STYLES = StyleSheet.create({
  flex_1: {flex: 1},
  controlsContainer: {
    position: 'absolute',
    bottom: 15,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    zIndex: 1,
  },
  controlItem: {
    width: 6,
    aspectRatio: 1,
    backgroundColor: 'white',
    borderRadius: 3,
    marginHorizontal: 2,
  },
  controlItemActive: {
    width: 6,
    aspectRatio: 1,
    backgroundColor: 'white',
    opacity: 0.5,
    borderRadius: 3,
    marginHorizontal: 2,
  },
  leftControl: {
    position: 'absolute',
    zIndex: 10,
    top: 20,
    left: 10,
  },
  rightControl: {
    position: 'absolute',
    zIndex: 1,
    bottom: 20,
    right: 10,
  },
  full: {
    height: '100%',
    width: '100%',
  },
});
