import {StackActions} from '@react-navigation/native';
import React from 'react';
import {Pressable, Text, View} from 'react-native';
import {DialogMessage} from '../../../domain/entity/structure/user.model';
import {DIALOG_STYLES} from '../styles/dialog.styles';

export default function DialogComponent({route, navigation}) {
  const {title, message, button} = route.params as DialogMessage;

  return (
    <View style={DIALOG_STYLES.dialogContainer}>
      <View style={DIALOG_STYLES.w_dialog}>
        <View style={DIALOG_STYLES.header}>
          <Text style={[DIALOG_STYLES.textCenter, {color: title.color}]}>
            {title.text}
          </Text>
        </View>
        <View style={DIALOG_STYLES.body}>
          <Text style={DIALOG_STYLES.messageDialog}>{message}</Text>
        </View>
        <View style={DIALOG_STYLES.footer}>
          <Pressable
            onPress={() => navigation.dispatch(StackActions.pop(1))}
            style={[
              DIALOG_STYLES.actionBtnDialog,
              {
                borderColor: button.color,
              },
            ]}>
            <Text style={{color: button.color}}>{button.text}</Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
}
