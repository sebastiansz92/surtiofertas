import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {ImageBackground, Pressable, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {ENVIRONMENT} from '../../../data/constants/api.constants';
import {ProductDetails} from '../../../domain/entity/structure/category-product.model';
import {FAVORITES_STYLES} from '../styles/ScreensStyles';
import {setProductTarget} from '../../../data/redux/slices/products/favorites/favorites.product.slice';

type HorizontalProductProps = {
  product: ProductDetails;
  carButtonAdd: boolean;
};

export default function ProductsHorizontalComponent(
  props: HorizontalProductProps
) {
  const {product} = props;
  const navigation = useNavigation();
  const dispatch = useDispatch();

  return (
    <Pressable
      onPress={() => {
        navigation.navigate('DetailProdut', {product});
        dispatch(setProductTarget(product.id));
      }}
      style={FAVORITES_STYLES.product}>
      <View style={FAVORITES_STYLES.horizontalProductContainer}>
        <ImageBackground
          style={FAVORITES_STYLES.imageProduct}
          source={{
            uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${product.image_url}`,
          }}
          resizeMode="contain"
        />
      </View>
      <View>
        <Text style={FAVORITES_STYLES.price}>
          {'$' +
            product?.priceOffered
              ?.toString()
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
        </Text>
        <Text
          numberOfLines={2}
          ellipsizeMode="clip"
          style={FAVORITES_STYLES.description}>
          {product.name}
        </Text>
        <Text style={FAVORITES_STYLES.content}>{product.weight}</Text>
      </View>
    </Pressable>
  );
}
