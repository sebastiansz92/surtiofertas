import React from 'react';
import {VirtualizedList} from 'react-native';
import {ProductDetails} from '../../../domain/entity/structure/category-product.model';
import ProductsHorizontalComponent from './ProductsHorizontalComponent';

type ProductItemsProps = {
  products: ProductDetails[];
};

export default function VirtualizedProductListComponent(
  props: ProductItemsProps
) {
  const getItemCount = (data: ProductDetails[]) => {
    return data?.length || 0;
  };

  const getItem = (data: ProductDetails[], index: number) => {
    return data[index];
  };

  const Item = (item: {product: ProductDetails}) => {
    const product: ProductDetails = item.product;
    return (
      <ProductsHorizontalComponent product={product} carButtonAdd={true} />
    );
  };
  return (
    <VirtualizedList
      showsHorizontalScrollIndicator={false}
      data={props.products}
      horizontal={true}
      initialNumToRender={4}
      renderItem={({item}) => <Item product={item} />}
      keyExtractor={(item, index) => {
        return `${item.categoryProductName || item.subcategoryProductName}_${
          item.id
        }_${index}`;
      }}
      getItemCount={getItemCount}
      getItem={getItem}
    />
  );
}
