import React, {useEffect, useState} from 'react';
import {Pressable, Text, View, StyleSheet} from 'react-native';
import LocationIcon from '../../../../assets/img/layout/ubicacion.svg';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import CarIcon from '../../../../assets/img/layout/carro-compras.svg';
import {createAddressViewModel} from '../../../presentation/view-models/address-view-model';
import {resetCurrentState} from '../../../data/redux/slices/address/address.slice';
import {resetReloadUserInfoAction} from '../../../data/redux/slices/user/login.slice';
import {resetShopingCartState} from '../../../data/redux/slices/products/cart/shoping-cart.slice';

export default function HeaderMainComponent() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [token, settoken] = useState<string | undefined>(undefined);

  const loginViewModel = createLoginViewModel(
    useSelector((state: StoreState) => state.login)
  );

  const addressViewModel = createAddressViewModel(
    useSelector((state: StoreState) => state.address)
  );

  const shoppingCartViewModel = createShoppingCartViewModel(
    useSelector((state: StoreState) => state.shoppingCart)
  );

  useEffect(() => {
    currentUserInfo();
    shoppingCartViewModel.getAllProductsInCar();
  }, []);

  useEffect(() => {
    if (loginViewModel.reloadUserInfo) {
      dispatch(resetCurrentState());
      dispatch(resetShopingCartState());
      currentUserInfo();
      shoppingCartViewModel.getAllProductsInCar();
      dispatch(resetReloadUserInfoAction(undefined));
    }
  }, [loginViewModel.reloadUserInfo]);

  useFocusEffect(
    React.useCallback(() => {
      currentUserInfo();
      shoppingCartViewModel.getAllProductsInCar();
    }, [])
  );

  const currentUserInfo = () => {
    async function getCurrentUser() {
      const user = await loginViewModel.getCurrentUserToken();
      settoken(user);
      if (user) {
        addressViewModel.getDefaultAddressByUser();
      }
    }
    getCurrentUser();
  };

  const onPressHeaderButton = (
    redirect: string,
    message: string,
    buttonText: string
  ) => {
    if (token) {
      navigation.navigate(redirect);
    } else {
      navigation.navigate('Login');
      const alertMessage: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: message,
        button: {
          color: '#005fab',
          text: buttonText,
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', alertMessage);
    }
  };

  return (
    <View style={HEADER_MAIN_STYLES.rowDirection}>
      <Pressable
        onPress={() =>
          onPressHeaderButton(
            'AddressModalSelect',
            'Para agregar una dirección de entrega es necesario que estés autenticado.',
            'Iniciar sesión'
          )
        }
        style={HEADER_MAIN_STYLES.addressContainer}>
        <View style={HEADER_MAIN_STYLES.iconContainer}>
          <LocationIcon />
        </View>
        <View style={HEADER_MAIN_STYLES.maxw80}>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={HEADER_MAIN_STYLES.addressText}>
            {addressViewModel.currentAddressByUser?.address ||
              'Agregar dirección'}
          </Text>
        </View>
        {addressViewModel.currentAddressByUser?.address && (
          <IconFontAwesome name="caret-down" size={12} color="#005EA9" />
        )}
      </Pressable>
      <Pressable
        onPress={() =>
          onPressHeaderButton(
            'ShoppinCartList',
            'Para agregar o ver productos en el carrito es necesario que estés autenticado.',
            'Iniciar sesión'
          )
        }
        style={HEADER_MAIN_STYLES.carContainer}>
        <View style={HEADER_MAIN_STYLES.badgeContainer}>
          <Text style={HEADER_MAIN_STYLES.badgeText}>
            {shoppingCartViewModel.shoppingCartData?.products.length || ''}
          </Text>
        </View>
        <CarIcon />
      </Pressable>
    </View>
  );
}
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {createLoginViewModel} from '../../view-models/login-view-model';
import {StoreState} from '../../../data/redux/reducers';
import {useDispatch, useSelector} from 'react-redux';
import {DialogMessage} from '../../../domain/entity/structure/user.model';
import {createShoppingCartViewModel} from '../../view-models/shopping-cart-view-model';

export const HEADER_MAIN_STYLES = StyleSheet.create({
  maxw80: {maxWidth: '80%'},
  addressContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {height: 15, width: 15, marginRight: 6},
  addressText: {
    color: '#1e1e1e',
    fontSize: 12,
    fontWeight: '700',
    marginRight: 3,
  },
  carContainer: {width: 30, aspectRatio: 1},
  badgeContainer: {
    position: 'absolute',
    right: 0,
    top: -7,
    left: 5,
    bottom: 0,
    zIndex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  badgeText: {fontSize: 10, color: '#ffed00', fontWeight: '700'},
  rowDirection: {
    flexDirection: 'row',
  },
});
