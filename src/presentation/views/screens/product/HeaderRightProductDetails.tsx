import {useIsFocused, useNavigation} from '@react-navigation/core';
import React, {useEffect} from 'react';
import {Pressable, View} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import {
  addOrRemoveFavorite,
  FavoriteProductsState,
  validateFavorite,
} from '../../../../data/redux/slices/products/favorites/favorites.product.slice';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {createLoginViewModel} from '../../../view-models/login-view-model';
import {DETAILPRODUCT_STYLES} from '../../styles/ScreensStyles';

export default function HeaderRightProductDetails() {
  const favoriteState: FavoriteProductsState = useSelector(
    (state: StoreState) => state.favorite
  );

  const loginViewModel = createLoginViewModel(
    useSelector((state: StoreState) => state.login)
  );

  const dispatch = useDispatch();

  const navigation = useNavigation();
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      dispatch(validateFavorite({productId: favoriteState.productTargetId}));
    }
  }, [isFocused]);

  return (
    <View style={DETAILPRODUCT_STYLES.mh10}>
      <Pressable
        onPress={() => {
          if (loginViewModel.userToken) {
            const isFavorite = favoriteState.isProductFavorite;
            dispatch(
              addOrRemoveFavorite({
                product: {
                  productId: favoriteState.productTargetId,
                },
                isFavorite: isFavorite,
              })
            );
          } else {
            navigation.navigate('Login');
            const logininfo: DialogMessage = {
              title: {
                text: 'Atención',
                color: '#005fab',
              },
              message:
                'Para agregar un producto favorito es necesario que estés autenticado.',
              button: {
                color: '#005fab',
                text: 'Iniciar sesión',
              },
              pop: 1,
            };
            navigation.navigate('DialogModal', logininfo);
          }
        }}>
        {favoriteState.isProductFavorite ? (
          <MaterialCommunityIcons
            size={25}
            name="cards-heart"
            color="#DF040B"
          />
        ) : (
          <MaterialCommunityIcons
            size={25}
            name="heart-outline"
            color="#005EA9"
          />
        )}
      </Pressable>
    </View>
  );
}
