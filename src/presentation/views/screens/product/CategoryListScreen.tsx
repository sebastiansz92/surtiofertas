import React from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import {CategoryProductViewModel} from '../../../view-models/category-product-view-model';
import Categories from '../../components/Categories.component';
import {CATEGORIES_STYLES} from '../../styles/categories.styles';
import {CATEGORYLIST_STYLES} from '../../styles/ScreensStyles';

export default function CategoryListScreen({route}) {
  return (
    <SafeAreaView style={CATEGORIES_STYLES.flex_1}>
      <View style={CATEGORYLIST_STYLES.container}>
        <Text style={CATEGORYLIST_STYLES.titleCategories}>Categorías</Text>
        <View style={CATEGORIES_STYLES.flex_1}>
          <ScrollView
            style={CATEGORIES_STYLES.flex_1}
            contentContainerStyle={CATEGORIES_STYLES.itemCenter}>
            <View style={CATEGORIES_STYLES.categoryListItem}>
              <View
                style={[
                  CATEGORIES_STYLES.flex_1,
                  CATEGORIES_STYLES.itemCenter,
                ]}>
                {route.params.map(
                  (value: CategoryProductViewModel, key: number) => {
                    return (
                      key % 2 === 0 && (
                        <View
                          key={`product-${key}`}
                          style={[{width: '100%', padding: 10}]}>
                          <Categories category={value} key={key} />
                        </View>
                      )
                    );
                  }
                )}
              </View>
              <View
                style={[
                  CATEGORIES_STYLES.flex_1,
                  CATEGORIES_STYLES.itemCenter,
                ]}>
                {route.params.map(
                  (value: CategoryProductViewModel, key: number) => {
                    return (
                      key % 2 !== 0 && (
                        <View
                          key={`product-${key}`}
                          style={{width: '100%', padding: 10}}>
                          <Categories category={value} key={key} />
                        </View>
                      )
                    );
                  }
                )}
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
}
