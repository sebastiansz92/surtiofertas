import React, {useEffect, useRef} from 'react';
import {Animated, ImageBackground, Pressable, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ENVIRONMENT} from '../../../../data/constants/api.constants';
import {StoreState} from '../../../../data/redux/reducers';
import {updateProductsByCategoryData} from '../../../../data/redux/slices/products/category/category.product.slice';
import {createCategoryProductViewModel} from '../../../view-models/category-product-view-model';
import VirtualizedProductListComponent from '../../components/VirtualizedProductListComponent';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/core';
import {CATEGORIES_STYLES} from '../../styles/categories.styles';

export default function CategoryDetails({route}) {
  const categoryViewModel = createCategoryProductViewModel(
    useSelector((state: StoreState) => state.categoryProduct)
  );

  const dispatch = useDispatch();

  useEffect(() => {
    categoryViewModel.getProductsByCategory(route.params.category.id);
    return () => {
      dispatch(updateProductsByCategoryData([]));
    };
  }, []);

  const CateogryProductItem = ({item}) => {
    return (
      <View>
        <View
          style={CATEGORIES_STYLES.productItemContainer}
          key={`${route.params.category.id}_${item.categoryName}`}>
          <Text style={CATEGORIES_STYLES.productItemText}>
            {item.categoryName}
          </Text>
          <VirtualizedProductListComponent
            products={item.categoryContent.content}
          />
        </View>
      </View>
    );
  };

  const categoryScroll = useRef(new Animated.Value(0)).current;
  const heightY = categoryScroll.interpolate({
    inputRange: [0, 65, 130, 200, 250],
    outputRange: [250, 200, 150, 100, 50],
    extrapolate: 'clamp',
  });

  const bottomTitle = categoryScroll.interpolate({
    inputRange: [249, 250],
    outputRange: [20, 5],
    extrapolate: 'clamp',
  });

  const navigation = useNavigation();
  return (
    <View style={CATEGORIES_STYLES.flex_1}>
      <View style={CATEGORIES_STYLES.arrowLeft}>
        <Pressable onPress={() => navigation.goBack()}>
          <MaterialCommunityIcons name="arrow-left" size={20} color={'white'} />
        </Pressable>
      </View>
      {categoryViewModel.productsByCategoryData.length > 0 && (
        <Animated.FlatList
          stickyHeaderIndices={[0]}
          ListHeaderComponent={
            <Animated.View
              style={{
                height: heightY,
                width: '100%',
              }}>
              <ImageBackground
                style={CATEGORIES_STYLES.full}
                imageStyle={{resizeMode: 'cover'}}
                source={{
                  uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${route.params.category.categoryimage64}`,
                }}>
                <Animated.View
                  style={[
                    CATEGORIES_STYLES.titleCategory,
                    {
                      bottom: bottomTitle,
                    },
                  ]}>
                  <View style={CATEGORIES_STYLES.titleCategoryStyle}>
                    <Text style={CATEGORIES_STYLES.titleCategoryText}>
                      {route.params.category.name}
                    </Text>
                  </View>
                </Animated.View>
              </ImageBackground>
            </Animated.View>
          }
          data={categoryViewModel.productsByCategoryData}
          renderItem={({item}) => <CateogryProductItem item={item} />}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    y: categoryScroll,
                  },
                },
              },
            ],
            {useNativeDriver: false}
          )}
          keyExtractor={(item, index) => {
            return `${item.categoryName}_${index}`;
          }}
        />
      )}
    </View>
  );
}
