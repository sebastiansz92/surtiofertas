import React, {useEffect, useState} from 'react';
import {DETAILPRODUCT_STYLES} from '../../styles/ScreensStyles';
import {useDispatch, useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import store from '../../../../data/redux/store';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {createValidateCodeViewModel} from '../../../view-models/validatecode-view-model';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  ScrollView,
  View,
  Text,
  Image,
  SafeAreaView,
  Pressable,
  Platform,
  ToastAndroid,
  ActionSheetIOS,
} from 'react-native';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {createReduxValidateCodeInterface} from '../../../../data/redux/slices/user/redux-validate-code-interface';
import {createValidateCodeInteractor} from '../../../../domain/interactors/use-cases/user/validatecode-usecase';
import {ENVIRONMENT} from '../../../../data/constants/api.constants';
import {
  addOrRemoveFavorite,
  FavoriteProductsState,
} from '../../../../data/redux/slices/products/favorites/favorites.product.slice';
import {createLoginViewModel} from '../../../view-models/login-view-model';
import CounterComponent from '../../components/CounterComponent';
import ProductEntity from '../../../../domain/entity/model/ProductEntity';
import {ProductDetails} from '../../../../domain/entity/structure/category-product.model';
import {
  ShoppingCartState,
  resetShopingCartState,
} from '../../../../data/redux/slices/products/cart/shoping-cart.slice';
import {createShoppingCartViewModel} from '../../../view-models/shopping-cart-view-model';

const ValidateCodeInterface = createReduxValidateCodeInterface(store.dispatch);
const ValidateCodeInteractor = createValidateCodeInteractor(
  ValidateCodeInterface
);

const productEntity = new ProductEntity();

export default function DetailProductScreen({navigation: {navigate}, route}) {
  const validateCodeViewModel = createValidateCodeViewModel(
    useSelector((state: StoreState) => state.validateCode)
  );

  const loginViewModel = createLoginViewModel(
    useSelector((state: StoreState) => state.login)
  );

  const shoppinCartViewModel = createShoppingCartViewModel(
    useSelector((state: StoreState) => state.shoppingCart)
  );

  const product: ProductDetails = route.params.product;
  const favoriteState: FavoriteProductsState = useSelector(
    (state: StoreState) => state.favorite
  );

  const shoppingCartState: ShoppingCartState = useSelector(
    (state: StoreState) => state.shoppingCart
  );

  const dispatch = useDispatch();
  const isfocused = useIsFocused();

  useEffect(() => {
    if (validateCodeViewModel.errorValidation !== undefined) {
      const validateCodeError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: validateCodeViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigate('DialogModal', validateCodeError);
    }

    if (shoppingCartState.productWasAddedToCart && isfocused) {
      dispatch(resetShopingCartState());
      navigate('ShoppinCartProductAdded', {
        product,
        quantity: Math.floor(subtotal / (product?.priceOffered || 1)),
      });
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      return () => {
        validateCodeViewModel.resetValidation(ValidateCodeInteractor);
      };
    }, [])
  );
  const [subtotal, setsubtotal] = useState(product.priceOffered || 0);

  const updateAmount = (currentAmount: number) => {
    if (
      product.priceOffered &&
      product.stock &&
      currentAmount <= product.stock
    ) {
      currentAmount > 0 && setsubtotal(product.priceOffered * currentAmount);
    } else {
      if (Platform.OS === 'ios') {
        ActionSheetIOS.showActionSheetWithOptions(
          {
            options: ['Ok'],
            message: 'No hay mas productos similares en el inventario',
            cancelButtonIndex: 0,
          },
          (buttonIndex) => {
            console.log(buttonIndex);
          }
        );
      } else {
        ToastAndroid.show(
          'No hay mas productos similares en el inventario',
          ToastAndroid.SHORT
        );
      }
    }
  };

  return (
    <SafeAreaView style={DETAILPRODUCT_STYLES.container}>
      <View style={DETAILPRODUCT_STYLES.background}>
        <ScrollView
          contentContainerStyle={DETAILPRODUCT_STYLES.scrollContainer}>
          <Text style={DETAILPRODUCT_STYLES.titleProduct}>{product.name}</Text>
          <Text style={DETAILPRODUCT_STYLES.netContent}>{product.weight}</Text>
          <Image
            style={DETAILPRODUCT_STYLES.imageProduct}
            source={{
              uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${product.image_url}`,
            }}
            resizeMode="contain"
          />
          <View style={DETAILPRODUCT_STYLES.containerAmountProduct}>
            <View style={DETAILPRODUCT_STYLES.containPrice}>
              <Text style={DETAILPRODUCT_STYLES.price}>
                {productEntity.formatPrice(product?.priceOffered || 0)}
              </Text>
            </View>
            <CounterComponent
              limit={product.stock}
              onUpdateAmount={updateAmount}
            />
          </View>
          <Text style={DETAILPRODUCT_STYLES.descriptionProduct}>
            {product.description}
          </Text>
          <Pressable
            onPress={() => {
              if (loginViewModel.userToken) {
                const isFavorite = favoriteState.isProductFavorite;
                dispatch(
                  addOrRemoveFavorite({
                    product: {
                      productId: favoriteState.productTargetId,
                    },
                    isFavorite: isFavorite,
                  })
                );
              } else {
                navigate('Login');
                const logininfo: DialogMessage = {
                  title: {
                    text: 'Atención',
                    color: '#005fab',
                  },
                  message:
                    'Para agregar un producto favorito es necesario que estés autenticado.',
                  button: {
                    color: '#005fab',
                    text: 'Iniciar sesión',
                  },
                  pop: 1,
                };
                navigate('DialogModal', logininfo);
              }
            }}
            style={DETAILPRODUCT_STYLES.containerOptions}>
            {favoriteState.isProductFavorite ? (
              <MaterialCommunityIcons
                size={25}
                name="cards-heart"
                color="#DF040B"
              />
            ) : (
              <MaterialCommunityIcons
                size={25}
                name="heart-outline"
                color="#005EA9"
              />
            )}
            <Text style={[DETAILPRODUCT_STYLES.addFavorites]}>
              {favoriteState.isProductFavorite
                ? 'Remover de favoritos'
                : 'Añadir a favoritos'}
            </Text>
          </Pressable>
        </ScrollView>
        <View style={DETAILPRODUCT_STYLES.formContainer}>
          <View style={DETAILPRODUCT_STYLES.buttonsContainer}>
            <Pressable
              onPress={() => {
                if (loginViewModel.userToken) {
                  shoppinCartViewModel.addProductToCar({
                    product,
                    quantity: Math.floor(
                      subtotal / (product?.priceOffered || 1)
                    ),
                  });
                } else {
                  navigate('Login');
                  const logininfo: DialogMessage = {
                    title: {
                      text: 'Atención',
                      color: '#005fab',
                    },
                    message:
                      'Para agregar este producto al carrito de compras es necesario que estés autenticado.',
                    button: {
                      color: '#005fab',
                      text: 'Iniciar sesión',
                    },
                    pop: 1,
                  };
                  navigate('DialogModal', logininfo);
                }
              }}
              style={DETAILPRODUCT_STYLES.addProductBtn}>
              <View style={DETAILPRODUCT_STYLES.containerAddProduct}>
                <View style={DETAILPRODUCT_STYLES.containAdd}>
                  <Text style={DETAILPRODUCT_STYLES.textAddButton}>
                    Agregar
                  </Text>
                </View>
                <View style={DETAILPRODUCT_STYLES.containPriceButton}>
                  <Text style={DETAILPRODUCT_STYLES.priceButton}>
                    {productEntity.formatPrice(subtotal || 0)}
                  </Text>
                </View>
              </View>
            </Pressable>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}
