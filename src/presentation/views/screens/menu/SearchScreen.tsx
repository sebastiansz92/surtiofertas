import React, {useEffect, useRef} from 'react';
import {Animated, Pressable, Text, View, TextInput} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import {updateProductsBySearchData} from '../../../../data/redux/slices/products/category/category.product.slice';
import {createCategoryProductViewModel} from '../../../view-models/category-product-view-model';
import VirtualizedProductListComponent from '../../components/VirtualizedProductListComponent';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {createReduxCategoryProductInterface} from '../../../../data/redux/slices/products/category/redux-category-product-interface';
import store from '../../../../data/redux/store';
import {createCategoryProductInteractor} from '../../../../domain/interactors/use-cases/products/category-product-usecase';
import {SEARCH_STYLES} from '../../styles/ScreensStyles';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {GLOBAL_STYLES} from '../../styles/common.styles';

const searchInterface = createReduxCategoryProductInterface(store.dispatch);
const searchInteractor = createCategoryProductInteractor(searchInterface);

export default function SearchScreen() {
  const categoryViewModel = createCategoryProductViewModel(
    useSelector((state: StoreState) => state.categoryProduct)
  );

  const textInputRef = React.useRef<TextInput>(null);

  const dispatch = useDispatch();

  useEffect(() => {
    if (categoryViewModel?.productsByCategorySearchData?.length === 0) {
      const searchWarning: DialogMessage = {
        title: {
          text: 'Por favor realiza una búsqueda',
          color: '#005fab',
        },
        message: 'El producto buscado no ha sido encontrado',
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', searchWarning);
    }
  }, [categoryViewModel.productsByCategorySearchData]);

  const getSearchProducts = (search: string) => {
    categoryViewModel.getProductsBySearch(search);
    return () => {
      dispatch(updateProductsBySearchData([]));
    };
  };

  const CateogryProductItem = ({item}) => {
    return (
      <View>
        <View
          style={SEARCH_STYLES.containerCategoryName}
          key={`${item.categoryContent.content[0].id}_${item.categoryName}`}>
          <Text style={SEARCH_STYLES.categoryName}>{item.categoryName}</Text>
          <VirtualizedProductListComponent
            products={item.categoryContent.content}
          />
        </View>
      </View>
    );
  };

  const categoryScroll = useRef(new Animated.Value(0)).current;
  const navigation = useNavigation();

  return (
    <View style={SEARCH_STYLES.flex_1}>
      <Animated.FlatList
        stickyHeaderIndices={[0]}
        ListHeaderComponent={
          <Animated.View style={SEARCH_STYLES.containerSearch}>
            <Animated.View style={SEARCH_STYLES.containerSearchItems}>
              <View style={SEARCH_STYLES.widthArroyLeft}>
                <Pressable
                  style={SEARCH_STYLES.pressableLeft}
                  onPress={() => navigation.goBack()}>
                  <Icon name="arrow-left" size={20} color="#005EA8" />
                </Pressable>
              </View>
              <View style={SEARCH_STYLES.widthInputSearch}>
                <View
                  style={[
                    GLOBAL_STYLES.inputContainer,
                    SEARCH_STYLES.inputStyle,
                  ]}>
                  <View
                    style={[
                      {
                        backgroundColor:
                          categoryViewModel.searchIconBg || '#bdbdbd',
                      },
                      GLOBAL_STYLES.iconContainer,
                    ]}>
                    <Icon name="search" size={20} color="white" />
                  </View>
                  <TextInput
                    ref={textInputRef}
                    placeholder="Buscador de productos..."
                    placeholderTextColor="#005fab"
                    onChangeText={(value: string) =>
                      categoryViewModel.onChangeInput(
                        searchInteractor,
                        'search',
                        value
                      )
                    }
                    style={[GLOBAL_STYLES.inputStyle]}
                  />
                </View>
                <Pressable
                  onPress={() => textInputRef.current?.clear()}
                  style={SEARCH_STYLES.xIcon}>
                  <Icon name="eraser" size={25} color="#616161" />
                </Pressable>
              </View>
              <View style={SEARCH_STYLES.widthSearch}>
                <Pressable
                  style={SEARCH_STYLES.pressableSearch}
                  onPress={() => getSearchProducts(categoryViewModel.search)}>
                  <Icon name="search" size={20} color="#005EA8" />
                </Pressable>
              </View>
            </Animated.View>
          </Animated.View>
        }
        data={categoryViewModel.productsByCategorySearchData}
        renderItem={({item}) => <CateogryProductItem item={item} />}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: categoryScroll,
                },
              },
            },
          ],
          {useNativeDriver: false}
        )}
        keyExtractor={(item, index) => {
          return `${item.categoryName}_${index}`;
        }}
      />
    </View>
  );
}
