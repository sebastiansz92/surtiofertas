import React, {useEffect, useState} from 'react';
import {
  Animated,
  Dimensions,
  FlatList,
  Pressable,
  SafeAreaView,
  ScrollView,
  SectionList,
  Text,
  View,
} from 'react-native';
import SliderComponent from '../../components/SliderComponent';
import {createOfferViewModel} from '../../../view-models/offer-view-model';
import {useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import {
  CategoryProductViewModel,
  createCategoryProductViewModel,
} from '../../../view-models/category-product-view-model';
import {CategorySupperTabs} from '../../../../domain/entity/structure/category-product.model';
import Categories from '../../components/Categories.component';
import {
  CATEGORIES_STYLES,
  HOME_STYLES,
  SECTION_BUSINESS_STYLES,
} from '../../styles/categories.styles';
import SectionBusinessCategoryComponent from '../../components/SectionBusinessCategoryComponent.component';
import {createInvoiceViewModel} from '../../../view-models/invoice-view-model';
import {useFocusEffect} from '@react-navigation/core';
import {LAYOUT_CT} from '../../../../data/constants/layout.constants';

export default function HomeScreen({navigation}) {
  const [categories, setCategories] = useState<any[]>([]);

  const offerViewModel = createOfferViewModel(
    useSelector((state: StoreState) => state.offer)
  );
  const categoryViewModel = createCategoryProductViewModel(
    useSelector((state: StoreState) => state.categoryProduct)
  );

  const invoiceViewModel = createInvoiceViewModel(
    useSelector((state: StoreState) => state.invoice)
  );

  useEffect(() => {
    if (categoryViewModel.allCategoriesProducts?.length > 0) {
      setCategories(categoryViewModel.allCategoriesProducts);
    }
  }, [categoryViewModel.allCategoriesProducts]);

  useEffect(() => {
    offerViewModel.getMarketOffers();
    categoryViewModel.getCategorySupper();
    categoryViewModel.getAllCategoriesProduct();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      invoiceViewModel.getPendingInvoicesByUser();
    }, [])
  );

  const [enabledAbsoluteTabs, setenabledAbsoluteTabs] = useState(false);

  let flatListRef: FlatList<number> | null;
  let tabSectionRef: SectionList<number, CategorySupperTabs> | null;

  const changeToTab = (index: number) => {
    if (index === categoryViewModel.businessCategoryData.length - 1) {
      let offset = 360;
      categoryViewModel.businessCategoryData.forEach((elm) => {
        offset = offset + elm.productsGroupedBy.length * 200;
      });
      flatListRef?.scrollToOffset({
        animated: true,
        offset: offset,
      });
    } else {
      flatListRef?.scrollToOffset({
        animated: true,
        offset:
          index === 0
            ? 0
            : categoryViewModel.businessCategoryData[index - 1]
                .productsGroupedBy.length *
                290 +
              300,
      });
    }
  };

  const handleScroll = (event: any) => {
    let offset = 0;
    categoryViewModel.businessCategoryData.forEach((elm) => {
      offset = offset + elm.productsGroupedBy.length * 125;
    });
    if (event.nativeEvent.contentOffset.y > 460) {
      offset = Math.floor(event.nativeEvent.contentOffset.y / offset);
      if (
        categoryViewModel.businessTabsData
          .map((elm) => elm.selected)
          .indexOf(true) !== offset
      ) {
        categoryViewModel.setSelectedTab(offset);
        tabSectionRef &&
          tabSectionRef.scrollToLocation({
            animated: true,
            sectionIndex: offset,
            itemIndex: 0,
          });
      }
      setenabledAbsoluteTabs(true);
    } else {
      setenabledAbsoluteTabs(false);
    }
  };

  const {width} = Dimensions.get('window');
  return (
    <SafeAreaView style={HOME_STYLES.flex_1}>
      <View style={HOME_STYLES.flex_1}>
        {enabledAbsoluteTabs && (
          <SectionList
            onLayout={() => {
              tabSectionRef &&
                tabSectionRef.scrollToLocation({
                  animated: true,
                  sectionIndex: categoryViewModel.businessTabsData
                    .map((elm) => elm.selected)
                    .indexOf(true),
                  itemIndex: 0,
                });
            }}
            onScrollToIndexFailed={(error) => {
              tabSectionRef?.scrollToLocation({
                sectionIndex: 0,
                itemIndex: 0,
              });
            }}
            style={HOME_STYLES.businessTab}
            showsHorizontalScrollIndicator={false}
            ref={(ref) => (tabSectionRef = ref)}
            sections={categoryViewModel.businessTabsData}
            horizontal={true}
            keyExtractor={(item, index) => item.toString() + index}
            renderSectionHeader={({section}) => {
              return (
                <Pressable
                  onPress={() => {
                    categoryViewModel.setSelectedTab(section.index);
                    changeToTab(section.index);
                    tabSectionRef &&
                      tabSectionRef.scrollToLocation({
                        animated: true,
                        sectionIndex: section.index,
                        itemIndex: 0,
                      });
                  }}
                  style={[
                    SECTION_BUSINESS_STYLES.sectionHeader,
                    {
                      borderBottomWidth: section.selected ? 3 : 0,
                      borderBottomColor: section.selected ? '#de0209' : 'white',
                    },
                  ]}>
                  <Text
                    style={{
                      color: section.selected ? '#616161' : '#bdbdbd',
                      fontSize: 16,
                      fontWeight: section.selected ? 'bold' : '300',
                    }}>
                    {section.title}
                  </Text>
                </Pressable>
              );
            }}
          />
        )}

        <FlatList
          ref={(ref) => (flatListRef = ref)}
          keyExtractor={() => 'categoryBusinnesList'}
          onScroll={handleScroll}
          style={HOME_STYLES.flex_1}
          data={[0]}
          showsVerticalScrollIndicator={false}
          nestedScrollEnabled={false}
          renderItem={() => {
            return (
              <View style={HOME_STYLES.sectionBusinessContainer}>
                <SectionBusinessCategoryComponent
                  onChangeTabSection={(index) => changeToTab(index)}
                  categoryViewModel={categoryViewModel}
                  scrollToSection={categoryViewModel.businessTabsData
                    .map((elm) => elm.selected)
                    .indexOf(true)}
                  businessCategoryData={categoryViewModel.businessCategoryData}
                />
              </View>
            );
          }}
          ListHeaderComponent={
            <>
              <Animated.View
                style={[
                  HOME_STYLES.sliderPromos,
                  {
                    height:
                      invoiceViewModel?.currentInvoices?.length > 0
                        ? LAYOUT_CT.promoSlidersHeightWithInvoices
                        : LAYOUT_CT.promoSlidersHeightWithOutInvoices,
                  },
                ]}>
                {invoiceViewModel?.currentInvoices?.length > 0 && (
                  <Pressable
                    onPress={() => navigation.navigate('InvoiceList')}
                    style={{
                      height: 40,
                      paddingHorizontal: 5,
                      marginBottom: 10,
                      width: '100%',
                    }}>
                    <View
                      style={{
                        backgroundColor: '#DF040B',
                        borderRadius: 5,
                        alignItems: 'center',
                        height: '100%',
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          color: '#FFF',
                          marginLeft: 20,
                          fontSize: 16,
                          fontWeight: '700',
                          flex: 1,
                        }}>
                        Pedidos activos
                      </Text>
                      <View
                        style={{
                          marginHorizontal: 10,
                          width: 20,
                          height: 20,
                          backgroundColor: 'white',
                          borderRadius: 10,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text style={{color: '#DF040B'}}>
                          {invoiceViewModel.currentInvoices?.length}
                        </Text>
                      </View>
                    </View>
                  </Pressable>
                )}

                <SliderComponent
                  screen={'home'}
                  slideElements={offerViewModel.offerProducts}
                  wrap={true}
                  rounded={true}
                  enabledControls={false}
                  stylesSlide={{paddingHorizontal: 5, width: width - 20}}
                />
              </Animated.View>
              <View style={CATEGORIES_STYLES.containerTitles}>
                <View style={CATEGORIES_STYLES.categoryTitle}>
                  <Text style={CATEGORIES_STYLES.categoryText}>Categorías</Text>
                </View>
                <View style={CATEGORIES_STYLES.seeAllTitle}>
                  <Pressable
                    onPress={() =>
                      navigation.navigate('CategoryList', categories)
                    }>
                    <Text style={CATEGORIES_STYLES.seeAllText}>ver todo</Text>
                  </Pressable>
                </View>
              </View>
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                style={HOME_STYLES.p10}>
                {categories.map(
                  (
                    value: CategoryProductViewModel,
                    key: string | number | null | undefined
                  ) => {
                    return (
                      <View key={key} style={HOME_STYLES.categoryFrame}>
                        <Categories category={value} />
                      </View>
                    );
                  }
                )}
              </ScrollView>
            </>
          }
        />
      </View>
    </SafeAreaView>
  );
}
