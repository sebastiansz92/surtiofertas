import {useFocusEffect} from '@react-navigation/core';
import React, {useEffect, useState} from 'react';
import {FlatList, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import {
  FavoriteProductsState,
  getFavoriteProductsByUser,
} from '../../../../data/redux/slices/products/favorites/favorites.product.slice';
import {createLoginViewModel} from '../../../view-models/login-view-model';
import ProductsHorizontalComponent from '../../components/ProductsHorizontalComponent';
import {FAVORITES_STYLES} from '../../styles/ScreensStyles';

import {createInvoiceViewModel} from '../../../view-models/invoice-view-model';
import ProductsVerticalComponent from '../../components/ProductsVerticalComponent';

export default function FavoritesScreen({navigation}) {
  const dispatch = useDispatch();
  const favoriteState: FavoriteProductsState = useSelector(
    (state: StoreState) => state.favorite
  );

  const loginViewModel = createLoginViewModel(
    useSelector((state: StoreState) => state.login)
  );

  const invoiceViewModel = createInvoiceViewModel(
    useSelector((state: StoreState) => state.invoice)
  );

  useEffect(() => {
    invoiceViewModel.getRecentOrdersProductsByUser();
    currentUserInfo();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      currentUserInfo();
    }, [])
  );

  const currentUserInfo = () => {
    async function getCurrentUser() {
      const user = await loginViewModel.getCurrentUserToken();
      if (user) {
        dispatch(getFavoriteProductsByUser());
      }
    }
    getCurrentUser();
  };

  const RecentOrderItem = ({orderItem}: any) => {
    return (
      <>
        <ProductsVerticalComponent
          product={orderItem}
          key={`ph-${orderItem.id}`}
        />
      </>
    );
  };

  return (
    <SafeAreaView style={FAVORITES_STYLES.container}>
      <FlatList
        renderItem={({item}) => <RecentOrderItem orderItem={item} />}
        keyExtractor={(item, index) => {
          return `${item.id}_${index}`;
        }}
        data={invoiceViewModel.recentOrdersByUserData}
        ItemSeparatorComponent={() => (
          <View
            style={{backgroundColor: '#e0e0e0', height: 7, width: '100%'}}
          />
        )}
        ListHeaderComponent={
          <>
            <View style={FAVORITES_STYLES.categoryName}>
              <Text style={FAVORITES_STYLES.imageText}>LISTA DE FAVORITOS</Text>
            </View>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal={true}>
              {favoriteState.favoriteProductsByUser?.length > 0 ? (
                favoriteState.favoriteProductsByUser.map((product, key) => {
                  return (
                    <ProductsHorizontalComponent
                      product={product}
                      carButtonAdd={false}
                      key={`ph-${key}`}
                    />
                  );
                })
              ) : (
                <View style={FAVORITES_STYLES.emptyContainer}>
                  <Text style={FAVORITES_STYLES.emptyMessage}>
                    Tu lista de favoritos se encuentra vacía
                  </Text>
                  <Text style={FAVORITES_STYLES.supcationEmptyMessage}>
                    Te invitamos a consultar nuestra variedad de productos
                  </Text>
                </View>
              )}
            </ScrollView>
            <View style={FAVORITES_STYLES.categoryName}>
              <Text style={FAVORITES_STYLES.imageText}>PEDIDOS RECIENTES</Text>
            </View>
            {invoiceViewModel.recentOrdersByUserData?.length === 0 && (
              <View style={FAVORITES_STYLES.emptyContainer}>
                <Text style={FAVORITES_STYLES.emptyMessage}>
                  Tu lista de pedidos recientes se encuentra
                </Text>
                <Text style={FAVORITES_STYLES.emptyMessage}>vacía</Text>
                <Text style={FAVORITES_STYLES.supcationEmptyMessage}>
                  Te invitamos a consultar nuestra variedad de productos
                </Text>
              </View>
            )}
          </>
        }
      />
    </SafeAreaView>
  );
}
