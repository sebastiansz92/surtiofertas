import React, {useEffect} from 'react';
import {Text, View, ImageBackground} from 'react-native';
import {ENVIRONMENT} from '../../../../data/constants/api.constants';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomButton from '../../components/CustomButton';
import {createShoppingCartViewModel} from '../../../view-models/shopping-cart-view-model';
import {useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import ProductEntity from '../../../../domain/entity/model/ProductEntity';
import {SHOPPINGCART_STYLES} from '../../styles/categories.styles';

const productEntity = new ProductEntity();

export default function ShoppinCartProductAddedScreen({route, navigation}) {
  const productInCart = route.params;
  const shoppingCartViewModel = createShoppingCartViewModel(
    useSelector((state: StoreState) => state.shoppingCart)
  );
  useEffect(() => {
    shoppingCartViewModel.getAllProductsInCar();
  }, []);

  return (
    <View style={SHOPPINGCART_STYLES.parent_container}>
      <View style={SHOPPINGCART_STYLES.child_container}>
        <View style={SHOPPINGCART_STYLES.row}>
          <ImageBackground
            imageStyle={SHOPPINGCART_STYLES.imageBorder}
            style={SHOPPINGCART_STYLES.imageStyle}
            resizeMode="contain"
            source={{
              uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${productInCart.product.image_url}`,
              // uri:
              //   'https://www.latercera.com/resizer/XVAJuSBTEM_A5X37YU6V1FopWFk=/900x600/smart/arc-anglerfish-arc2-prod-copesa.s3.amazonaws.com/public/MCTC7ABLMNCAZOQH2JA6DZZFT4.jpg',
            }}>
            <View style={SHOPPINGCART_STYLES.checkElement}>
              <MaterialCommunityIcons
                name="check-bold"
                size={15}
                color={'white'}
              />
            </View>
          </ImageBackground>
          <View style={SHOPPINGCART_STYLES.flex_1}>
            <Text style={SHOPPINGCART_STYLES.messagePoductAdded}>
              Has añadido a tu carrito:
            </Text>
            <View style={SHOPPINGCART_STYLES.productDetailsContainer}>
              <Text style={SHOPPINGCART_STYLES.unityText}>
                {productInCart.quantity > 1
                  ? productInCart.quantity + ' unidades'
                  : productInCart.quantity + ' unidad'}
              </Text>
              <Text
                style={SHOPPINGCART_STYLES.productName}
                ellipsizeMode="clip"
                numberOfLines={2}>
                {productInCart.product.name}
              </Text>
            </View>
          </View>
        </View>
        <View style={SHOPPINGCART_STYLES.productsInCartContainer} />
        <Text style={SHOPPINGCART_STYLES.productsInCartLabel}>
          {shoppingCartViewModel.shoppingCartData?.products &&
          shoppingCartViewModel.shoppingCartData?.products.length > 1
            ? shoppingCartViewModel.shoppingCartData?.products.length +
              ' productos en tu carrito: '
            : shoppingCartViewModel.shoppingCartData?.products.length +
              ' producto en tu carrito: '}
          <Text style={SHOPPINGCART_STYLES.bold}>
            {productEntity.formatPrice(
              shoppingCartViewModel.shoppingCartData?.subtotalProductsInCar || 0
            )}
          </Text>
        </Text>
        <CustomButton
          colorText="white"
          colorButton="blue"
          styleText={SHOPPINGCART_STYLES.seeCarListText}
          styleButton={SHOPPINGCART_STYLES.seeCarListBtn}
          textButton="VER CARRITO"
          buttonProps={{
            title: 'VER CARRITO',
            onPress: () => navigation.navigate('ShoppinCartList'),
          }}
        />

        <CustomButton
          colorText="blue"
          colorButton="white"
          styleText={SHOPPINGCART_STYLES.seeCarListText}
          styleButton={SHOPPINGCART_STYLES.addMoreProducts}
          textButton="Agregar mas productos"
          buttonProps={{
            title: 'Pedir de nuevo',
            onPress: () => navigation.pop(2),
          }}
        />
      </View>
    </View>
  );
}
