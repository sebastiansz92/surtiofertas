import React, {useEffect, useRef, useState} from 'react';
import {
  ActionSheetIOS,
  Alert,
  Animated,
  Easing,
  FlatList,
  ImageBackground,
  Platform,
  Pressable,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ENVIRONMENT} from '../../../../data/constants/api.constants';
import {StoreState} from '../../../../data/redux/reducers';
import {
  addProductToCar,
  resetShowMessageRemoved,
} from '../../../../data/redux/slices/products/cart/shoping-cart.slice';
import ProductEntity from '../../../../domain/entity/model/ProductEntity';
import {
  ProductDetails,
  ShoppingCartDataState,
} from '../../../../domain/entity/structure/category-product.model';
import {createShoppingCartViewModel} from '../../../view-models/shopping-cart-view-model';
import CounterComponent from '../../components/CounterComponent';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CustomButton from '../../components/CustomButton';
import {SHOPPINGCART_STYLES} from '../../styles/categories.styles';
import {useNavigation} from '@react-navigation/core';

const productEntity = new ProductEntity();

export default function ShoppingCartListScreen() {
  const shoppingCartViewModel = createShoppingCartViewModel(
    useSelector((state: StoreState) => state.shoppingCart)
  );

  const [cartProducts, setcartProducts] = useState<ShoppingCartDataState>([]);
  const dispatch = useDispatch();

  useEffect(() => {
    if (shoppingCartViewModel.showProductRemovedMessage) {
      fadeIn();
      setTimeout(() => {
        fadeOut();
      }, 2000);
      dispatch(resetShowMessageRemoved(false));
    }
    setcartProducts(shoppingCartViewModel.shoppingCartData);
  }, [
    shoppingCartViewModel.showProductRemovedMessage,
    shoppingCartViewModel.shoppingCartData,
  ]);

  const navigation = useNavigation();

  const updateAmount = (amount: number, product: ProductDetails) => {
    if (amount === 0 && Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ['Eliminar del carrito', 'Cancelar'],
          message: '¿Deseas eliminar el producto del carrito?',
          cancelButtonIndex: 1,
        },
        (buttonIndex) => {
          if (buttonIndex === 1) {
            removeProductInCart(product.id || -1);
          }
        }
      );
    } else if (amount === 0 && Platform.OS === 'android') {
      Alert.alert(
        '',
        '¿Deseas eliminar el producto del carrito?',
        [
          {
            text: 'Eliminar',
            onPress: () => removeProductInCart(product.id || -1),
            style: 'default',
          },
          {
            text: 'Cancelar',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: true}
      );
    } else {
      let currentProducts = [...cartProducts.products];
      const productPosition = currentProducts
        ?.map((elm) => elm.id)
        .indexOf(product.id);
      if (productPosition >= 0) {
        let currentProduct = {...currentProducts[productPosition]};
        currentProduct.quantity = amount;
        currentProducts[productPosition] = currentProduct;
        setcartProducts({
          products: currentProducts,
          subtotalProductsInCar: productEntity.getSubtotalValue(
            currentProducts
          ),
        });
        dispatch(
          addProductToCar({
            product: currentProduct,
            quantity: currentProduct.quantity,
            rewrite: true,
          })
        );
      }
    }
  };

  const removeProductInCart = (productId: number) => {
    shoppingCartViewModel.removeProductInCar(productId);
  };

  const ProductInCart = (item) => {
    return (
      <View style={SHOPPINGCART_STYLES.cartListContainer}>
        <View style={SHOPPINGCART_STYLES.imageListContainer}>
          <ImageBackground
            style={SHOPPINGCART_STYLES.imageProduct}
            source={{
              uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${item.product.image_url}`,
            }}
            resizeMode="contain"
          />
        </View>
        <View style={SHOPPINGCART_STYLES.productDetails}>
          <Text
            style={SHOPPINGCART_STYLES.productNameList}
            ellipsizeMode="clip"
            numberOfLines={2}>
            {item.product.name}{' '}
            <Text style={SHOPPINGCART_STYLES.bold}>
              {' '}
              ({productEntity.formatPrice(item.product.priceOffered)})
            </Text>
          </Text>
          <View style={SHOPPINGCART_STYLES.row}>
            <View style={SHOPPINGCART_STYLES.flex_1}>
              <Text style={SHOPPINGCART_STYLES.unityTextList}>
                {item.product.quantity > 1
                  ? item.product.quantity + ' unidades'
                  : item.product.quantity + ' unidad'}
              </Text>
              <Text style={SHOPPINGCART_STYLES.subtotalProduct}>
                {productEntity.formatPrice(
                  item.product.priceOffered * item.product.quantity
                )}
              </Text>
            </View>
            <View style={SHOPPINGCART_STYLES.h100}>
              <CounterComponent
                initAmount={item.product.quantity}
                onUpdateAmount={(amount) => updateAmount(amount, item.product)}
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  const CartListSeparator = () => {
    return <View style={SHOPPINGCART_STYLES.separator} />;
  };

  const fadeAnim = useRef(new Animated.Value(0)).current;

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 40,
      duration: 1000,
      useNativeDriver: false,
      easing: Easing.linear,
    }).start();
  };

  const fadeOut = () => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: false,
      easing: Easing.linear,
    }).start();
  };

  const height = fadeAnim.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 50],
    extrapolate: 'clamp',
  });

  return (
    <View style={SHOPPINGCART_STYLES.shoppingCartListContainer}>
      {cartProducts?.products?.length > 0 ? (
        <>
          <FlatList
            ItemSeparatorComponent={CartListSeparator}
            data={cartProducts.products}
            renderItem={({item}) => <ProductInCart product={item} />}
            keyExtractor={(item, index) => {
              return `${item.name}_${index}`;
            }}
            stickyHeaderIndices={[0]}
            ListHeaderComponent={() => {
              if (cartProducts.products?.length > 0) {
                return (
                  <Pressable
                    style={SHOPPINGCART_STYLES.removeProductsInCart}
                    onPress={() =>
                      shoppingCartViewModel.removeAllProductsInCar()
                    }>
                    <MaterialCommunityIcons
                      name="cart-remove"
                      size={25}
                      color="#b71c1c"
                    />
                    <Text style={SHOPPINGCART_STYLES.removeProductsText}>
                      Eliminar productos del carrito
                    </Text>
                  </Pressable>
                );
              } else {
                return <></>;
              }
            }}
          />
          <Animated.View
            style={[
              SHOPPINGCART_STYLES.deleteMessageContainer,
              {height: height},
            ]}>
            <Text style={SHOPPINGCART_STYLES.white_text}>
              El producto se ha eliminado de tu carrito exitosamente
            </Text>
          </Animated.View>
        </>
      ) : (
        <View style={SHOPPINGCART_STYLES.emptyContainer}>
          <Text style={SHOPPINGCART_STYLES.emptyMessage}>
            Tu carrito está vacío
          </Text>
          <Text style={SHOPPINGCART_STYLES.supcationEmptyMessage}>
            Te invitamos a consultar nuestra variedad de productos
          </Text>
        </View>
      )}
      {cartProducts?.products?.length > 0 && (
        <View style={[SHOPPINGCART_STYLES.white, SHOPPINGCART_STYLES.p15]}>
          <CustomButton
            colorText="white"
            colorButton="blue"
            styleText={SHOPPINGCART_STYLES.buyBtnText}
            styleButton={SHOPPINGCART_STYLES.buyBtn}
            textButton={`Comprar ${productEntity.formatPrice(
              cartProducts.subtotalProductsInCar || 0
            )}`}
            buttonProps={{
              title: `Comprar ${productEntity.formatPrice(
                cartProducts.subtotalProductsInCar || 0
              )}`,
              onPress: () => {
                navigation.navigate('CheckoutDetails');
              },
            }}
          />
        </View>
      )}
    </View>
  );
}
