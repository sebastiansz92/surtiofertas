import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {ImageBackground, KeyboardAvoidingView, Text, View} from 'react-native';
import CustomButton from '../../components/CustomButton';
import {GENERATEDINVOICE_STYLES} from '../../styles/ScreensStyles';
import BlueCheck from '../../../../../assets/img/layout/pedido-realizado.svg';

export default function GeneratedInvoiceScreen({route}) {
  const {message} = route.params;
  const navigation = useNavigation();
  return (
    <KeyboardAvoidingView
      style={GENERATEDINVOICE_STYLES.generatedInvoiceContainer}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={GENERATEDINVOICE_STYLES.alphaImage}>
        <View style={GENERATEDINVOICE_STYLES.image}>
          <BlueCheck />
        </View>
        <Text style={GENERATEDINVOICE_STYLES.generatedInvoiceText}>
          Tu pedido fue realizado
        </Text>
        <Text style={GENERATEDINVOICE_STYLES.generatedInvoiceDescription}>
          {message}
        </Text>
        <View style={GENERATEDINVOICE_STYLES.formContainer}>
          <View style={GENERATEDINVOICE_STYLES.buttonsContainer}>
            <CustomButton
              colorText="white"
              colorButton="blue"
              styleText={
                GENERATEDINVOICE_STYLES.generatedInvoiceContinueBtnText
              }
              styleButton={GENERATEDINVOICE_STYLES.generatedInvoiceContinueBtn}
              textButton="Continuar"
              buttonProps={{
                title: 'Continuar',
                onPress: () => navigation.navigate('Home'),
              }}
            />
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
