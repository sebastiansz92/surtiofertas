import React from 'react';
import {FlatList, Image, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import {createInvoiceViewModel} from '../../../view-models/invoice-view-model';
import ProductEntity from '../../../../domain/entity/model/ProductEntity';
import {ENVIRONMENT} from '../../../../data/constants/api.constants';
import CustomButton from '../../components/CustomButton';
import {setInvoiceDetails} from '../../../../data/redux/slices/invoice/invoice.slice';

const productEntity = new ProductEntity();

export default function InvoiceListScreen({navigation}) {
  const invoiceViewModel = createInvoiceViewModel(
    useSelector((state: StoreState) => state.invoice)
  );
  const dispatch = useDispatch();
  const InvoiceItem = ({invoice}) => {
    return (
      <View style={{backgroundColor: 'white', padding: 10}}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'center',
            marginBottom: 10,
          }}>
          <View style={{flex: 1, paddingLeft: 5}}>
            <Text style={{color: '#1e1e1e', fontWeight: '700', fontSize: 12}}>
              Dirección de envío:
            </Text>
            <Text style={{fontSize: 14, color: '#414141'}}>
              {invoice.address.direction}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'center',
            marginBottom: 10,
          }}>
          <View style={{flex: 1, paddingLeft: 5}}>
            <Text style={{color: '#1e1e1e', fontWeight: '700', fontSize: 12}}>
              Método de envío:
            </Text>
            <Text style={{fontSize: 14, color: '#414141'}}>
              {invoice.deliveryMethod.id === 1 && 'Domicilio surtiofertas'}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'center',
            marginBottom: 10,
          }}>
          <View style={{flex: 1, paddingLeft: 5}}>
            <Text style={{color: '#1e1e1e', fontWeight: '700', fontSize: 12}}>
              Método de pago:
            </Text>
            <Text style={{fontSize: 14, color: '#414141'}}>
              Efectivo contraentrega
            </Text>
          </View>
        </View>
        <View style={{flexDirection: 'column'}}>
          <Text
            style={{
              fontSize: 18,
              color: '#1e1e1e',
              fontWeight: '700',
              marginBottom: 5,
            }}>
            Resumen de tu compra
          </Text>
          <View style={{alignItems: 'center'}}>
            {invoice.invoiceDetails.map((elm, index) => {
              return (
                <View
                  key={`inovice${index}`}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingVertical: 5,
                  }}>
                  <Image
                    style={{width: 30, height: 30}}
                    source={{
                      uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${elm.product.image_url}`,
                    }}
                    resizeMode="contain"
                  />
                  <View style={{flex: 1, marginHorizontal: 10}}>
                    <Text
                      numberOfLines={1}
                      ellipsizeMode="tail"
                      style={{
                        color: '#1e1e1e',
                        fontSize: 13,
                        fontWeight: '700',
                      }}>
                      {elm.product.name}
                    </Text>
                    <Text
                      style={{
                        color: '#414141',
                        fontWeight: '700',
                        fontSize: 11,
                      }}>
                      {elm.productQuantity > 1
                        ? elm.productQuantity + ' unidades'
                        : elm.productQuantity + ' unidad'}
                    </Text>
                  </View>
                  <View style={{marginLeft: 15}}>
                    <Text style={{color: '#414141', fontWeight: '600'}}>
                      {productEntity.formatPrice(
                        elm.productQuantity * elm.product.baseprice
                      )}
                    </Text>
                  </View>
                </View>
              );
            })}
          </View>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <CustomButton
              colorText="blue"
              colorButton="white"
              styleText={{fontWeight: '700', fontSize: 14, textAlign: 'center'}}
              styleButton={{
                alignSelf: 'center',
                paddingVertical: 10,
                borderRadius: 15,
                marginTop: 10,
                width: '100%',
              }}
              textButton="SEGUIR PEDIDO"
              buttonProps={{
                title: 'SEGUIR PEDIDO',
                onPress: () => {
                  dispatch(setInvoiceDetails(invoice));
                  navigation.navigate('InvoiceStates');
                },
              }}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      <FlatList
        ItemSeparatorComponent={() => (
          <View
            style={{backgroundColor: '#e0e0e0', height: 7, width: '100%'}}
          />
        )}
        data={invoiceViewModel.currentInvoices}
        renderItem={({item}) => <InvoiceItem invoice={item} />}
        keyExtractor={(item, index) => {
          return `${item.id}_${index}`;
        }}
      />
    </View>
  );
}
