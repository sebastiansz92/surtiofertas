import React, {useEffect, useState} from 'react';
import {Image, Pressable, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector} from 'react-redux';
import StorageData from '../../../../data/StorageData';
import {StoreState} from '../../../../data/redux/reducers';
import LayoutEntity from '../../../../domain/entity/model/LayoutEntity';
import {createInvoiceViewModel} from '../../../view-models/invoice-view-model';
import {InvoiceStateEnum} from '../../../../domain/entity/structure/invoice.model';
import {ENVIRONMENT} from '../../../../data/constants/api.constants';
import ProductEntity from '../../../../domain/entity/model/ProductEntity';
import CustomButton from '../../components/CustomButton';

const _asyncStorage = new StorageData();
const _layoutEntity = new LayoutEntity(_asyncStorage);
const productEntity = new ProductEntity();

export default function InvoiceStateScreen({navigation}) {
  const invoiceViewModel = createInvoiceViewModel(
    useSelector((state: StoreState) => state.invoice)
  );

  const [currentState, setcurrentState] = useState<
    InvoiceStateEnum | undefined
  >(undefined);

  useEffect(() => {
    setcurrentState(
      invoiceViewModel.invoiceDetails?.states.find((elm) => elm.current)?.state
    );
    const intervalQueryInvoice = setInterval(() => {
      invoiceViewModel.getInvoiceDetailsByOrder(
        invoiceViewModel.invoiceDetails?.id || -1
      );
    }, 12000);
    return () => {
      clearInterval(intervalQueryInvoice);
    };
  }, [invoiceViewModel.invoiceDetails]);
  return (
    <ScrollView
      stickyHeaderIndices={[0]}
      style={{flex: 1, backgroundColor: '#f5f5f5'}}>
      <View>
        <View
          style={{
            backgroundColor: 'white',
            padding: 10,
            borderBottomWidth: 1,
            borderBottomColor: '#e0e0e0',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Pressable onPress={() => navigation.goBack()}>
            <MaterialCommunityIcons
              name="chevron-left"
              size={30}
              color="#212121"
            />
          </Pressable>
          <View style={{flex: 1}}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                fontSize: 18,
                color: '#212121',
              }}>
              PROGRESO DE TU ORDEN
            </Text>
            <Text
              style={{
                textAlign: 'center',
                color: '#2E2E2E',
                fontWeight: '300',
                fontSize: 16,
              }}>
              {invoiceViewModel.invoiceDetails &&
                (invoiceViewModel.invoiceDetails?.states
                  ?.map((elm) => elm.current)
                  .indexOf(true) +
                  1) *
                  20}
              %
            </Text>
          </View>
          <Pressable onPress={() => navigation.pop(2)}>
            <MaterialCommunityIcons
              name="home-outline"
              size={30}
              color="#005EA9"
            />
          </Pressable>
        </View>
      </View>

      <View style={{paddingLeft: 20}}>
        <View
          style={{
            borderLeftWidth: 0.9,
            borderLeftColor: '#00C851', //'#bdbdbd'
            paddingLeft: 20,
            paddingTop: 20,
            paddingBottom: 15,
          }}>
          <Text style={{color: '#212121', fontSize: 18, fontWeight: 'bold'}}>
            Tu pedido fue recibido
          </Text>
          <Text style={{color: '#2E2E2E', fontSize: 14}}>
            {_layoutEntity.formatDate(
              invoiceViewModel?.invoiceDetails?.states.find(
                (elm) => elm.state === InvoiceStateEnum.RECEIVED
              )?.createdDate
            )}
          </Text>
          <View
            style={{
              width: 20,
              height: 20,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              backgroundColor: '#00C851',
              position: 'absolute',
              left: -10,
              top: '70%',
            }}>
            <MaterialCommunityIcons name="check-bold" size={12} color="#fff" />
          </View>
        </View>
      </View>

      <View style={{paddingLeft: 20}}>
        <View
          style={{
            borderLeftWidth: 0.9,
            borderLeftColor:
              currentState === InvoiceStateEnum.ACCEPTED ||
              currentState === InvoiceStateEnum.PACKING ||
              currentState === InvoiceStateEnum.DISPATCHING ||
              currentState === InvoiceStateEnum.DELIVERED
                ? '#00C851'
                : '#bdbdbd',
            paddingLeft: 20,
            paddingVertical: 15,
          }}>
          <Text
            style={{
              color:
                currentState === InvoiceStateEnum.ACCEPTED ||
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#212121'
                  : '#bdbdbd',
              fontSize: 18,
              fontWeight:
                currentState === InvoiceStateEnum.ACCEPTED ||
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? 'bold'
                  : '500',
            }}>
            Tu pedido fue aceptado
          </Text>
          {(currentState === InvoiceStateEnum.ACCEPTED ||
            currentState === InvoiceStateEnum.PACKING ||
            currentState === InvoiceStateEnum.DISPATCHING ||
            currentState === InvoiceStateEnum.DELIVERED) && (
            <Text style={{color: '#2E2E2E', fontSize: 14}}>
              {_layoutEntity.formatDate(
                invoiceViewModel?.invoiceDetails?.states.find(
                  (elm) => elm.state === InvoiceStateEnum.ACCEPTED
                )?.createdDate
              )}
            </Text>
          )}

          <View
            style={{
              width: 20,
              height: 20,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              backgroundColor:
                currentState === InvoiceStateEnum.ACCEPTED ||
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#00C851'
                  : 'white',
              position: 'absolute',
              left: -10,
              top: '70%',
              borderColor: '#bdbdbd',
              borderWidth:
                currentState === InvoiceStateEnum.ACCEPTED ||
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? 0
                  : 1,
            }}>
            <MaterialCommunityIcons name="check-bold" size={12} color="#fff" />
          </View>
        </View>
      </View>

      <View style={{paddingLeft: 20}}>
        <View
          style={{
            borderLeftWidth: 0.9,
            borderLeftColor:
              currentState === InvoiceStateEnum.PACKING ||
              currentState === InvoiceStateEnum.DISPATCHING ||
              currentState === InvoiceStateEnum.DELIVERED
                ? '#00C851'
                : '#bdbdbd',
            paddingLeft: 20,
            paddingVertical: 15,
          }}>
          <Text
            style={{
              color:
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#212121'
                  : '#bdbdbd',
              fontSize: 18,
              fontWeight:
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? 'bold'
                  : '500',
            }}>
            Tu pedido esta siendo empacado
          </Text>
          {(currentState === InvoiceStateEnum.PACKING ||
            currentState === InvoiceStateEnum.DISPATCHING ||
            currentState === InvoiceStateEnum.DELIVERED) && (
            <Text style={{color: '#2E2E2E', fontSize: 14}}>
              {_layoutEntity.formatDate(
                invoiceViewModel?.invoiceDetails?.states.find(
                  (elm) => elm.state === InvoiceStateEnum.PACKING
                )?.createdDate
              )}
            </Text>
          )}

          <View
            style={{
              width: 20,
              height: 20,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              backgroundColor:
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#00C851'
                  : 'white',
              position: 'absolute',
              left: -10,
              top: '70%',
              borderColor: '#bdbdbd',
              borderWidth:
                currentState === InvoiceStateEnum.PACKING ||
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? 0
                  : 1,
            }}>
            <MaterialCommunityIcons name="check-bold" size={12} color="#fff" />
          </View>
        </View>
      </View>
      <View style={{paddingLeft: 20}}>
        <View
          style={{
            borderLeftWidth: 0.9,
            borderLeftColor:
              currentState === InvoiceStateEnum.DISPATCHING ||
              currentState === InvoiceStateEnum.DELIVERED
                ? '#00C851'
                : '#bdbdbd',
            paddingLeft: 20,
            paddingVertical: 15,
          }}>
          <Text
            style={{
              color:
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#212121'
                  : '#bdbdbd',
              fontSize: 18,
              fontWeight:
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? 'bold'
                  : '500',
            }}>
            Tu pedido va en camino a tu dirección
          </Text>
          {(currentState === InvoiceStateEnum.DISPATCHING ||
            currentState === InvoiceStateEnum.DELIVERED) && (
            <Text style={{color: '#2E2E2E', fontSize: 14}}>
              {_layoutEntity.formatDate(
                invoiceViewModel?.invoiceDetails?.states.find(
                  (elm) => elm.state === InvoiceStateEnum.DISPATCHING
                )?.createdDate
              )}
            </Text>
          )}
          <View
            style={{
              width: 20,
              height: 20,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              backgroundColor:
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#00C851'
                  : 'white',
              position: 'absolute',
              left: -10,
              top: '70%',
              borderColor: '#bdbdbd',
              borderWidth:
                currentState === InvoiceStateEnum.DISPATCHING ||
                currentState === InvoiceStateEnum.DELIVERED
                  ? 0
                  : 1,
            }}>
            <MaterialCommunityIcons name="check-bold" size={12} color="#fff" />
          </View>
        </View>
      </View>
      <View style={{paddingLeft: 20}}>
        <View
          style={{
            borderLeftWidth: 0.9,
            borderLeftColor:
              currentState === InvoiceStateEnum.DELIVERED
                ? '#00C851'
                : '#bdbdbd',
            paddingLeft: 20,
            paddingVertical: 15,
          }}>
          <Text
            style={{
              color:
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#212121'
                  : '#bdbdbd',
              fontSize: 18,
              fontWeight:
                currentState === InvoiceStateEnum.DELIVERED ? 'bold' : '500',
            }}>
            Tu pedido ha sido entregado
          </Text>
          {currentState === InvoiceStateEnum.DELIVERED && (
            <Text style={{color: '#2E2E2E', fontSize: 14}}>
              {_layoutEntity.formatDate(
                invoiceViewModel?.invoiceDetails?.states.find(
                  (elm) => elm.state === InvoiceStateEnum.DELIVERED
                )?.createdDate
              )}
            </Text>
          )}
          <View
            style={{
              width: 20,
              height: 20,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              backgroundColor:
                currentState === InvoiceStateEnum.DELIVERED
                  ? '#00C851'
                  : 'white',
              position: 'absolute',
              left: -10,
              top: '70%',
              borderColor: '#bdbdbd',
              borderWidth: currentState === InvoiceStateEnum.DELIVERED ? 0 : 1,
            }}>
            <MaterialCommunityIcons name="check-bold" size={12} color="#fff" />
          </View>
        </View>
      </View>
      <View style={{padding: 15}}>
        <View
          style={{
            backgroundColor: 'white',
            padding: 15,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: '#212121'}}>
            Dirección de entrega
          </Text>
          <Text
            style={{
              fontSize: 12,
              paddingTop: 3,
              fontWeight: 'bold',
              color: '#616161',
            }}>
            Calle 11 # 68 A 122
          </Text>
          <Text
            style={{
              fontSize: 20,
              marginTop: 20,
              fontWeight: 'bold',
              color: '#212121',
            }}>
            Método de pago
          </Text>
          <Text
            style={{
              fontSize: 12,
              paddingTop: 3,
              fontWeight: 'bold',
              color: '#616161',
            }}>
            Efectivo contraentrega
          </Text>
          <Text
            style={{
              fontSize: 20,
              marginTop: 20,
              fontWeight: 'bold',
              color: '#212121',
            }}>
            Productos en esta orden
          </Text>
          <View style={{alignItems: 'center'}}>
            {invoiceViewModel.invoiceDetails?.invoiceDetails.map(
              (elm, index) => {
                return (
                  <View
                    key={`inovice${index}`}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingVertical: 5,
                    }}>
                    <Image
                      style={{width: 30, height: 30}}
                      source={{
                        uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${elm.product.image_url}`,
                      }}
                      resizeMode="contain"
                    />
                    <View style={{flex: 1, marginHorizontal: 10}}>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={{
                          color: '#1e1e1e',
                          fontSize: 13,
                          fontWeight: '700',
                        }}>
                        {elm.product.name}
                      </Text>
                      <Text
                        style={{
                          color: '#414141',
                          fontWeight: '700',
                          fontSize: 11,
                        }}>
                        {elm.productQuantity > 1
                          ? elm.productQuantity + ' unidades'
                          : elm.productQuantity + ' unidad'}
                      </Text>
                    </View>
                    <View style={{marginLeft: 15}}>
                      <Text style={{color: '#414141', fontWeight: '600'}}>
                        {productEntity.formatPrice(
                          elm.productQuantity * (elm.product.baseprice || 1)
                        )}
                      </Text>
                    </View>
                  </View>
                );
              }
            )}
          </View>
        </View>
      </View>
      {currentState === InvoiceStateEnum.DELIVERED && (
        <View style={{padding: 15}}>
          <CustomButton
            colorText="white"
            colorButton="blue"
            styleText={{fontWeight: 'bold', textAlign: 'center'}}
            styleButton={{
              padding: 15,
              borderRadius: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            textButton="Finalizar"
            buttonProps={{
              title: 'Finalizar',
              onPress: () => navigation.pop(2),
            }}
          />
        </View>
      )}
      {currentState === InvoiceStateEnum.RECEIVED && (
        <View style={{padding: 15}}>
          <CustomButton
            colorText="white"
            colorButton="blue"
            styleText={{fontWeight: 'bold', textAlign: 'center'}}
            styleButton={{
              padding: 15,
              borderRadius: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            textButton="CANCELAR PEDIDO"
            buttonProps={{
              title: 'CANCELAR PEDIDO',
              onPress: () =>
                invoiceViewModel.cancelInvoice({
                  comment: 'cancel by user',
                  event: 'CANCELAR_PEDIDO',
                  invoiceId: invoiceViewModel?.invoiceDetails?.id || -1,
                }),
            }}
          />
        </View>
      )}
    </ScrollView>
  );
}
