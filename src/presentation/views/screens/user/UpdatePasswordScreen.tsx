import React, {useEffect} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  ImageBackground,
  View,
} from 'react-native';
import KeyIcon from '../../../../../assets/img/layout/llave-password.svg';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {createReduxUpdatePasswordInterface} from '../../../../data/redux/slices/user/redux-updatepassword-interface';
import store from '../../../../data/redux/store';
import {createUpdatePasswordViewModel} from '../../../view-models/updatepassword-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {UPDATEPASSWORD_STYLES} from '../../styles/ScreensStyles';
import CustomInput from '../../components/CustomInput.component';
import CustomButton from '../../components/CustomButton';
import {createUpdatePasswordInteractor} from '../../../../domain/interactors/use-cases/user/updatepassword-usecase';

const UpdatePasswordInterface = createReduxUpdatePasswordInterface(
  store.dispatch
);
const updatePasswordInteractor = createUpdatePasswordInteractor(
  UpdatePasswordInterface
);

export default function UpdatePasswordScreen({route, navigation}) {
  const {userId} = route?.params;
  const updatePasswordViewModel = createUpdatePasswordViewModel(
    useSelector((state: StoreState) => state.updatePassword)
  );

  useEffect(() => {
    if (updatePasswordViewModel.errorValidation !== undefined) {
      const recoveryError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: updatePasswordViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', recoveryError);
    }

    if (updatePasswordViewModel.passwordWasUpdated === true) {
      const updatePasswordMessage: DialogMessage = {
        title: {
          text: 'Contraseña actualizada',
          color: '#005fab',
        },
        message: updatePasswordViewModel.passwordUpdateMessage || '',
        button: {
          color: '#005fab',
          text: 'CONTINUAR',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', updatePasswordMessage);
      navigation.pop(3);
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      return () => {
        updatePasswordViewModel.resetValidation(updatePasswordInteractor);
      };
    }, [])
  );

  return (
    <KeyboardAvoidingView
      style={UPDATEPASSWORD_STYLES.updateContainer}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={UPDATEPASSWORD_STYLES.alphaImage}>
        <Text style={UPDATEPASSWORD_STYLES.title}>Actualiza tu contraseña</Text>
        <Text style={UPDATEPASSWORD_STYLES.description}>
          Lorem, voluptatum, officiis perspiciatis est distinctio deserunt
          reprehenderit magni dolor.
        </Text>
        <View style={UPDATEPASSWORD_STYLES.formContainer}>
          <CustomInput
            bgColorIcon={updatePasswordViewModel.passwordIconBg}
            style={UPDATEPASSWORD_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Contraseña',
              secureTextEntry: true,
              placeholderTextColor: '#005fab',
              onChangeText: (value: string) =>
                updatePasswordViewModel.onChangeInput(
                  updatePasswordInteractor,
                  'password',
                  value
                ),
              onFocus: () =>
                updatePasswordViewModel.onFocusInput(
                  updatePasswordInteractor,
                  'password',
                  updatePasswordViewModel.password
                ),
              onBlur: () =>
                updatePasswordViewModel.onBlurInput(
                  updatePasswordInteractor,
                  'password',
                  updatePasswordViewModel.password
                ),
            }}>
            <KeyIcon />
          </CustomInput>
          <CustomInput
            bgColorIcon={updatePasswordViewModel.confirmPasswordIconBg}
            style={UPDATEPASSWORD_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Confirma la contraseña',
              secureTextEntry: true,
              placeholderTextColor: '#005fab',
              onChangeText: (value: string) =>
                updatePasswordViewModel.onChangeInput(
                  updatePasswordInteractor,
                  'confirmPassword',
                  value
                ),
              onFocus: () =>
                updatePasswordViewModel.onFocusInput(
                  updatePasswordInteractor,
                  'confirmPassword',
                  updatePasswordViewModel.confirmPassword
                ),
              onBlur: () =>
                updatePasswordViewModel.onBlurInput(
                  updatePasswordInteractor,
                  'confirmPassword',
                  updatePasswordViewModel.confirmPassword
                ),
            }}>
            <KeyIcon />
          </CustomInput>
          <View style={UPDATEPASSWORD_STYLES.buttonsContainer}>
            <CustomButton
              colorText="white"
              colorButton="blue"
              styleText={UPDATEPASSWORD_STYLES.btnText}
              styleButton={UPDATEPASSWORD_STYLES.btn}
              textButton="Actualizar contraseña"
              buttonProps={{
                title: 'Actualizar Contraseña',
                onPress: () =>
                  updatePasswordViewModel.onPressButtonUpdatePassword(
                    updatePasswordInteractor,
                    userId
                  ),
              }}
            />
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
