import React, {useEffect} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  ImageBackground,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {createReduxValidateCodeInterface} from '../../../../data/redux/slices/user/redux-validate-code-interface';
import store from '../../../../data/redux/store';
import {createValidateCodeViewModel} from '../../../view-models/validatecode-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {createRecoveryViewModel} from '../../../view-models/recovery-view-model';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {HTTP_STATUS} from '../../../../data/constants/api.constants';
import {VALIDATECODE_STYLES} from '../../styles/ScreensStyles';
import CodePinVerificator from '../../components/codePinVerificator.component';
import CustomButton from '../../components/CustomButton';
import {createValidateCodeInteractor} from '../../../../domain/interactors/use-cases/user/validatecode-usecase';

const ValidateCodeInterface = createReduxValidateCodeInterface(store.dispatch);
const validateCodeInteractor = createValidateCodeInteractor(
  ValidateCodeInterface
);

export default function ValidateCodeScreen({route, navigation}) {
  const validateCodeViewModel = createValidateCodeViewModel(
    useSelector((state: StoreState) => state.validateCode)
  );

  const recoveryViewModel = createRecoveryViewModel(
    useSelector((state: StoreState) => state.recovery)
  );

  useEffect(() => {
    if (validateCodeViewModel.errorValidation !== undefined) {
      const validateCodeError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: validateCodeViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', validateCodeError);
    }
    if (
      !validateCodeViewModel.recoveryResponse &&
      validateCodeViewModel.pinValue.length === 4
    ) {
      validateCodeViewModel.onPressButtonValidateCode(validateCodeInteractor);
    }
    if (
      validateCodeViewModel.recoveryResponse &&
      validateCodeViewModel.recoveryResponse?.status !== HTTP_STATUS.OK
    ) {
      const validateCodeError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: validateCodeViewModel.recoveryResponse?.message,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', validateCodeError);
    } else if (
      validateCodeViewModel.recoveryResponse &&
      validateCodeViewModel.recoveryResponse?.status === HTTP_STATUS.OK
    ) {
      navigation.navigate('UpdatePassword', {
        userId: validateCodeViewModel.recoveryResponse.userId,
      });
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      return () => {
        validateCodeViewModel.resetValidation(validateCodeInteractor);
      };
    }, [])
  );

  return (
    <KeyboardAvoidingView
      style={VALIDATECODE_STYLES.validateCodeContainer}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={VALIDATECODE_STYLES.alphaImage}>
        <Text style={VALIDATECODE_STYLES.validateCodeText}>Validar código</Text>
        <Text style={VALIDATECODE_STYLES.validateCodeDescription}>
          Lorem, voluptatum, officiis perspiciatis est distinctio deserunt
          reprehenderit magni dolor.
        </Text>
        <View style={VALIDATECODE_STYLES.formContainer}>
          <CodePinVerificator
            validateCodeViewModel={validateCodeViewModel}
            validateCodeInteractor={validateCodeInteractor}
          />
          <View style={VALIDATECODE_STYLES.buttonsContainer}>
            <CustomButton
              colorText="blue"
              colorButton=""
              styleText={VALIDATECODE_STYLES.validateCodeSendBtnText}
              styleButton={VALIDATECODE_STYLES.validateCodeSendBtn}
              textButton="Volver a enviar código"
              buttonProps={{
                title: 'Volver a enviar código',
                onPress: () =>
                  validateCodeViewModel.onPressButtonSendAgainCode(
                    validateCodeInteractor,
                    recoveryViewModel.email
                  ),
              }}
            />
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
