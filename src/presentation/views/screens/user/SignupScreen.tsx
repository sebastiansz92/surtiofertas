import React, {useEffect} from 'react';
import {ImageBackground, Pressable, Text, View} from 'react-native';
import UserIcon from '../../../../../assets/img/layout/user-registro.svg';
import KeyIcon from '../../../../../assets/img/layout/llave-password.svg';
import EmailIcon from '../../../../../assets/img/layout/correo.svg';
import PhoneIcon from '../../../../../assets/img/layout/celular.svg';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {createReduxSignupInterface} from '../../../../data/redux/slices/user/redux-signup-interface';
import store from '../../../../data/redux/store';
import {createSignupViewModel} from '../../../view-models/signup-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {LOGIN_STYLES, SIGNUP_STYLES} from '../../styles/ScreensStyles';
import CustomInput from '../../components/CustomInput.component';
import {createSignupInteractor} from '../../../../domain/interactors/use-cases/user/signup-usecase';

const signupInterface = createReduxSignupInterface(store.dispatch);
const signupInteractor = createSignupInteractor(signupInterface);

export default function SignupScreen({navigation}) {
  const signupViewModel = createSignupViewModel(
    useSelector((state: StoreState) => state.signup)
  );

  useEffect(() => {
    if (signupViewModel.errorValidation !== undefined) {
      const signupError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: signupViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', signupError);
    }
    if (signupViewModel.userLogin) {
      navigation.navigate('Main');
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      signupViewModel.resetValidation(signupInteractor);
    }, [])
  );

  return (
    <ScrollView style={LOGIN_STYLES.loginContainer}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={LOGIN_STYLES.alphaImage}>
        <Text style={SIGNUP_STYLES.signupText}>Registrate</Text>
        <View style={LOGIN_STYLES.formContainer}>
          <CustomInput
            bgColorIcon={signupViewModel.nameIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Nombre completo',
              placeholderTextColor: '#005fab',
              onChangeText: (value: string) =>
                signupViewModel.onChangeInput(signupInteractor, 'name', value),
              onFocus: () =>
                signupViewModel.onFocusInput(signupInteractor, 'name'),
              onBlur: () =>
                signupViewModel.onBlurInput(
                  signupInteractor,
                  'name',
                  signupViewModel.name
                ),
            }}>
            <UserIcon />
          </CustomInput>
          <CustomInput
            bgColorIcon={signupViewModel.phoneIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Celular',
              placeholderTextColor: '#005fab',
              keyboardType: 'numeric',
              onChangeText: (value: string) =>
                signupViewModel.onChangeInput(signupInteractor, 'phone', value),
              onFocus: () =>
                signupViewModel.onFocusInput(signupInteractor, 'phone'),
              onBlur: () =>
                signupViewModel.onBlurInput(
                  signupInteractor,
                  'phone',
                  signupViewModel.phone
                ),
            }}>
            <PhoneIcon />
          </CustomInput>
          <CustomInput
            bgColorIcon={signupViewModel.emailIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Correo electrónico',
              placeholderTextColor: '#005fab',
              keyboardType: 'email-address',
              onChangeText: (value: string) =>
                signupViewModel.onChangeInput(signupInteractor, 'email', value),
              onFocus: () =>
                signupViewModel.onFocusInput(signupInteractor, 'email'),
              onBlur: () =>
                signupViewModel.onBlurInput(
                  signupInteractor,
                  'email',
                  signupViewModel.email
                ),
            }}>
            <EmailIcon />
          </CustomInput>
          <CustomInput
            bgColorIcon={signupViewModel.passwordIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Contraseña',
              placeholderTextColor: '#005fab',
              secureTextEntry: true,
              onChangeText: (value: string) =>
                signupViewModel.onChangeInput(
                  signupInteractor,
                  'password',
                  value
                ),
              onFocus: () =>
                signupViewModel.onFocusInput(signupInteractor, 'password'),
              onBlur: () =>
                signupViewModel.onBlurInput(
                  signupInteractor,
                  'password',
                  signupViewModel.password
                ),
            }}>
            <KeyIcon />
          </CustomInput>
          <CustomInput
            bgColorIcon={signupViewModel.passwordConfirmIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Confirmar contraseña',
              placeholderTextColor: '#005fab',
              secureTextEntry: true,
              onChangeText: (value: string) =>
                signupViewModel.onChangeInput(
                  signupInteractor,
                  'passwordConfirm',
                  value
                ),
              onFocus: () =>
                signupViewModel.onFocusInput(
                  signupInteractor,
                  'passwordConfirm'
                ),
              onBlur: () =>
                signupViewModel.onBlurInput(
                  signupInteractor,
                  'passwordConfirm',
                  signupViewModel.passwordConfirm
                ),
            }}>
            <KeyIcon />
          </CustomInput>
          <View style={SIGNUP_STYLES.buttonsContainer}>
            <Pressable
              onPress={() =>
                signupViewModel.onPressButtonSignup(signupInteractor)
              }
              style={LOGIN_STYLES.loginBtn}>
              <Text style={LOGIN_STYLES.loginBtnText}>Registrar</Text>
            </Pressable>

            <Pressable
              style={LOGIN_STYLES.signupBtn}
              onPress={() => navigation.pop()}>
              <Text style={LOGIN_STYLES.signupBtnText}>Ya tengo cuenta</Text>
            </Pressable>
          </View>
        </View>
      </ImageBackground>
    </ScrollView>
  );
}
