import React, {useEffect} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  ImageBackground,
  View,
  Pressable,
} from 'react-native';
import CustomInput from '../../components/CustomInput.component';
import EmailIcon from '../../../../../assets/img/layout/correo.svg';
import KeyIcon from '../../../../../assets/img/layout/llave-password.svg';
import {useSelector} from 'react-redux';
import {CommonActions, useFocusEffect} from '@react-navigation/native';
import {createReduxLoginInterface} from '../../../../data/redux/slices/user/redux-login-interface';
import store from '../../../../data/redux/store';
import {createLoginInteractor} from '../../../../domain/interactors/use-cases/user/login-usecase';
import {createLoginViewModel} from '../../../view-models/login-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import StorageData from '../../../../data/StorageData';
import {LOGIN_STYLES} from '../../styles/ScreensStyles';

const loginInterface = createReduxLoginInterface(store.dispatch);
const loginInteractor = createLoginInteractor(loginInterface);

export default function LoginScreen({navigation}) {
  const loginViewModel = createLoginViewModel(
    useSelector((state: StoreState) => state.login)
  );

  useEffect(() => {
    if (loginViewModel.errorValidation !== undefined) {
      const loginError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: loginViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', loginError);
    }
    if (loginViewModel.userToken) {
      navigation.navigate('Main');
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      navigation.dispatch((state) => {
        // Remove the home route from the stack
        const routes = state.routes.filter((r) => r.name !== 'Main');

        return CommonActions.reset({
          ...state,
          routes,
          index: routes.length - 1,
        });
      });
      getToken();
      async function getToken() {
        const asyncStorage = new StorageData();
        const token = await asyncStorage.getData('userToken');
        if (token) {
          navigation.pop();
        }
      }
      loginViewModel.resetValidation(loginInteractor);
    }, [])
  );

  return (
    <KeyboardAvoidingView
      style={LOGIN_STYLES.loginContainer}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={LOGIN_STYLES.alphaImage}>
        <Text style={LOGIN_STYLES.loginText}>Iniciar Sesión</Text>
        <View style={LOGIN_STYLES.formContainer}>
          <CustomInput
            bgColorIcon={loginViewModel.emailIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              keyboardType: 'email-address',
              placeholder: 'Correo electrónico',
              placeholderTextColor: '#005fab',
              onChangeText: (value: string) =>
                loginViewModel.onChangeInput(loginInteractor, 'email', value),
              onFocus: () =>
                loginViewModel.onFocusInput(loginInteractor, 'email'),
              onBlur: () =>
                loginViewModel.onBlurInput(
                  loginInteractor,
                  'email',
                  loginViewModel.email
                ),
            }}>
            <EmailIcon />
          </CustomInput>
          <CustomInput
            bgColorIcon={loginViewModel.passwordIconBg}
            style={LOGIN_STYLES.inputStyle}
            inputProps={{
              placeholder: 'Contraseña',
              secureTextEntry: true,
              placeholderTextColor: '#005fab',
              onChangeText: (value: string) =>
                loginViewModel.onChangeInput(
                  loginInteractor,
                  'password',
                  value
                ),
              onFocus: () =>
                loginViewModel.onFocusInput(loginInteractor, 'password'),
              onBlur: () =>
                loginViewModel.onBlurInput(
                  loginInteractor,
                  'password',
                  loginViewModel.password
                ),
            }}>
            <KeyIcon />
          </CustomInput>
          <Pressable onPress={() => navigation.navigate('RecoveryPassword')}>
            <Text style={LOGIN_STYLES.forgetText}>
              ¿Has olvidado tu contraseña?
            </Text>
          </Pressable>

          <View style={LOGIN_STYLES.buttonsContainer}>
            <Pressable
              onPress={() => loginViewModel.onPressButtonLogin(loginInteractor)}
              style={LOGIN_STYLES.loginBtn}>
              <Text style={LOGIN_STYLES.loginBtnText}>Ingresar</Text>
            </Pressable>

            <Pressable
              style={LOGIN_STYLES.signupBtn}
              onPress={() => navigation.navigate('Signup')}>
              <Text style={LOGIN_STYLES.signupBtnText}>Registrate</Text>
            </Pressable>
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
