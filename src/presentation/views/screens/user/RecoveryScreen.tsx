import React, {useEffect} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  ImageBackground,
  View,
} from 'react-native';
import EmailIcon from '../../../../../assets/img/layout/correo.svg';
import {useSelector} from 'react-redux';

import {useFocusEffect} from '@react-navigation/native';
import {createReduxRecoveryInterface} from '../../../../data/redux/slices/user/redux-recovery-interface';
import store from '../../../../data/redux/store';
import {createRecoveryViewModel} from '../../../view-models/recovery-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {RECOVERY_STYLES} from '../../styles/ScreensStyles';
import CustomButton from '../../components/CustomButton';
import CustomInput from '../../components/CustomInput.component';
import {createRecoveryInteractor} from '../../../../domain/interactors/use-cases/user/recovery-usecase';

const RecoveryInterface = createReduxRecoveryInterface(store.dispatch);
const RecoveryInteractor = createRecoveryInteractor(RecoveryInterface);

export default function RecoveryScreen({navigation}) {
  const recoveryViewModel = createRecoveryViewModel(
    useSelector((state: StoreState) => state.recovery)
  );

  useEffect(() => {
    if (recoveryViewModel.errorValidation !== undefined) {
      const recoveryError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: recoveryViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', recoveryError);
    }
    if (recoveryViewModel.recoveryMessageSend) {
      const recoveryMessage: DialogMessage = {
        title: {
          text: 'Mensaje enviado',
          color: '#005fab',
        },
        message: recoveryViewModel.recoveryMessage || '',
        button: {
          color: '#005fab',
          text: 'CONTINUAR',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', recoveryMessage);
      navigation.push('ValidateCode');
    }
  });

  useFocusEffect(
    React.useCallback(() => {
      return () => {
        recoveryViewModel.resetValidation(RecoveryInteractor);
      };
    }, [])
  );

  return (
    <KeyboardAvoidingView
      style={RECOVERY_STYLES.recoveryContainer}
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={RECOVERY_STYLES.alphaImage}>
        <Text style={RECOVERY_STYLES.recoveryText}>Recupera tu contraseña</Text>
        <Text style={RECOVERY_STYLES.recoveryDescription}>
          Lorem, voluptatum, officiis perspiciatis est distinctio deserunt
          reprehenderit magni dolor.
        </Text>
        <View style={RECOVERY_STYLES.formContainer}>
          <CustomInput
            bgColorIcon={recoveryViewModel.emailIconBg}
            style={RECOVERY_STYLES.inputStyle}
            inputProps={{
              keyboardType: 'email-address',
              placeholder: 'Correo electrónico',
              placeholderTextColor: '#005fab',
              onChangeText: (value: string) =>
                recoveryViewModel.onChangeInput(
                  RecoveryInteractor,
                  'email',
                  value
                ),
              onFocus: () =>
                recoveryViewModel.onFocusInput(
                  RecoveryInteractor,
                  'email',
                  recoveryViewModel.email
                ),
              onBlur: () =>
                recoveryViewModel.onBlurInput(
                  RecoveryInteractor,
                  'email',
                  recoveryViewModel.email
                ),
            }}>
            <EmailIcon />
          </CustomInput>
          <View style={RECOVERY_STYLES.buttonsContainer}>
            <CustomButton
              colorText="white"
              colorButton="blue"
              styleText={RECOVERY_STYLES.recoveryBtnText}
              styleButton={RECOVERY_STYLES.recoveryBtn}
              textButton="Enviar código"
              buttonProps={{
                title: 'Recuperar Contraseña',
                onPress: () =>
                  recoveryViewModel.onPressButtonRecovery(RecoveryInteractor),
              }}
            />
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
