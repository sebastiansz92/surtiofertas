import React, {useEffect, useState} from 'react';
import {ImageBackground, Pressable, ScrollView, Text, View} from 'react-native';
import StorageData from '../../../../data/StorageData';
import LoginEntity from '../../../../domain/entity/model/LoginEntity';
import ValidatorEntity from '../../../../domain/entity/model/ValidatorEntity';
import {ACCOUNT_STYLES} from '../../styles/ScreensStyles';
import jwt_decode from 'jwt-decode';
import LocationIcon from '../../../../../assets/img/layout/ubicacion.svg';
import InvoiceIcon from '../../../../../assets/img/layout/pedidos.svg';
import CustomButton from '../../components/CustomButton';
import {useDispatch} from 'react-redux';
import {reloadUserinfoAction} from '../../../../data/redux/slices/user/login.slice';

const asyncStorage = new StorageData();
const validator = new ValidatorEntity();
const loginEntity = new LoginEntity(validator, asyncStorage);

export default function AccountScreen({navigation}) {
  const [initialLetterName, setinitialLetterName] = useState('');
  const [userName, setuserName] = useState('');
  useEffect(() => {
    loginEntity.getUserToken().then((user) => {
      const decodedUser: any = jwt_decode(user);
      setinitialLetterName(decodedUser.name.charAt(0).toUpperCase());
      setuserName(decodedUser.name.toUpperCase());
    });
  }, []);

  const dispatch = useDispatch();
  return (
    <ScrollView contentContainerStyle={ACCOUNT_STYLES.accountContainer}>
      <ImageBackground
        source={require('../../../../../assets/img/layout/marca-alpha.png')}
        style={ACCOUNT_STYLES.alphaImage}>
        <View style={ACCOUNT_STYLES.accountProfileCircle}>
          <Text style={ACCOUNT_STYLES.accountInitialLetterName}>
            {initialLetterName}
          </Text>
        </View>
        <View>
          <Text style={ACCOUNT_STYLES.accountTextName}>{userName}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '60%',
          }}>
          {/* <View
          style={{
            padding: 10,
            width: '50%',
          }}>
          <Pressable
            style={{
              borderColor: '#e0e0e0',
              borderWidth: 1,
              borderRadius: 8,
              padding: 10,
            }}>
            <View
              style={{width: '100%', aspectRatio: 1, paddingHorizontal: 15}}>
              <AccountIconBarFocus />
            </View>

            <Text
              style={{
                marginTop: 5,
                textAlign: 'center',
                color: '#005EA9',
                fontWeight: 'bold',
              }}>
              Datos personales
            </Text>
          </Pressable>
        </View> */}
          <View
            style={{
              padding: 10,
              width: '50%',
            }}>
            <Pressable
              style={{
                backgroundColor: 'white',
                borderColor: '#e0e0e0',
                borderWidth: 1,
                borderRadius: 8,
                padding: 10,
              }}
              onPress={() => navigation.navigate('AddressModalSelect')}>
              <View style={{width: '100%', aspectRatio: 1}}>
                <LocationIcon />
              </View>
              <Text
                style={{
                  marginTop: 5,
                  textAlign: 'center',
                  color: '#005EA9',
                  fontWeight: 'bold',
                }}>
                Mis direcciones
              </Text>
            </Pressable>
          </View>
          <View
            style={{
              padding: 10,
              width: '50%',
            }}>
            <Pressable
              style={{
                backgroundColor: 'white',
                borderColor: '#e0e0e0',
                borderWidth: 1,
                borderRadius: 8,
                padding: 10,
              }}
              onPress={() => navigation.navigate('Favorites')}>
              <View style={{width: '100%', aspectRatio: 1}}>
                <InvoiceIcon />
              </View>
              <Text
                style={{
                  marginTop: 5,
                  textAlign: 'center',
                  color: '#005EA9',
                  fontWeight: 'bold',
                }}>
                Tus pedidos
              </Text>
            </Pressable>
          </View>
        </View>
        <View style={{width: '90%'}}>
          <CustomButton
            colorText="blue"
            colorButton="white"
            styleText={{fontWeight: 'bold', textAlign: 'center'}}
            styleButton={{
              width: '100%',
              paddingVertical: 10,
              borderRadius: 8,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            textButton="CERRAR SESIÓN"
            buttonProps={{
              title: 'CERRAR SESIÓN',
              onPress: () => {
                asyncStorage.removeItem('userToken').then(() => {
                  navigation.navigate('Home');
                  dispatch(reloadUserinfoAction(true));
                });
              },
            }}
          />
        </View>
      </ImageBackground>
    </ScrollView>
  );
}
