import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Pressable,
  ScrollView,
  Switch,
  Text,
  View,
} from 'react-native';
import MapView from 'react-native-maps';
import CustomButton from '../../components/CustomButton';
import CustomInput from '../../components/CustomInput.component';
import {EDIT_ADDRESS, LIST_ADDRESS_STYLES} from '../../styles/address.styles';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createAddressViewModel} from '../../../view-models/address-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {useSelector} from 'react-redux';
import {AddressValues} from '../../../../domain/entity/structure/address.model';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';

export default function EditAddressScreen({navigation, route}) {
  const addressViewModel = createAddressViewModel(
    useSelector((state: StoreState) => state.address)
  );

  const [region, setRegion] = useState({
    latitude: 6.265694883502279,
    longitude: -75.57499703822191,
    latitudeDelta: 0.005,
    longitudeDelta: 0.004,
  });

  const [editAddress, seteditAddress] = useState<AddressValues>({
    name: route?.params?.name || '',
    direction: addressViewModel?.currentAddressDecode?.address,
    details: route?.params?.details || '',
    isDefault: route?.params?.isDefault || false,
    latitude: addressViewModel?.currentAddressDecode?.position?.latitude,
    longitude: addressViewModel?.currentAddressDecode?.position?.longitude,
  });

  const [fromCurrentLoc, setfromCurrentLoc] = useState(false);

  useEffect(() => {
    if (addressViewModel.addressCreatedOrUpdated) {
      addressViewModel.resetCurrentState();
      navigation.pop(3);
    }

    if (addressViewModel.errorValidation !== undefined) {
      const addressFormError: DialogMessage = {
        title: {
          text: 'Atención',
          color: '#005fab',
        },
        message: addressViewModel.errorValidation,
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', addressFormError);
      addressViewModel.resetErrorValidationState();
    }
  });

  useEffect(() => {
    if (route.params) {
      addressViewModel.getAddressByCoords(
        route.params.position.lat,
        route.params.position.lng
      );
      asignInitialRegion(route.params.position.lat, route.params.position.lng);
    } else {
      asignInitialRegion(
        addressViewModel.currentAddressDecode?.position?.latitude ||
          region.latitude,
        addressViewModel.currentAddressDecode?.position?.longitude ||
          region.longitude
      );
    }

    function asignInitialRegion(lat: number, lng: number) {
      setRegion({
        ...region,
        latitude: lat,
        longitude: lng,
      });
    }
  }, []);

  return (
    <View style={EDIT_ADDRESS.flex_1}>
      <View style={EDIT_ADDRESS.flex_2}>
        <MapView
          style={EDIT_ADDRESS.map_container}
          onRegionChangeComplete={(position) => {
            if (route.params === undefined || fromCurrentLoc) {
              addressViewModel.getAddressByCoords(
                position.latitude,
                position.longitude
              );
            } else {
              setfromCurrentLoc(true);
            }
          }}
          initialRegion={region}
          region={{
            latitude:
              addressViewModel?.currentAddressDecode?.position?.latitude ||
              6.265694883502279,
            longitude:
              addressViewModel?.currentAddressDecode?.position?.longitude ||
              -75.57499703822191,
            latitudeDelta: 0.005,
            longitudeDelta: 0.004,
          }}
        />
        <View style={EDIT_ADDRESS.marker_container}>
          <ImageBackground
            style={EDIT_ADDRESS.img_marker}
            source={require('../../../../../assets/img/layout/marker.png')}
          />
        </View>
      </View>
      <View style={EDIT_ADDRESS.view_scroll_container}>
        <ScrollView>
          <Text style={EDIT_ADDRESS.address_text}>
            Dirección: <Text style={EDIT_ADDRESS.required_input}>*</Text>
          </Text>
          <View style={EDIT_ADDRESS.input_address_container}>
            <CustomInput
              bgColorIcon="#005fab"
              inputProps={{
                value: addressViewModel?.currentAddressDecode?.address,
                style: {paddingRight: 50, fontSize: 10},
                editable: false,
                scrollEnabled: true,
                onChangeText: (direction: string) => {
                  seteditAddress({...editAddress, direction});
                },
              }}>
              <IconFontAwesome name="map" size={20} color="#fff" />
            </CustomInput>
            <Pressable
              onPress={() => navigation.pop()}
              style={EDIT_ADDRESS.edit_icon_input}>
              <IconFontAwesome name="edit" size={25} color="#616161" />
            </Pressable>
          </View>
          <View style={EDIT_ADDRESS.details_input_container}>
            <Text>Detalles:</Text>
            <CustomInput
              bgColorIcon="#005fab"
              inputProps={{
                placeholder: 'Apto, oficina, piso',
                placeholderTextColor: '#b3b3b3',
                onChangeText: (details: string) => {
                  seteditAddress({...editAddress, details});
                },
                value: editAddress.details,
              }}>
              <MaterialCommunityIcons
                name="home-city"
                size={25}
                color="#ffffff"
              />
            </CustomInput>
          </View>
          <View style={EDIT_ADDRESS.label_input_container}>
            <Text>
              Nombre: <Text style={EDIT_ADDRESS.required_input}>*</Text>
            </Text>
            <CustomInput
              bgColorIcon="#005fab"
              inputProps={{
                placeholder: 'Casa, oficina',
                placeholderTextColor: '#b3b3b3',
                onChangeText: (name: string) => {
                  seteditAddress({...editAddress, name});
                },
                value: editAddress.name,
              }}>
              <IconFontAwesome name="tag" size={25} color="#ffffff" />
            </CustomInput>
          </View>
          <View style={EDIT_ADDRESS.switch_container}>
            <Switch
              trackColor={{false: '#767577', true: '#DF040B'}}
              thumbColor={editAddress.isDefault ? '#FFEF00' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={(isDefault) =>
                seteditAddress({...editAddress, isDefault})
              }
              value={editAddress.isDefault}
            />
            <Text>Habilitar dirección por defecto</Text>
          </View>
        </ScrollView>
      </View>
      <View style={LIST_ADDRESS_STYLES.btnContainer}>
        <CustomButton
          colorText="white"
          colorButton="blue"
          styleText={LIST_ADDRESS_STYLES.addAddressBtnText}
          styleButton={LIST_ADDRESS_STYLES.addAddressBtn}
          textButton="Añadir dirección"
          buttonProps={{
            title: 'Añadir dirección',
            onPress: () => {
              let address = {
                ...editAddress,
                latitude:
                  addressViewModel?.currentAddressDecode?.position?.latitude,
                longitude:
                  addressViewModel?.currentAddressDecode?.position?.longitude,
                direction: addressViewModel?.currentAddressDecode?.address,
              };
              if (route.params?.id) {
                address.id = route.params.id;
              }
              addressViewModel.updateAddress(address);
            },
          }}
        />
      </View>
    </View>
  );
}
