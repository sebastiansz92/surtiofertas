import React from 'react';
import {Text, View} from 'react-native';
import {GooglePlaceData} from 'react-native-google-places-autocomplete';
import {SEARCH_ADDRESS} from '../../styles/address.styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

type RowAddressResultProps = {
  row: GooglePlaceData;
};

export default function RowAddressResult(props: RowAddressResultProps) {
  const {row} = props;
  return (
    <View style={SEARCH_ADDRESS.result_container}>
      <MaterialCommunityIcons
        style={SEARCH_ADDRESS.result_icon}
        name="map-marker-outline"
        size={30}
        color="#212121"
      />
      <View style={SEARCH_ADDRESS.flex_1}>
        <Text style={SEARCH_ADDRESS.result_text}>
          {row.structured_formatting.main_text}
        </Text>
        <Text style={SEARCH_ADDRESS.result_text2}>
          {row.structured_formatting.secondary_text}
        </Text>
      </View>
      <View style={SEARCH_ADDRESS.mR10}>
        <IconFontAwesome name="angle-right" size={20} color="#1e1e1e" />
      </View>
    </View>
  );
}
