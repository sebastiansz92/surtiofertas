import React, {useEffect} from 'react';
import {Pressable, ScrollView, Text, View} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {SEARCH_ADDRESS} from '../../styles/address.styles';
import {GooglePlaceData} from 'react-native-google-places-autocomplete/GooglePlacesAutocomplete';
import RowAddressResult from './RowAddressResult';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import {StoreState} from '../../../../data/redux/reducers';
import {useSelector} from 'react-redux';
import {createAddressViewModel} from '../../../view-models/address-view-model';
import {useFocusEffect} from '@react-navigation/native';

export default function AddressLocationScreen({navigation}) {
  const addressViewModel = createAddressViewModel(
    useSelector((state: StoreState) => state.address)
  );

  useEffect(() => {
    addressViewModel.getCurrentPosition();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      addressViewModel.getCurrentPosition();
    }, [])
  );

  return (
    <ScrollView
      style={SEARCH_ADDRESS.searchContainer}
      keyboardShouldPersistTaps="always">
      <View style={SEARCH_ADDRESS.p_10}>
        <GooglePlacesAutocomplete
          autoFillOnNotFound={false}
          currentLocation={false}
          debounce={200}
          enableHighAccuracyLocation={true}
          enablePoweredByContainer={false}
          fetchDetails={true}
          filterReverseGeocodingByTypes={[
            'locality',
            'administrative_area_level_3',
          ]}
          GoogleReverseGeocodingQuery={{
            language: 'es',
            region: 'co',
          }}
          isRowScrollable={false}
          minLength={2}
          placeholder="Ingresa una dirección"
          query={{
            key: 'AIzaSyDFltYLYZ6O0_CSAZcxU-K3jXDr3YccEZU',
            language: 'es', // language of the results
            types: ['address', 'establishment', '(regions)'], // default: 'geocode'
            location: '6.2518401, -75.563591',
            radius: '40000',
            components: 'country:co',
            strictbounds: true,
          }}
          renderRow={(elm: GooglePlaceData) => <RowAddressResult row={elm} />}
          onPress={(data, details = null) => {
            if (data.description && details) {
              navigation.navigate('EditAddress', {
                position: details.geometry.location,
                address: data.description,
              });
            }
          }}
          styles={{
            textInputContainer: SEARCH_ADDRESS.textInputContainer,
            textInput: SEARCH_ADDRESS.textInput,
            row: SEARCH_ADDRESS.row,
            separator: SEARCH_ADDRESS.separator,
            container: SEARCH_ADDRESS.container,
          }}
          nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
          renderLeftButton={() => (
            <View style={SEARCH_ADDRESS.leftButtonAutocomplete}>
              <MaterialCommunityIcons
                name="map-marker-plus"
                size={25}
                color={'white'}
              />
            </View>
          )}
        />
        <View style={SEARCH_ADDRESS.currentLocContainer}>
          <View style={SEARCH_ADDRESS.currentLocIconContainer}>
            <IconFontAwesome name="location-arrow" size={20} color="#1e1e1e" />
          </View>
          <Pressable
            onPress={() => {
              addressViewModel.currentAddressDecode
                ? navigation.navigate('EditAddress')
                : addressViewModel.getCurrentPosition();
            }}
            style={SEARCH_ADDRESS.flex_1}>
            <Text style={SEARCH_ADDRESS.currentText}>Posición actual</Text>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={SEARCH_ADDRESS.textLeft}>
              {addressViewModel?.currentAddressDecode?.address || ''}
            </Text>
          </Pressable>
        </View>
        <Pressable
          onPress={() =>
            navigation.navigate('EditAddress', {
              position: {
                lat:
                  addressViewModel.currentAddressDecode?.position?.latitude ||
                  6.265694883502279,
                lng:
                  addressViewModel.currentAddressDecode?.position?.longitude ||
                  -75.57499703822191,
              },
              address: 'Cl. 67, Medellín, Antioquia',
            })
          }
          style={SEARCH_ADDRESS.setOnMapContainer}>
          <IconFontAwesome name="map" size={20} color="#1e1e1e" />
          <View style={SEARCH_ADDRESS.textSetMapContainer}>
            <Text style={SEARCH_ADDRESS.hintNotFound}>
              ¿No encuentras la dirección?
            </Text>
            <Text style={SEARCH_ADDRESS.textLeft}>
              Fija ladirección en el mapa
            </Text>
          </View>
          <IconFontAwesome name="angle-right" size={20} color="#1e1e1e" />
        </Pressable>
      </View>
    </ScrollView>
  );
}
