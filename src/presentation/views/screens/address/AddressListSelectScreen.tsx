import React, {useEffect, useState} from 'react';
import {Pressable, SafeAreaView, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useDispatch, useSelector} from 'react-redux';
import {StoreState} from '../../../../data/redux/reducers';
import {resetAdressUpdateState} from '../../../../data/redux/slices/address/address.slice';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';
import {createAddressViewModel} from '../../../view-models/address-view-model';
import CustomButton from '../../components/CustomButton';
import {LIST_ADDRESS_STYLES} from '../../styles/address.styles';

export default function AddressListSelectScreen({navigation}) {
  const addressViewModel = createAddressViewModel(
    useSelector((state: StoreState) => state.address)
  );

  const [showOptions, setshowOptions] = useState<any>({});

  const dispatch = useDispatch();

  useEffect(() => {
    addressViewModel.getAddressesByUser();
  }, []);

  useEffect(() => {
    if (addressViewModel.addressCreatedOrUpdated) {
      setshowOptions({});
      dispatch(resetAdressUpdateState(undefined));
    } else if (addressViewModel.addressCreatedOrUpdated === false) {
      const addressError: DialogMessage = {
        title: {
          text: 'La dirección no puede ser eliminada',
          color: '#005fab',
        },
        message:
          'La dirección no ha podido ser eliminada. Recuerda que debes conservar por lo menos una dirección.',
        button: {
          color: '#005fab',
          text: 'OK',
        },
        pop: 1,
      };
      navigation.navigate('DialogModal', addressError);
      dispatch(resetAdressUpdateState(undefined));
    }

    if (addressViewModel.defaultAddressWillUpdated) {
      addressViewModel.resetCurrentState();
      navigation.goBack();
    }
  }, [
    addressViewModel.addressListByUser,
    addressViewModel.defaultAddressWillUpdated,
  ]);

  const updateAddress = (address: any) => {
    address.id &&
      !address.isDefault &&
      addressViewModel.setDefaultAddress(address.id);
  };

  return (
    <SafeAreaView style={LIST_ADDRESS_STYLES.flex_1}>
      <View style={LIST_ADDRESS_STYLES.flex_1}>
        <Pressable
          onPress={() => navigation.pop()}
          style={LIST_ADDRESS_STYLES.timesContainer}>
          <Icon name="times" size={20} color="#424242" />
        </Pressable>
        {addressViewModel?.addressListByUser?.length > 0 ? (
          <>
            <Text style={LIST_ADDRESS_STYLES.addressHint}>
              Escoge una dirección de entrega
            </Text>
            <ScrollView style={LIST_ADDRESS_STYLES.scrollviewContainer}>
              {addressViewModel.addressListByUser.map((elm, key) => {
                return (
                  <Pressable
                    key={elm.id}
                    style={LIST_ADDRESS_STYLES.addressContainer}>
                    <Icon name="map-signs" size={30} color="#212121" />
                    <View style={LIST_ADDRESS_STYLES.addressTextContainer}>
                      <Pressable
                        onPress={() => {
                          updateAddress(elm);
                        }}
                        key={`${elm.id}${key}`}>
                        <Text
                          numberOfLines={showOptions[key] ? 1 : 2}
                          ellipsizeMode={showOptions[key] ? 'tail' : undefined}
                          style={LIST_ADDRESS_STYLES.addressText}>
                          {elm.address}
                        </Text>
                        <Text
                          numberOfLines={showOptions[key] ? 1 : 2}
                          ellipsizeMode={showOptions[key] ? 'tail' : undefined}
                          style={LIST_ADDRESS_STYLES.placeAddressText}>
                          {elm.name}
                        </Text>
                      </Pressable>
                    </View>
                    {showOptions[key] && (
                      <View style={LIST_ADDRESS_STYLES.containOptions}>
                        <View
                          style={[
                            LIST_ADDRESS_STYLES.editBtn,
                            {backgroundColor: '#1e1e1e'},
                          ]}>
                          <Pressable
                            onPress={() =>
                              setshowOptions({
                                ...showOptions,
                                [key]: !showOptions[key],
                              })
                            }
                            key={`${elm.id}${key}`}>
                            <Text style={LIST_ADDRESS_STYLES.cancelBtn}>
                              CANCELAR
                            </Text>
                          </Pressable>
                        </View>
                        <View style={LIST_ADDRESS_STYLES.trashBtn}>
                          <Pressable
                            onPress={() =>
                              addressViewModel.disabledAddress(elm.id)
                            }
                            key={`${elm.id}${key}`}>
                            <Icon name="trash-alt" size={20} color="#D13438" />
                          </Pressable>
                        </View>
                        <View style={LIST_ADDRESS_STYLES.editBtn}>
                          <Pressable
                            onPress={() =>
                              navigation.navigate('EditAddress', {
                                position: {
                                  lat: elm?.position?.latitude,
                                  lng: elm.position?.longitude,
                                },
                                address: elm.address,
                                name: elm.name,
                                details: elm.details,
                                isDefault: elm.isDefault,
                                id: elm.id,
                              })
                            }
                            key={`${elm.id}${key}`}>
                            <Icon name="edit" size={20} color="#005DA9" />
                          </Pressable>
                        </View>
                      </View>
                    )}
                    {elm.isDefault && !showOptions[key] && (
                      <Icon
                        style={LIST_ADDRESS_STYLES.defaultCheck}
                        name="check"
                        size={20}
                        color="#212121"
                      />
                    )}
                    {!showOptions[key] && (
                      <Pressable
                        onPress={() =>
                          setshowOptions({
                            ...showOptions,
                            [key]: !showOptions[key],
                          })
                        }
                        style={LIST_ADDRESS_STYLES.ph_10}
                        key={`${elm.id}${key}`}>
                        <Icon name="ellipsis-v" size={30} color="#212121" />
                      </Pressable>
                    )}
                  </Pressable>
                );
              })}
            </ScrollView>
          </>
        ) : (
          <View style={LIST_ADDRESS_STYLES.addressEmpty}>
            <Text style={LIST_ADDRESS_STYLES.addressHint}>
              Actualmente no tienes registrada alguna dirección
            </Text>
          </View>
        )}

        <View style={LIST_ADDRESS_STYLES.btnContainer}>
          <CustomButton
            colorText="white"
            colorButton="blue"
            styleText={LIST_ADDRESS_STYLES.addAddressBtnText}
            styleButton={LIST_ADDRESS_STYLES.addAddressBtn}
            textButton="Añadir dirección"
            buttonProps={{
              title: 'Añadir dirección',
              onPress: () => {
                navigation.navigate('AddressLocation');
              },
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}
