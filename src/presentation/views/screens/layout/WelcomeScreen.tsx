import React from 'react';
import {Dimensions, Pressable, Text, View} from 'react-native';
import StorageData from '../../../../data/StorageData';
import LayoutEntity from '../../../../domain/entity/model/LayoutEntity';
import SliderComponent from '../../components/SliderComponent';
import {WELCOME_STYLES} from '../../styles/ScreensStyles';

const asyncStorage = new StorageData();
const layoutEntity = new LayoutEntity(asyncStorage);

export default function WelcomeScreen({navigation}) {
  const welcomeImage = [
    {
      image: require('../../../../../assets/img/welcome/Bienvenida-1.png'),
    },
    {
      image: require('../../../../../assets/img/welcome/Bienvenida-2.png'),
    },
    {
      image: require('../../../../../assets/img/welcome/Bienvenida-3.png'),
    },
  ];

  const {width} = Dimensions.get('window');

  return (
    <View style={WELCOME_STYLES.container}>
      <SliderComponent
        screen={'welcome'}
        slideElements={welcomeImage}
        wrap={false}
        rounded={false}
        stylesSlide={{width: width}}
        enabledControls={true}
      />
      <Pressable
        style={WELCOME_STYLES.skipBtn}
        onPress={() => {
          layoutEntity.setEnableTutorialStatus();
          setTimeout(() => {
            navigation.navigate('Main');
          }, 1000);
        }}>
        <Text style={WELCOME_STYLES.skipText}>Saltar</Text>
      </Pressable>
    </View>
  );
}
