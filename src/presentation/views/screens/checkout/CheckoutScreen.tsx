import React, {useEffect, useState} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {
  ENVIRONMENT,
  HTTP_STATUS,
} from '../../../../data/constants/api.constants';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createAddressViewModel} from '../../../view-models/address-view-model';
import {StoreState} from '../../../../data/redux/reducers';
import {useSelector} from 'react-redux';
import CustomButton from '../../components/CustomButton';
import {useFocusEffect, useIsFocused} from '@react-navigation/core';
import ProductEntity from '../../../../domain/entity/model/ProductEntity';
import {createShoppingCartViewModel} from '../../../view-models/shopping-cart-view-model';
import {createInvoiceViewModel} from '../../../view-models/invoice-view-model';
import {DialogMessage} from '../../../../domain/entity/structure/user.model';

const productEntity = new ProductEntity();

export default function CheckoutScreen({navigation}) {
  const shoppingCartViewModel = createShoppingCartViewModel(
    useSelector((state: StoreState) => state.shoppingCart)
  );

  const invoiceViewModel = createInvoiceViewModel(
    useSelector((state: StoreState) => state.invoice)
  );

  const [queryAddress, setqueryAddress] = useState(false);

  const isFocused = useIsFocused();
  if (isFocused && !queryAddress) {
    invoiceViewModel.preInvoice();
    setqueryAddress(true);
  }

  useFocusEffect(
    React.useCallback(() => {
      if (invoiceViewModel.preInvoiceData.address === undefined) {
        const checkoutError: DialogMessage = {
          title: {
            text: 'Atención',
            color: '#005fab',
          },
          message:
            'Para continuar con tu compra es necesario agregar una dirección de entrega.',
          button: {
            color: '#005fab',
            text: 'OK',
          },
          pop: 1,
        };
        navigation.navigate('AddressModalSelect');
        navigation.navigate('DialogModal', checkoutError);
      }
      setqueryAddress(false);
      invoiceViewModel.preInvoice();
    }, [])
  );

  useEffect(() => {
    if (
      shoppingCartViewModel.responseGenerateInvoice?.status ===
      HTTP_STATUS.CREATED
    ) {
      navigation.navigate('GeneratedInvoice', {
        message:
          shoppingCartViewModel.responseGenerateInvoice.responseData.data,
      });
    }
  }, [shoppingCartViewModel.responseGenerateInvoice]);

  const separator = () => {
    return (
      <View style={{backgroundColor: '#e0e0e0', height: 7, width: '100%'}} />
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView
        contentContainerStyle={{flexGrow: 1, backgroundColor: '#e0e0e0'}}>
        <View style={{backgroundColor: 'white'}}>
          <Text
            style={{
              fontSize: 18,
              color: '#424242',
              fontWeight: '700',
              paddingHorizontal: 10,
              paddingTop: 10,
            }}>
            Tus productos
          </Text>
          {invoiceViewModel.preInvoiceData?.products?.map((product, index) => {
            return (
              <View
                key={`product${index}`}
                style={{flexDirection: 'row', padding: 10}}>
                <Image
                  style={{width: 40, height: 40}}
                  source={{
                    uri: `${ENVIRONMENT.IMAGES_BASE_URL}/${product.image_url}`,
                  }}
                  resizeMode="contain"
                />
                <View style={{flex: 1, paddingHorizontal: 15}}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{color: '#212121', fontWeight: '700'}}>
                    {product.name}
                  </Text>
                  <Text
                    style={{fontWeight: '300', color: '#757575', fontSize: 12}}>
                    {product.quantity && product.quantity > 1
                      ? `${product.quantity} unidades`
                      : `${product.quantity} unidad`}
                  </Text>
                </View>
              </View>
            );
          })}
        </View>
        {separator()}
        <View style={{backgroundColor: 'white', paddingBottom: 10}}>
          <Text
            style={{
              fontSize: 18,
              color: '#424242',
              fontWeight: '700',
              paddingHorizontal: 10,
              paddingTop: 10,
            }}>
            Dirección de entrega
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{padding: 10}}>
              <MaterialCommunityIcons
                name="map-marker-check-outline"
                size={25}
                color={'#007E33'}
              />
            </View>
            <View style={{justifyContent: 'center', flex: 1}}>
              <Text style={{color: '#757575', fontWeight: '700', fontSize: 10}}>
                Dirección seleccionada
              </Text>
              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                style={{fontSize: 12, color: '#424242', fontWeight: '700'}}>
                {invoiceViewModel.preInvoiceData?.address || ''}
              </Text>
            </View>
            <View style={{paddingHorizontal: 10}}>
              <CustomButton
                colorText="red"
                colorButton="white"
                styleText={{
                  fontSize: 14,
                  fontWeight: '700',
                  textAlign: 'center',
                }}
                styleButton={{backgroundColor: 'white'}}
                textButton="EDITAR"
                buttonProps={{
                  title: 'EDITAR',
                  onPress: () => navigation.navigate('AddressModalSelect'),
                }}
              />
            </View>
          </View>
        </View>
        {separator()}
        <View style={{backgroundColor: 'white'}}>
          <Text
            style={{
              fontSize: 18,
              color: '#424242',
              fontWeight: '700',
              paddingHorizontal: 10,
              paddingTop: 10,
            }}>
            Método de envío
          </Text>
          <Text
            style={{
              color: '#757575',
              fontWeight: '500',
              fontSize: 14,
              paddingHorizontal: 10,
              paddingBottom: 10,
            }}>
            Domicilio surtiofertas
          </Text>
        </View>
        {separator()}
        <View style={{backgroundColor: 'white'}}>
          <Text
            style={{
              fontSize: 18,
              color: '#424242',
              fontWeight: '700',
              paddingHorizontal: 10,
              paddingTop: 10,
            }}>
            Método de pago
          </Text>
          <Text
            style={{
              color: '#757575',
              fontWeight: '500',
              fontSize: 14,
              paddingHorizontal: 10,
              paddingBottom: 10,
            }}>
            Efectivo contraentrega
          </Text>
        </View>
        {separator()}
        <View style={{backgroundColor: 'white'}}>
          <Text
            style={{
              fontSize: 18,
              color: '#424242',
              fontWeight: '700',
              paddingHorizontal: 10,
              paddingTop: 10,
            }}>
            Resumen de tu compra
          </Text>
          <View style={{paddingHorizontal: 10, paddingBottom: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  flex: 1,
                  color: '#757575',
                  fontWeight: '500',
                  fontSize: 14,
                }}>
                Costo productos
              </Text>
              <Text
                style={{
                  color: '#757575',
                  fontWeight: '500',
                  fontSize: 14,
                }}>
                {productEntity.formatPrice(
                  (invoiceViewModel.preInvoiceData?.subtotal || 0) +
                    (invoiceViewModel.preInvoiceData?.taxes || 0)
                )}
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  flex: 1,
                  color: '#757575',
                  fontWeight: '500',
                  fontSize: 14,
                }}>
                Costo de envío
              </Text>
              <Text
                style={{
                  color: '#757575',
                  fontWeight: '500',
                  fontSize: 14,
                }}>
                {productEntity.formatPrice(
                  invoiceViewModel.preInvoiceData?.deliveryCost || 0
                )}
              </Text>
            </View>
          </View>
        </View>
        {separator()}
      </ScrollView>
      <View style={{padding: 10}}>
        <CustomButton
          colorText="white"
          colorButton="blue"
          styleText={{fontWeight: '700', textAlign: 'center'}}
          styleButton={{
            alignSelf: 'center',
            paddingVertical: 15,
            borderRadius: 15,
            width: '100%',
          }}
          textButton={`Hacer pedido ${productEntity.formatPrice(
            (invoiceViewModel.preInvoiceData?.subtotal || 0) +
              (invoiceViewModel.preInvoiceData?.taxes || 0) +
              (invoiceViewModel.preInvoiceData?.deliveryCost || 0)
          )}`}
          buttonProps={{
            title: 'Hacer pedido',
            onPress: () => {
              invoiceViewModel.generateInvoice(
                invoiceViewModel.preInvoiceData?.deliveryMethods?.find(
                  (elm) => elm.isDefault
                )?.id || 1,
                invoiceViewModel.preInvoiceData?.paymentMethods?.find(
                  (elm) => elm.isDefault
                )?.id || 1
              );
            },
          }}
        />
      </View>
    </SafeAreaView>
  );
}
