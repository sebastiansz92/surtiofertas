import React, {useEffect, useState} from 'react';
import {SafeAreaView, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from './menu/HomeScreen';
import AccountScreen from './user/AccountScreen';
import FavoritesScreen from './menu/FavoritesScreen';
import AppTabBar from '../components/AppTabBar.component';
import SearchScreen from './menu/SearchScreen';
import {createLoginViewModel} from '../../view-models/login-view-model';
import {StoreState} from '../../../data/redux/reducers';
import {useSelector} from 'react-redux';

const BottomTab = createBottomTabNavigator();

export default function MainScreen() {
  const [isUserLogged, setisUserLogged] = useState<boolean | undefined>(
    undefined
  );

  const loginViewModel = createLoginViewModel(
    useSelector((state: StoreState) => state.login)
  );

  useEffect(() => {
    currentUserInfo();
  }, []);

  useEffect(() => {
    if (loginViewModel.reloadUserInfo) {
      setisUserLogged(false);
    }
  }, [loginViewModel.reloadUserInfo]);

  const currentUserInfo = () => {
    async function getCurrentUser() {
      const user = await loginViewModel.getCurrentUserToken();
      if (user) {
        setisUserLogged(user !== undefined);
      }
    }
    getCurrentUser();
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <BottomTab.Navigator tabBar={(props) => <AppTabBar {...props} />}>
          <BottomTab.Screen
            options={{title: 'Inicio'}}
            name="Home"
            component={HomeScreen}
          />
          <BottomTab.Screen
            options={{title: 'Favoritos'}}
            name="Favorites"
            component={FavoritesScreen}
          />
          <BottomTab.Screen
            options={{title: 'Buscar'}}
            name="Search"
            component={SearchScreen}
          />
          {/* <BottomTab.Screen
            options={{title: 'Soporte'}}
            name="Support"
            component={SupportScreen}
          /> */}
          {isUserLogged && (
            <BottomTab.Screen
              options={{title: 'Cuenta'}}
              name="Account"
              component={AccountScreen}
            />
          )}
        </BottomTab.Navigator>
      </View>
    </SafeAreaView>
  );
}
