import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import WelcomeScreen from './layout/WelcomeScreen';
import MainScreen from './MainScreen';
import DetailProductScreen from './product/DetailProductScreen';
import HeaderLogo from '../components/HeaderLogo.component';
import HeaderMainComponent from '../components/HeaderMain.component';
import {Platform} from 'react-native';
import AddressListSelectScreen from './address/AddressListSelectScreen';
import RecoveryScreen from './user/RecoveryScreen';
import ValidateCodeScreen from './user/ValidateCodeScreen';
import UpdatePasswordScreen from './user/UpdatePasswordScreen';
import LoginScreen from './user/LoginScreen';
import SignupScreen from './user/SignupScreen';
import AddressLocationScreen from './address/AddressLocationScreen';
import EditAddressScreen from './address/EditAddressScreen';
import CategoryListScreen from './product/CategoryListScreen';
import HeaderRightProductDetails from './product/HeaderRightProductDetails';
import StorageData from '../../../data/StorageData';
import LayoutEntity from '../../../domain/entity/model/LayoutEntity';
import CategoryDetails from './product/CategoryDetails';
import ShoppinCartProductAddedScreen from './ShoppingCart/ShoppinCartProductAddedScreen';
import ShoppingCartListScreen from './ShoppingCart/ShoppingCartListScreen';
import CheckoutScreen from './checkout/CheckoutScreen';
import GeneratedInvoiceScreen from './invoice/GeneratedInvoiceScreen';
import InvoiceListScreen from './invoice/InvoiceListScreen';
import InvoiceStateScreen from './invoice/InvoiceStateScreen';
import AccountScreen from './user/AccountScreen';

const asyncStorage = new StorageData();
const layoutEntity = new LayoutEntity(asyncStorage);

const RootScreen = () => {
  const [enableWelcomeScreen, setenableWelcomeScreen] = useState<
    boolean | undefined
  >(undefined);
  useEffect(() => {
    layoutEntity.getEnableTutorialStatus().then((enabled) => {
      if (!enabled) {
        setenableWelcomeScreen(true);
      } else {
        setenableWelcomeScreen(false);
      }
    });
  }, []);

  const Stack = createStackNavigator();

  const headerLogoParams = {
    headerStyle: {
      backgroundColor: '#ffed00',
      borderBottomLeftRadius: 30,
      borderBottomRightRadius: 30,
      height: 150,
    },
    headerTitle: () => <HeaderLogo />,
  };

  return (
    <>
      {enableWelcomeScreen !== undefined && (
        <Stack.Navigator
          screenOptions={{
            cardStyle: {
              backgroundColor: 'white',
            },
          }}
          headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
          initialRouteName="Welcome">
          {enableWelcomeScreen && (
            <Stack.Screen
              name="Welcome"
              options={{headerShown: false}}
              component={WelcomeScreen}
            />
          )}

          <Stack.Screen
            options={{
              headerStyle: {
                backgroundColor: '#ffed00',
                height: Platform.OS === 'android' ? 50 : 100,
                elevation: 0,
              },
              headerTitle: () => <HeaderMainComponent />,
              headerLeft: () => null,
            }}
            name="Main"
            component={MainScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
            }}
            name="RecoveryPassword"
            component={RecoveryScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
            }}
            name="ValidateCode"
            component={ValidateCodeScreen}
          />
          <Stack.Screen
            options={{
              headerTitle: '',
              headerRight: HeaderRightProductDetails,
            }}
            name="DetailProdut"
            component={DetailProductScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
            }}
            name="UpdatePassword"
            component={UpdatePasswordScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
            }}
            name="Login"
            component={LoginScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
            }}
            name="Signup"
            component={SignupScreen}
          />
          <Stack.Screen
            name="AddressModalSelect"
            options={{headerShown: false}}
            component={AddressListSelectScreen}
          />
          <Stack.Screen
            name="AddressLocation"
            component={AddressLocationScreen}
            options={{
              title: '',
            }}
          />
          <Stack.Screen
            options={{
              title: '',
            }}
            name="EditAddress"
            component={EditAddressScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
            }}
            name="CategoryList"
            component={CategoryListScreen}
          />
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name="CategoryDetails"
            component={CategoryDetails}
          />
          <Stack.Screen
            options={{
              headerTitle: '',
            }}
            name="ShoppinCartProductAdded"
            component={ShoppinCartProductAddedScreen}
          />
          <Stack.Screen
            options={{
              headerTitle: 'Carrito',
              headerTitleAlign: 'center',
            }}
            name="ShoppinCartList"
            component={ShoppingCartListScreen}
          />
          <Stack.Screen
            options={{
              headerTitle: 'Tu pedido',
              headerTitleAlign: 'center',
            }}
            name="CheckoutDetails"
            component={CheckoutScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: headerLogoParams.headerStyle,
              headerTitle: headerLogoParams.headerTitle,
              headerLeft: () => null,
            }}
            name="GeneratedInvoice"
            component={GeneratedInvoiceScreen}
          />
          <Stack.Screen
            options={{
              headerTitle: 'Tu pedidos pendientes',
              headerTitleAlign: 'center',
            }}
            name="InvoiceList"
            component={InvoiceListScreen}
          />
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name="InvoiceStates"
            component={InvoiceStateScreen}
          />
          <Stack.Screen
            options={{
              headerStyle: {
                backgroundColor: '#ffed00',
                height: Platform.OS === 'android' ? 50 : 100,
                elevation: 0,
              },
              headerTitle: () => <HeaderMainComponent />,
              headerLeft: () => null,
            }}
            name="Account"
            component={AccountScreen}
          />
        </Stack.Navigator>
      )}
    </>
  );
};

export default RootScreen;
