import {StyleSheet} from 'react-native';

export const HEADER_LOGO_STYLES = StyleSheet.create({
  viewContainer: {
    width: '100%',
    height: 150,
    alignSelf: 'center',
    marginTop: 10,
    flexDirection: 'column',
    paddingHorizontal: 40,
  },
  imageLogo: {
    width: '100%',
    resizeMode: 'contain',
  },
});
