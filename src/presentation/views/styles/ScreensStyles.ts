import {Dimensions, StyleSheet} from 'react-native';

export const LOGIN_STYLES = StyleSheet.create({
  loginContainer: {flex: 1, backgroundColor: 'white'},
  headerContainer: {
    backgroundColor: '#ffed00',
    height: 200,
    paddingTop: 40,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    paddingHorizontal: '15%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  alphaImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  loginText: {
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
    marginVertical: 30,
  },
  formContainer: {flex: 1, alignSelf: 'center', width: '70%'},
  inputStyle: {
    marginVertical: 12,
  },
  forgetText: {alignSelf: 'center', color: '#bdbdbd', fontWeight: '700'},
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 260,
  },
  loginBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    backgroundColor: '#005fab',
    width: '75%',
    marginVertical: 12,
  },
  loginBtnText: {
    color: 'white',
    fontWeight: '700',
    textAlign: 'center',
  },
  signupBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    width: '75%',
    marginTop: 12,
    marginBottom: 25,
  },
  signupBtnText: {
    color: '#005fab',
    fontWeight: '700',
    textAlign: 'center',
  },
});

export const SIGNUP_STYLES = StyleSheet.create({
  signupText: {
    marginVertical: 10,
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
  },

  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 0,
  },
});

export const RECOVERY_STYLES = StyleSheet.create({
  recoveryContainer: {flex: 1, backgroundColor: 'white'},
  alphaImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  recoveryText: {
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
    marginTop: 70,
  },
  recoveryDescription: {
    color: '#494949',
    fontSize: 14,
    fontWeight: '700',
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 30,
    paddingLeft: 40,
    paddingRight: 40,
  },
  formContainer: {flex: 1, alignSelf: 'center', width: '70%'},
  inputStyle: {
    marginVertical: 12,
  },
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 60,
  },
  recoveryBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    width: '75%',
    marginVertical: 12,
  },
  recoveryBtnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
});

export const VALIDATECODE_STYLES = StyleSheet.create({
  flex_row: {flexDirection: 'row'},
  validateCodeContainer: {flex: 1, backgroundColor: 'white'},
  alphaImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  validateCodeText: {
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
    marginTop: 70,
  },
  validateCodeDescription: {
    color: '#494949',
    fontSize: 14,
    fontWeight: '700',
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 30,
    paddingLeft: 40,
    paddingRight: 40,
  },
  formContainer: {flex: 1, alignSelf: 'center', width: '70%'},
  inputStyle: {
    flex: 1,
    borderBottomColor: '#005EA9',
    borderBottomWidth: 4,
    backgroundColor: 'rgba(0, 94, 169, 0.1)',
    marginRight: 20,
  },
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  validateCodeBtn: {
    alignSelf: 'center',
    paddingVertical: 10,
    borderRadius: 15,
    width: '45%',
    marginVertical: 12,
  },
  validateCodeSendBtn: {
    alignSelf: 'center',
    paddingVertical: 10,
    borderRadius: 15,
    width: '100%',
    marginVertical: 12,
  },
  validateCodeBtnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
  validateCodeSendBtnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
});

export const DETAILPRODUCT_STYLES = StyleSheet.create({
  container: {flex: 1},
  mh10: {marginHorizontal: 10},
  background: {flex: 1, backgroundColor: '#fafafa'},
  scrollContainer: {
    flexGrow: 1,
    padding: 20,
    paddingBottom: 100,
  },
  contain: {
    resizeMode: 'cover',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  containerAmountProduct: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  containerOptions: {
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerAddProduct: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleProduct: {
    color: '#005EAA',
    fontSize: 20,
  },
  counter: {
    // flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    // paddingHorizontal: 10,
    backgroundColor: 'white',
    borderRadius: 15,
    // height: '100%',
    padding: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  amount: {
    flex: 1,
    textAlign: 'center',
    // paddingHorizontal: 20,
    color: '#494949',
    fontSize: 15,
    fontWeight: '700',
  },
  minus: {
    paddingLeft: 10,
    color: '#494949',
    fontSize: 20,
    fontWeight: '700',
  },
  plus: {
    paddingRight: 10,
    color: '#494949',
    fontSize: 20,
    fontWeight: '700',
  },
  netContent: {
    fontWeight: 'normal',
    marginBottom: 10,
  },
  containFavorites: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containIcon: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containAdd: {
    width: '40%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  containPriceButton: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  addFavorites: {
    fontSize: 16,
    marginLeft: 10,
    color: '#2E2E2E',
    fontWeight: '700',
  },
  containPrice: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  containerAmount: {
    // width: '50%',
    width: 100,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  price: {
    color: '#494949',
    fontSize: 20,
    fontWeight: '700',
  },
  priceButton: {
    color: '#FFFF',
    fontSize: 15,
    fontWeight: '700',
  },
  textAddButton: {
    color: '#FFFF',
    fontSize: 15,
    fontWeight: '700',
  },
  descriptionProduct: {
    color: '#494949',
    fontSize: 17,
    marginBottom: 10,
    marginTop: 10,
  },
  formContainer: {
    flex: 1,
    position: 'absolute',
    bottom: 10,
    left: 20,
    right: 20,
    alignSelf: 'center',
  },
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  addProductBtn: {
    backgroundColor: '#005EA9',
    paddingVertical: 10,
    borderRadius: 15,
    width: '100%',
  },
  validateCodeBtnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
  validateCodeSendBtnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
  imageProduct: {
    width: '100%',
    height: 300,
  },
});

export const UPDATEPASSWORD_STYLES = StyleSheet.create({
  updateContainer: {flex: 1, backgroundColor: 'white'},
  headerContainer: {
    backgroundColor: '#ffed00',
    height: 200,
    paddingTop: 40,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    paddingHorizontal: '15%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  alphaImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  loginText: {
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
    marginVertical: 30,
  },
  formContainer: {flex: 1, alignSelf: 'center', width: '70%'},
  inputStyle: {
    marginVertical: 12,
  },
  forgetText: {alignSelf: 'center', color: '#bdbdbd', fontWeight: '700'},
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 260,
  },
  loginBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    backgroundColor: '#005fab',
    width: '75%',
    marginVertical: 12,
  },
  loginBtnText: {
    color: 'white',
    fontWeight: '700',
    textAlign: 'center',
  },
  signupBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    width: '75%',
    marginTop: 12,
    marginBottom: 25,
  },
  signupBtnText: {
    color: '#005fab',
    fontWeight: '700',
    textAlign: 'center',
  },
  title: {
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
    marginTop: 70,
  },
  description: {
    color: '#494949',
    fontSize: 14,
    fontWeight: '700',
    textAlign: 'center',
    marginTop: 30,
    marginBottom: 30,
    paddingLeft: 40,
    paddingRight: 40,
  },
  btnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
  btn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    width: '75%',
    marginVertical: 12,
  },
});

export const FAVORITES_STYLES = StyleSheet.create({
  container: {flex: 1, width: '100%'},
  headerDimentions: {marginTop: 100},
  bold: {fontWeight: '700'},
  userNotAuthContainer: {
    flex: 1,
    marginTop: '20%',
    paddingHorizontal: 30,
  },
  textUserNotAuth: {textAlign: 'center', color: '#424242', fontSize: 15},
  loginButton: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    width: '85%',
    marginTop: 30,
  },
  titleSave: {
    color: '#005EAA',
    fontSize: 25,
    marginLeft: 10,
    fontWeight: 'bold',
  },
  titleRecentsOrders: {
    color: '#005EAA',
    fontSize: 25,
    fontWeight: 'bold',
  },
  scrollViewSaved: {
    marginTop: 20,
  },
  scrollViewOders: {
    marginTop: 20,
  },
  imageProduct: {
    width: 80,
    height: 80,
  },
  product: {
    width: 100,
    height: 200,
    margin: 10,
  },
  horizontalProductContainer: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: 100,
    height: 100,
    elevation: 5,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  price: {
    color: '#005EAA',
    fontSize: 15,
    fontWeight: 'bold',
  },
  date: {
    color: '#939395',
    fontSize: 13,
  },
  dateAlign: {
    alignItems: 'flex-end',
  },
  description: {
    color: '#757575',
    fontSize: 12,
    fontWeight: 'bold',
  },
  content: {
    color: '#939395',
    fontSize: 13,
    // marginTop: 5,
  },
  contentVertical: {
    color: '#939395',
    fontSize: 12,
    fontWeight: 'bold',
    // marginTop: 5,
  },
  productOrders: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 10,
  },
  btnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
  btn: {
    alignSelf: 'center',
    paddingVertical: 15,
    borderRadius: 15,
    width: '85%',
    marginTop: 30,
    marginLeft: 35,
  },
  btnAgain: {
    alignItems: 'flex-end',
  },
  sizeImage: {
    marginRight: 10,
    // width: '20%',
  },
  sizeDescription: {
    flex: 1,
  },
  sizeDateBtn: {
    // width: 160,
  },
  emptyContainer: {
    flex: 1,
    padding: 20,
  },
  emptyMessage: {
    fontSize: 20,
    color: '#424242',
    fontWeight: 'bold',
  },
  supcationEmptyMessage: {
    fontSize: 14,
    color: '#424242',
  },
  categoryName: {
    width: '100%',
    padding: 10,
    marginTop: 0,
    backgroundColor: '#005DA8',
    justifyContent: 'center',
  },
  imageText: {
    fontWeight: 'bold',
    color: '#FFFFFF',
    fontSize: 16,
  },
  containerVerticalProductos: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export const CATEGORYLIST_STYLES = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: 20, backgroundColor: '#f5f5f5'},
  containerProducts: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  titleCategories: {
    color: '#005EAA',
    fontSize: 25,
    fontWeight: 'bold',
    paddingVertical: 10,
  },
  marginProduct: {
    marginTop: 20,
  },
});

export const WELCOME_STYLES = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    // paddingHorizontal: 10,
    // paddingBottom: 10,
    backgroundColor: '#ffed00',
  },
  skipBtn: {
    // bottom: 50,
  },
  skipText: {
    textAlign: 'center',
    color: '#005EA8',
    fontWeight: 'bold',
    fontSize: 25,
  },
});

export const SEARCH_STYLES = StyleSheet.create({
  inputStyle: {
    marginVertical: 12,
  },
  containerCategoryName: {
    padding: 10,
    backgroundColor: 'white',
  },
  containerSearch: {
    height: 70,
    width: '100%',
  },
  containerSearchItems: {
    flexDirection: 'row',
    backgroundColor: '#eeeeee',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  widthArroyLeft: {
    width: '10%',
  },
  pressableLeft: {
    alignItems: 'center',
  },
  widthInputSearch: {
    width: '80%',
  },
  widthSearch: {
    width: '10%',
  },
  pressableSearch: {
    alignItems: 'center',
  },
  categoryName: {
    fontWeight: 'bold',
    fontSize: 15,
    paddingLeft: 10,
    color: '#424242',
  },
  xIcon: {
    position: 'absolute',
    right: 0,
    height: 45,
    aspectRatio: 1,
    top: 10,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flex_1: {flex: 1},
});

export const GENERATEDINVOICE_STYLES = StyleSheet.create({
  generatedInvoiceContainer: {flex: 1, backgroundColor: 'white'},
  alphaImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: 150,
    marginTop: 50,
  },
  generatedInvoiceText: {
    color: '#494949',
    fontSize: 30,
    fontWeight: '700',
    marginTop: 40,
  },
  generatedInvoiceDescription: {
    color: '#494949',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 50,
    marginBottom: 30,
    paddingLeft: 40,
    paddingRight: 40,
  },
  formContainer: {flex: 1, alignSelf: 'center', width: '45%'},
  buttonsContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  generatedInvoiceContinueBtn: {
    alignSelf: 'center',
    paddingVertical: 10,
    borderRadius: 15,
    width: '100%',
    marginVertical: 12,
  },
  generatedInvoiceContinueBtnText: {
    fontWeight: '700',
    textAlign: 'center',
  },
});

export const ACCOUNT_STYLES = StyleSheet.create({
  accountContainer: {flex: 1, backgroundColor: 'white', alignItems: 'center'},
  alphaImage: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  accountProfileCircle: {
    marginTop: 30,
    borderRadius:
      Math.round(
        Dimensions.get('window').width + Dimensions.get('window').height
      ) / 2,
    width: Dimensions.get('window').width * 0.3,
    height: Dimensions.get('window').width * 0.3,
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderColor: '#005DA9',
    justifyContent: 'center',
    alignItems: 'center',
  },
  accountInitialLetterName: {
    fontSize: 55,
    color: '#005DA9',
  },
  accountTextName: {
    marginTop: 20,
    fontSize: 25,
    color: '#005DA9',
    fontWeight: 'bold',
  },
});
