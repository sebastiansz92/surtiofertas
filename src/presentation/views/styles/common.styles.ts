import {StyleSheet} from 'react-native';

export const GLOBAL_STYLES = StyleSheet.create({
  flex_1: {
    flex: 1,
  },

  inputContainer: {
    display: 'flex',
    flexDirection: 'row',
    zIndex: 1,
    height: 45,
  },

  iconContainer: {
    alignItems: 'center',
    height: '100%',
    aspectRatio: 1,
    justifyContent: 'center',
    padding: 12,
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
  },

  inputStyle: {
    height: '100%',
    backgroundColor: 'white',
    flex: 1,
    paddingLeft: 10,
    fontWeight: '600',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },

  backgroundYellow: {
    backgroundColor: '#FFEF00',
  },

  backgroundBlue: {
    backgroundColor: '#005EA9',
  },

  backgroundRed: {
    backgroundColor: '#DF040B',
  },

  backgroundWhite: {
    backgroundColor: '#FFFF',
  },

  backgroundGrey: {
    backgroundColor: '#9B9B9B',
  },

  backgroundInherit: {
    backgroundColor: 'transparent',
  },

  darkGrey: {
    color: '#414141',
  },

  yellow: {
    color: '#FFEF00',
  },

  blue: {
    color: '#005EA9',
  },

  red: {
    color: '#DF040B',
  },

  defaultColor: {
    color: '#FFFF',
  },
});
