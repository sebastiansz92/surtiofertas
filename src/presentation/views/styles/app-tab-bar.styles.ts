import {StyleSheet} from 'react-native';

export const APP_BAR_STYLES = StyleSheet.create({
  androidContainer: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingVertical: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
  },
  iosContainer: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingVertical: 5,
    borderTopColor: '#cecece',
    borderTopWidth: 1,
  },

  buttonMenu: {flex: 1, alignItems: 'center'},
  buttonIcon: {height: 25, aspectRatio: 1},
  textButton: {fontSize: 12, fontWeight: '700'},
});
