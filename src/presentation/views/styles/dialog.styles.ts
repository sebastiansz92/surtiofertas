import {StyleSheet} from 'react-native';

export const DIALOG_STYLES = StyleSheet.create({
  dialogContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w_dialog: {width: '80%'},
  header: {
    paddingVertical: 15,
    backgroundColor: 'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  body: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e0e0e0',
    paddingVertical: 30,
    paddingHorizontal: 10,
  },
  footer: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'white',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },

  textCenter: {textAlign: 'center'},
  messageDialog: {color: '#424242', textAlign: 'center'},
  actionBtnDialog: {
    width: '100%',
    paddingVertical: 5,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
