import AsyncStorage from '@react-native-async-storage/async-storage';

export default class StorageData {
  async storeData(key: string, value: string | null): Promise<any> {
    try {
      if (value) {
        await AsyncStorage.setItem(key, value);
      }
    } catch (e) {
      // saving error
    }
  }

  async getData(key: string): Promise<any> {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        return value;
      }
    } catch (e) {
      // error reading value
    }
  }

  async removeItem(key: string): Promise<any> {
    try {
      await AsyncStorage.removeItem(key);
    } catch (e) {
      // error reading value
    }
  }
}
