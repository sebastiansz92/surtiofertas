import {ProductDetails} from '../../domain/entity/structure/category-product.model';
import {AddOrRemoveFavoriteRequest} from '../../domain/entity/structure/favorite-product.model';
import {ProductsRepository} from '../../domain/repository/product.repository';
import {ROUTES} from '../constants/api.constants';
import HttpBaseApi from '../HttpBaseApi';

class ProductsApi extends HttpBaseApi implements ProductsRepository {
  removeAllProductsInCar(): Promise<Response> {
    return this.delete(`${ROUTES.SHOPPING_CART.BASE}`, null, true);
  }
  removeProductInCart(productId: number): Promise<Response> {
    return this.delete(
      `${ROUTES.SHOPPING_CART.REMOVE}/${productId}`,
      null,
      true
    );
  }
  getProductsInCart(): Promise<Response> {
    return this.get(`${ROUTES.SHOPPING_CART.BASE}`, null, true);
  }
  addProductToCar(
    product: ProductDetails,
    rewrite: boolean
  ): Promise<Response> {
    return this.put(
      `${ROUTES.SHOPPING_CART.BASE}${
        rewrite ? '?quantity_action=rewrite' : ''
      }`,
      product,
      null,
      true
    );
  }
  getProductsByCategory(category: number): Promise<Response> {
    return this.get(`${ROUTES.PRODUCT.GET_BY_CATEGORY}/${category}`);
  }
  getProductsBySearch(search: string): Promise<Response> {
    return this.get(`${ROUTES.PRODUCT.GET_BY_SEARCH}?searchingFor=${search}`);
  }
  validateFavorite(product: AddOrRemoveFavoriteRequest): Promise<Response> {
    return this.post(`${ROUTES.FAVORITE.VALIDATE}`, product, null, true);
  }
  addOrRemoveFavoriteProduct(
    product: AddOrRemoveFavoriteRequest
  ): Promise<Response> {
    return this.post(`${ROUTES.FAVORITE.ADD_REMOVE}`, product, null, true);
  }
  getFavoriteProductsByUser(): Promise<Response> {
    return this.get(`${ROUTES.FAVORITE.GET_ALL}`, null, true);
  }
  getProductsBySupperCategory(): Promise<Response> {
    return this.get(`${ROUTES.CATEGORY.GET_ALL_BY_SUPPERCATEGORY}?size=4`);
  }
  getCurrentOffers(): Promise<Response> {
    return this.get(`${ROUTES.OFFER}`);
  }
  getAllCategoriesProduct(): Promise<Response> {
    return this.get(`${ROUTES.CATEGORY.GET_ALL_CATEGORIES_PRODUCT}`);
  }
}

export default ProductsApi;
