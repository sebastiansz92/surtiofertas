import {RecoveryRequest} from '../../domain/entity/structure/recovery.model';
import {SignupRequest} from '../../domain/entity/structure/signup.model';
import {UpdatePasswordRequest} from '../../domain/entity/structure/update-password.model';
import {LoginRequest} from '../../domain/entity/structure/user.model';
import {ValidateCodeRequest} from '../../domain/entity/structure/validate-code.model';
import {ROUTES} from '../constants/api.constants';
import HttpBaseApi from '../HttpBaseApi';
import {UserRepository} from '../../domain/repository/user.repository';

class UserApi extends HttpBaseApi implements UserRepository {
  loginUser(user: LoginRequest): Promise<Response> {
    return this.post(`${ROUTES.USER.LOGIN}`, user);
  }

  signupUser(user: SignupRequest): Promise<Response> {
    return this.post(`${ROUTES.USER.SIGNUP}`, user);
  }

  recoveryPassword(user: RecoveryRequest): Promise<Response> {
    return this.post(`${ROUTES.USER.RECOVERY}`, user);
  }

  validateOTPCode(codeRecovery: ValidateCodeRequest): Promise<Response> {
    return this.post(`${ROUTES.USER.VALIDATE_CODE}`, codeRecovery);
  }

  updatePassword(passwordInfo: UpdatePasswordRequest): Promise<Response> {
    return this.post(`${ROUTES.USER.UPDATE_PASSWORD}`, passwordInfo);
  }
}

export default UserApi;
