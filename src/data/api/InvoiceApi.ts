import {
  CancelInvoiceRequest,
  GenerateInvoiceRequest,
} from '../../domain/entity/structure/invoice.model';
import {InvoiceRepository} from '../../domain/repository/invoice.repository';
import {ROUTES} from '../constants/api.constants';
import HttpBaseApi from '../HttpBaseApi';

class InvoiceApi extends HttpBaseApi implements InvoiceRepository {
  cancelInvoice(invoiceRequest: CancelInvoiceRequest): Promise<Response> {
    return this.put(`${ROUTES.INVOICE.CANCEL}`, invoiceRequest);
  }
  preInvoice(): Promise<Response> {
    return this.get(`${ROUTES.INVOICE.PRE}`, null, true);
  }
  getInvoicesByUser(): Promise<Response> {
    return this.get(`${ROUTES.INVOICE.BY_USER}&size=50`, null, true);
  }
  generateInvoice(invoiceRequest: GenerateInvoiceRequest): Promise<Response> {
    return this.post(`${ROUTES.INVOICE.BASE}`, invoiceRequest, null, true);
  }
  getRecentOrdersProductsByUser(): Promise<Response> {
    return this.get(`${ROUTES.INVOICE.BY_USER_SUCCESS}`, null, true);
  }

  getInvoiceDetailsById(orderId: number): Promise<Response> {
    return this.get(`${ROUTES.INVOICE.BASE}/${orderId}`, null, true);
  }
}

export default InvoiceApi;
