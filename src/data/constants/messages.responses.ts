export const ERROR = {
  EMAIL_EMPTY: '¡El correo electrónico es obligatorio!',
  CODE_EMPTY: '¡El código de verificación es obligatorio!',
  CODE_INVALID: '¡El código de verificación debe ser numérico!',
  EMAIL_INVALID: '¡El correo electrónico ingresado es inválido!',
  PASSWORD_EMPTY: '¡La contraseña es obligatoria!',
  NAME_EMPTY: '¡El nombre es obligatorio!',
  CELLPHONE_EMPTY: '¡El celular es obligatorio!',
  CELLPHONE_INVALID:
    '¡El celular ingresado no es válido!. Debes ingresar el valor numérico sin espacios o caracteres especiales (por ejemplo 3000000000)!',
  PASSWORD_CONFIRM_EMPTY: '¡Debes confirmar la contraseña!',
  PASSWORD_EQUAL: '¡Las contraseñas ingresadas no coinciden!',
  INCORRECT_LOGIN: '¡Usuario o contraseña son incorrectos!',
  NO_MATCH_PASSWORD: '¡Las contraseñas no coinciden!',
  ADDRESS_EMPTY: '¡Se debe ingresar la dirección!',
};
