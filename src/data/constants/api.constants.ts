export const ENVIRONMENT = {
  PROD: 'https://api.surtiofertas.com/supper/',
  IMAGES_BASE_URL: 'https://multimedia.surtiofertas.com',
  API_KEY: 'AIzaSyDFltYLYZ6O0_CSAZcxU-K3jXDr3YccEZU',
};

export const ROUTES = {
  USER: {
    LOGIN: 'login',
    SIGNUP: 'user/signup',
    RECOVERY: 'user/recoveryPassword',
    VALIDATE_CODE: 'user/validateOTPCode',
    UPDATE_PASSWORD: 'user/updatePassword',
  },
  OFFER: 'offer',
  ADDRESS: {
    GEOCODING_API: `https://maps.googleapis.com/maps/api/geocode/json?key=${ENVIRONMENT.API_KEY}&language=es-419`,
    DEFAULT_ADDRESS_USER: 'address/default',
    ADDRESSES_BY_USER: 'address',
  },
  CATEGORY: {
    GET_ALL_BY_SUPPERCATEGORY: 'categorySupper/productsGroupedBySupperCategory',
    GET_ALL_CATEGORIES_PRODUCT: 'category/getAllCategoriesProduct',
  },
  PRODUCT: {
    GET_BY_CATEGORY: 'product/byCategory',
    GET_BY_SEARCH: 'product',
  },
  FAVORITE: {
    ADD_REMOVE: 'favorite/addOrRemove',
    GET_ALL: 'favorite/getAll',
    VALIDATE: 'favorite/validate',
  },
  SHOPPING_CART: {
    BASE: 'cart',
    REMOVE: 'cart/product',
  },
  INVOICE: {
    BASE: 'invoice',
    BY_USER: 'invoice/by_user?state=pending',
    BY_USER_SUCCESS: 'invoice/by_user?state=success',
    PRE: 'invoice/pre',
    CANCEL: 'order_smachine/user_event',
  },
};

export const HTTP_STATUS = {
  OK: 200,
  CONFLICT: 409,
  NOT_FOUND: 404,
  UNAUTHORIZED: 401,
  INTERNAL_ERROR: 500,
  CREATED: 201,
  ACCEPTED: 202,
  NOT_CONTENT: 204,
  BAD_GATEWAY: 502,
  BAD_REQUEST: 400,
  FORBIDDEN: 403,
};
