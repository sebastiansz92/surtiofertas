export const FORM_PATTERNS = {
  EMAIL: '^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$',
  CELLPHONE: '^(\\d){10}$',
};
