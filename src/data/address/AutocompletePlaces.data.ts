import Geolocation, {
  GeolocationError,
  GeolocationResponse,
} from '@react-native-community/geolocation';
import {Platform} from 'react-native';
import {GPSPosition} from '../../domain/entity/structure/address.model';

class AutoCompletePlaces {
  constructor() {
    Platform.OS === 'ios' && this.setRNConfiguration();
  }

  private setRNConfiguration() {
    Geolocation.setRNConfiguration({
      authorizationLevel: 'whenInUse',
      skipPermissionRequests: true,
    });
  }

  public requestAuthorization() {
    Geolocation.requestAuthorization();
  }

  public getCurrentPosition(): Promise<GPSPosition> {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(
        (position: GeolocationResponse) => {
          resolve({position: position.coords, status: 'OK'});
        },
        (error: GeolocationError) => {
          reject({status: 'ERROR', error: error.message});
        }
      );
    });
  }
}

export default AutoCompletePlaces;
