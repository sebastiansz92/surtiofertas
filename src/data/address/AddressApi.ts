import {AddressValues} from '../../domain/entity/structure/address.model';
import {AddressRepository} from '../../domain/repository/address.repository';
import {ROUTES} from '../constants/api.constants';
import HttpBaseApi from '../HttpBaseApi';

class AddressApi extends HttpBaseApi implements AddressRepository {
  disableAddressByUser(addressId: number): Promise<Response> {
    return this.delete(
      `${ROUTES.ADDRESS.ADDRESSES_BY_USER}/${addressId}`,
      null,
      true
    );
  }
  AddAddressByUser(address: AddressValues): Promise<Response> {
    return this.post(
      `${ROUTES.ADDRESS.ADDRESSES_BY_USER}`,
      address,
      null,
      true
    );
  }
  updateAddressByUser(address: AddressValues): Promise<Response> {
    return this.put(`${ROUTES.ADDRESS.ADDRESSES_BY_USER}`, address, null, true);
  }
  getDefaultUserAddress(): Promise<Response> {
    return this.get(`${ROUTES.ADDRESS.DEFAULT_ADDRESS_USER}`, null, true);
  }
  getAddressesByUser(): Promise<Response> {
    return this.get(`${ROUTES.ADDRESS.ADDRESSES_BY_USER}`, null, true);
  }
  setDefaultAddressByUser(addressId: number): Promise<Response> {
    return this.put(
      `${ROUTES.ADDRESS.DEFAULT_ADDRESS_USER}/${addressId}`,
      {},
      null,
      true
    );
  }
}

export default AddressApi;
