import {LatLng} from 'react-native-maps';
import {MapsRepository} from '../../domain/repository/maps.repository';
import {ROUTES} from '../constants/api.constants';
import HttpBaseApi from '../HttpBaseApi';

class MapsApi extends HttpBaseApi implements MapsRepository {
  getAddressByCoords(position: LatLng): Promise<Response> {
    return this.get(
      `${ROUTES.ADDRESS.GEOCODING_API}&latlng=${position.latitude},${position.longitude}`,
      null,
      false,
      true
    );
  }
}

export default MapsApi;
