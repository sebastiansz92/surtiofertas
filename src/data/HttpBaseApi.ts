import StorageData from './StorageData';
import {ENVIRONMENT} from './constants/api.constants';
import {showGlobalLoading} from './redux/slices/layout.slice';
import store from './redux/store';

export default class HttpBaseApi {
  private baseUrl = `${ENVIRONMENT.PROD}`;
  private _asyncStorage;

  constructor() {
    this._asyncStorage = new StorageData();
  }

  protected async get(
    relativeUrl: string,
    headers?: any,
    auth: boolean = false,
    anotherBaseUrl: boolean = false
  ) {
    if (!headers) {
      headers = {
        'Content-Type': 'application/json',
      };
      if (auth) {
        headers = {
          ...headers,
          Authorization: await this._asyncStorage.getData('userToken'),
        };
      }
    }
    const url = `${anotherBaseUrl ? '' : this.baseUrl}${relativeUrl}`;
    store.dispatch(showGlobalLoading(true));
    return fetch(`${url}`, {
      method: 'GET',
      headers: headers,
    }).finally(() => {
      store.dispatch(showGlobalLoading(false));
    });
  }

  protected async post(
    relativeUrl: string,
    body: any,
    headers?: any,
    auth: boolean = false
  ) {
    if (!headers) {
      headers = {
        'Content-Type': 'application/json',
      };

      if (auth) {
        headers = {
          ...headers,
          Authorization: await this._asyncStorage.getData('userToken'),
        };
      }
    }
    store.dispatch(showGlobalLoading(true));
    return fetch(`${this.baseUrl}${relativeUrl}`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(body),
    }).finally(() => {
      store.dispatch(showGlobalLoading(false));
    });
  }
  protected async put(
    relativeUrl: string,
    body: any,
    headers?: any,
    auth: boolean = false
  ) {
    if (!headers) {
      headers = {
        'Content-Type': 'application/json',
      };

      if (auth) {
        headers = {
          ...headers,
          Authorization: await this._asyncStorage.getData('userToken'),
        };
      }
    }
    store.dispatch(showGlobalLoading(true));
    return fetch(`${this.baseUrl}${relativeUrl}`, {
      method: 'PUT',
      headers: headers,
      body: JSON.stringify(body),
    }).finally(() => {
      store.dispatch(showGlobalLoading(false));
    });
  }

  protected async delete(
    relativeUrl: string,
    headers?: any,
    auth: boolean = false
  ) {
    if (!headers) {
      headers = {
        'Content-Type': 'application/json',
      };

      if (auth) {
        headers = {
          ...headers,
          Authorization: await this._asyncStorage.getData('userToken'),
        };
      }
    }
    store.dispatch(showGlobalLoading(true));
    return fetch(`${this.baseUrl}${relativeUrl}`, {
      method: 'DELETE',
      headers: headers,
      // body: JSON.stringify(body),
    }).finally(() => {
      store.dispatch(showGlobalLoading(false));
    });
  }
}
