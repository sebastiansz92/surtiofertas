import {createSlice, PayloadAction} from '@reduxjs/toolkit';
export interface LayoutState {
  showGlobalLoading: boolean;
}

const initialState: LayoutState = {
  showGlobalLoading: false,
};

const layoutSlice = createSlice({
  name: 'layout',
  initialState,
  reducers: {
    showGlobalLoading: (state, action: PayloadAction<boolean>) => {
      state.showGlobalLoading = action.payload;
    },
  },
});

export const {showGlobalLoading} = layoutSlice.actions;

export default layoutSlice.reducer;
