import {Dispatch} from 'react';
import {GroupedProductsInBusinessCategoryFormated} from '../../../../domain/entity/structure/category-product.model';
import {
  CancelInvoiceRequest,
  GenerateInvoiceRequest,
} from '../../../../domain/entity/structure/invoice.model';
import {InvoiceInterface} from '../../../../domain/interactors/interfaces/invoice/invoice.interface';
import {generateInvoice} from '../products/cart/shoping-cart.slice';
import {
  cancelInvoice,
  getPendingInvoices,
  getRecentOrdersProductsByUser,
  invoiceDetails,
  preInvoice,
  updateRecentOrdersProductsByUser,
} from './invoice.slice';

export function createReduxInvoiceInterface(
  dispatch: Dispatch<{}>
): InvoiceInterface {
  return {
    generateInvoice(invoiceRequest: GenerateInvoiceRequest): void {
      dispatch(generateInvoice(invoiceRequest));
    },

    getPendingInvoicesByUser(): void {
      dispatch(getPendingInvoices());
    },
    getRecentOrdersProductsByUser(): void {
      dispatch(getRecentOrdersProductsByUser());
    },
    updateRecentOrdersProductsByUser(
      recentOrdersProductsByUserData: GroupedProductsInBusinessCategoryFormated[]
    ): void {
      dispatch(
        updateRecentOrdersProductsByUser(recentOrdersProductsByUserData)
      );
    },
    preInvoice(): void {
      dispatch(preInvoice());
    },

    getInvoiceDetailsByOrder(orderId: number): void {
      dispatch(invoiceDetails(orderId));
    },

    cancelInvoice(invoiceRequest: CancelInvoiceRequest): void {
      dispatch(cancelInvoice(invoiceRequest));
    },
  };
}
