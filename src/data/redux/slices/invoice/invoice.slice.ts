import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {GroupedProductsInBusinessCategoryFormated} from '../../../../domain/entity/structure/category-product.model';
import {
  CancelInvoiceRequest,
  InvoiceDetails,
  PreInvoiceResponse,
} from '../../../../domain/entity/structure/invoice.model';
import InvoiceApi from '../../../api/InvoiceApi';
import {HTTP_STATUS} from '../../../constants/api.constants';

export interface InvoiceState {
  currentInvoices: InvoiceDetails[];
  recentOrdersByUserData: Array<any>;
  preInvoiceData: PreInvoiceResponse;
  invoiceDetails: InvoiceDetails | undefined;
}

const invoiceApi = new InvoiceApi();

export const getPendingInvoices = createAsyncThunk(
  'invoice/authUser',
  async () => {
    const response = await invoiceApi.getInvoicesByUser();
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        status: response.status,
        data: respJson.data,
      };
    } else {
      return {
        status: response.status,
        data: [],
      };
    }
  }
);

export const getRecentOrdersProductsByUser = createAsyncThunk(
  'invoice/getRecentOrdersProductsByUser',
  async () => {
    const response = await invoiceApi.getRecentOrdersProductsByUser();
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        data: respJson.data,
        status: response.status,
      };
    } else {
      return {
        data: [],
        status: response.status,
      };
    }
  }
);

export const preInvoice = createAsyncThunk('invoice/preInvoice', async () => {
  const response = await invoiceApi.preInvoice();
  if (response.status === HTTP_STATUS.OK) {
    const respJson = await response.json();
    return {
      data: respJson.data,
      status: response.status,
    };
  }
});

export const invoiceDetails = createAsyncThunk(
  'invoice/invoiceDetails',
  async (orderId: number) => {
    const response = await invoiceApi.getInvoiceDetailsById(orderId);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        data: respJson.data,
        status: response.status,
      };
    }
  }
);

export const cancelInvoice = createAsyncThunk(
  'invoice/cancelInvoice',
  async (invoiceRequest: CancelInvoiceRequest) => {
    const response = await invoiceApi.cancelInvoice(invoiceRequest);
  }
);

const initialState: InvoiceState = {
  currentInvoices: [],
  recentOrdersByUserData: [],
  preInvoiceData: {},
  invoiceDetails: undefined,
};

const invoiceSlice = createSlice({
  name: 'invoice',
  initialState,
  reducers: {
    updateRecentOrdersProductsByUser: (
      state,
      action: PayloadAction<GroupedProductsInBusinessCategoryFormated[]>
    ) => {
      state.recentOrdersByUserData = action.payload;
    },
    setInvoiceDetails: (state, action: PayloadAction<InvoiceDetails>) => {
      state.invoiceDetails = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getPendingInvoices.fulfilled, (state, action) => {
      state.currentInvoices = action.payload?.data.content;
    });
    builder.addCase(
      getRecentOrdersProductsByUser.fulfilled,
      (state, action) => {
        state.recentOrdersByUserData = action.payload?.data.content;
      }
    );
    builder.addCase(preInvoice.fulfilled, (state, action) => {
      state.preInvoiceData = action.payload?.data;
    });
    builder.addCase(invoiceDetails.fulfilled, (state, action) => {
      state.invoiceDetails = action.payload?.data;
    });
  },
});

export const {
  updateRecentOrdersProductsByUser,
  setInvoiceDetails,
} = invoiceSlice.actions;

export default invoiceSlice.reducer;
