import {FocusStyleParam} from './../../../../domain/entity/structure/signup.model';
import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import UserApi from '../../../api/UserApi';
import {KeyValue} from '../../../../domain/entity/structure/common.model';
import {UpdatePasswordRequest} from '../../../../domain/entity/structure/update-password.model';
import {HTTP_STATUS} from '../../../constants/api.constants';

export interface UpdatePasswordState {
  password: string;
  confirmPassword: string;
  passwordIconBg: string | undefined;
  confirmPasswordIconBg: string | undefined;
  pendingLogin: boolean;
  errorValidation: string | undefined;
  passwordWasUpdated: boolean;
  passwordUpdateMessage: string | undefined;
}

const userApi = new UserApi();

export const updatePasswordUser = createAsyncThunk(
  'updatePassword/updatePassword',
  async (userPasswordRequest: UpdatePasswordRequest) => {
    const response = await userApi.updatePassword(userPasswordRequest);
    const responseJson = await response.json();
    return {
      responseData: {
        passwordWasUpdated: response.status === HTTP_STATUS.OK ? true : false,
        message: responseJson.data,
      },
    };
  }
);

const initialState: UpdatePasswordState = {
  password: '',
  confirmPassword: '',
  pendingLogin: false,
  passwordIconBg: undefined,
  confirmPasswordIconBg: undefined,
  errorValidation: undefined,
  passwordWasUpdated: false,
  passwordUpdateMessage: undefined,
};

const updatePasswordSlice = createSlice({
  name: 'UpdatePassword',
  initialState,
  reducers: {
    setFocusInputStyle: (
      state: any,
      action: PayloadAction<FocusStyleParam>
    ) => {
      state[action.payload.input] = action.payload.iconBg;
    },
    setErrorValidation: (state, action: PayloadAction<string | undefined>) => {
      state.errorValidation = action.payload;
    },
    setInputValue: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(updatePasswordUser.pending, (state) => {
      state.pendingLogin = true;
    });
    builder.addCase(updatePasswordUser.fulfilled, (state, action) => {
      state.pendingLogin = false;
      state.passwordWasUpdated = action.payload.responseData.passwordWasUpdated;
      state.passwordUpdateMessage = action.payload.responseData.message;
    });
  },
});

export const {
  setErrorValidation,
  setFocusInputStyle,
  setInputValue,
} = updatePasswordSlice.actions;

export default updatePasswordSlice.reducer;
