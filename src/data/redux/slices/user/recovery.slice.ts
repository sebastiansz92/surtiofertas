import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {KeyValue} from '../../../../domain/entity/structure/common.model';
import {
  FocusStyleParam,
  RecoveryRequest,
} from '../../../../domain/entity/structure/recovery.model';
import UserApi from '../../../api/UserApi';
import {HTTP_STATUS} from '../../../constants/api.constants';

export interface RecoveryState {
  email: string;
  emailIconBg: string | undefined;
  errorValidation: string | undefined;
  recoveryMessageSend: boolean;
  recoveryMessage: string | undefined;
}

const userApi = new UserApi();

export const recoveryUser = createAsyncThunk(
  'user-recovery/recovery',
  async (user: RecoveryRequest) => {
    const response = await userApi.recoveryPassword(user);
    const respJson = await response.json();
    return {responseData: respJson, status: response.status};
  }
);

const initialState: RecoveryState = {
  email: '',
  emailIconBg: undefined,
  errorValidation: undefined,
  recoveryMessageSend: false,
  recoveryMessage: undefined,
};

const recoverySlice = createSlice({
  name: 'user-recovery',
  initialState,
  reducers: {
    setFocusStyle: (state: any, action: PayloadAction<FocusStyleParam>) => {
      state[action.payload.input] = action.payload.iconBg;
    },
    setErrorValidation: (state, action: PayloadAction<string | undefined>) => {
      state.errorValidation = action.payload;
    },
    setValueInput: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(recoveryUser.fulfilled, (state, action) => {
      if (action.payload.status === HTTP_STATUS.NOT_FOUND) {
        state.errorValidation = action.payload.responseData.data;
      } else if (action.payload.status === HTTP_STATUS.OK) {
        state.recoveryMessageSend = true;
        state.recoveryMessage = action.payload.responseData.data;
      }
    });
  },
});

export const {
  setErrorValidation,
  setFocusStyle,
  setValueInput,
} = recoverySlice.actions;

export default recoverySlice.reducer;
