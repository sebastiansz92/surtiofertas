import {Dispatch} from 'react';
import {RecoveryRequest} from '../../../../domain/entity/structure/recovery.model';
import {RecoveryInterface} from '../../../../domain/interactors/interfaces/recovery.interface';
import {
  setErrorValidation,
  setValueInput,
  setFocusStyle,
  recoveryUser,
} from './recovery.slice';

export function createReduxRecoveryInterface(
  dispatch: Dispatch<{}>
): RecoveryInterface {
  return {
    setFocusStyle(iconBg: string, input: string): void {
      dispatch(setFocusStyle({iconBg, input}));
    },
    recoveryUser(user: RecoveryRequest): void {
      dispatch(recoveryUser(user));
    },
    setErrorValidation(error: string | undefined) {
      dispatch(setErrorValidation(error));
    },
    setValueInput(key: string, value: string): void {
      dispatch(setValueInput({key, value}));
    },
  };
}
