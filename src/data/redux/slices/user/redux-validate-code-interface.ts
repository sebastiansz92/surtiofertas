import {Dispatch} from 'react';
import {RecoveryRequest} from '../../../../domain/entity/structure/recovery.model';
import {ValidateCodeRequest} from '../../../../domain/entity/structure/validate-code.model';
import {ValidateCodeInterface} from '../../../../domain/interactors/interfaces/validatecode.interface';
import {recoveryUser} from './recovery.slice';
import {
  asignNextPinInputFocus,
  resetPinValues,
  setErrorValidation,
  setFocusStyle,
  setRecoveryResponse,
  setValueInput,
  validateCodeUser,
  resetPinTarget,
} from './validate-code.slice';

export function createReduxValidateCodeInterface(
  dispatch: Dispatch<{}>
): ValidateCodeInterface {
  return {
    setFocusStyle(iconBg: string, input: string): void {
      dispatch(setFocusStyle({iconBg, input}));
    },
    validateCodeUser(code: ValidateCodeRequest): void {
      dispatch(validateCodeUser(code));
    },
    setErrorValidation(error: string | undefined) {
      dispatch(setErrorValidation(error));
      dispatch(setRecoveryResponse(undefined));
      dispatch(resetPinValues(''));
      dispatch(resetPinTarget(0));
    },
    setValueInput(key: string, value: string): void {
      dispatch(setValueInput({key, value}));
    },
    asignNextPinInputFocus(nextIndex: number, pinValueString: string): void {
      if (pinValueString.length === 4) {
        dispatch(setValueInput({key: 'pinValue', value: pinValueString}));
      } else {
        dispatch(asignNextPinInputFocus(nextIndex));
      }
    },
    recoveryUser(request: RecoveryRequest): void {
      dispatch(recoveryUser(request));
    },
  };
}
