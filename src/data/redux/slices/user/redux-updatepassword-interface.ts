import {Dispatch} from 'react';
import {UpdatePasswordRequest} from '../../../../domain/entity/structure/update-password.model';
import {UpdatePasswordInterface} from '../../../../domain/interactors/interfaces/updatepassword.interface';
import {
  updatePasswordUser,
  setErrorValidation,
  setInputValue,
  setFocusInputStyle,
} from './updatepassword.slice';

export function createReduxUpdatePasswordInterface(
  dispatch: Dispatch<{}>
): UpdatePasswordInterface {
  return {
    setFocusStyle(iconBg: string, input: string): void {
      dispatch(setFocusInputStyle({iconBg, input}));
    },
    updatePasswordUser(userPasswordRequest: UpdatePasswordRequest): void {
      dispatch(updatePasswordUser(userPasswordRequest));
    },
    setErrorValidation(error: string | undefined) {
      dispatch(setErrorValidation(error));
    },
    setInputValue(key: string, value: string): void {
      dispatch(setInputValue({key, value}));
    },
  };
}
