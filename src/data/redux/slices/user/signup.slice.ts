import {HTTP_STATUS} from './../../../constants/api.constants';
import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {KeyValue} from '../../../../domain/entity/structure/common.model';
import {
  FocusStyleParam,
  SignupRequest,
} from '../../../../domain/entity/structure/signup.model';
import UserApi from '../../../api/UserApi';
import StorageData from '../../../StorageData';

export interface SignupState {
  name: string;
  nameIconBg: string | undefined;
  phone: string;
  phoneIconBg: string | undefined;
  email: string;
  emailIconBg: string | undefined;
  password: string;
  passwordIconBg: string | undefined;
  passwordConfirm: string;
  passwordConfirmIconBg: string | undefined;
  pendingSignup: boolean;
  errorValidation: string | undefined;
  userLogin: boolean;
}

const userApi = new UserApi();
const asyncStorage = new StorageData();

export const signupUser = createAsyncThunk(
  'user-signup/signup',
  async (user: SignupRequest) => {
    const response = await userApi.signupUser(user);
    let respJson = await response.json();
    if (response.status === HTTP_STATUS.OK) {
      await asyncStorage.storeData(
        'userToken',
        response.headers.get('Authorization')
      );
    }
    return {responseData: respJson, status: response.status};
  }
);

const initialState: SignupState = {
  name: '',
  nameIconBg: undefined,
  phone: '',
  phoneIconBg: undefined,
  email: '',
  emailIconBg: undefined,
  password: '',
  passwordIconBg: undefined,
  passwordConfirm: '',
  passwordConfirmIconBg: undefined,
  pendingSignup: false,
  errorValidation: undefined,
  userLogin: false,
};

const signupSlice = createSlice({
  name: 'user-signup',
  initialState,
  reducers: {
    setFocusStyle: (state: any, action: PayloadAction<FocusStyleParam>) => {
      state[action.payload.input] = action.payload.iconBg;
    },
    setErrorValidation: (state, action: PayloadAction<string | undefined>) => {
      state.errorValidation = action.payload;
    },
    setValueInput: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(signupUser.pending, (state) => {
      state.pendingSignup = true;
    });
    builder.addCase(signupUser.fulfilled, (state, action) => {
      state.pendingSignup = false;
      if (action.payload.status === HTTP_STATUS.OK) {
        state.userLogin = true;
      } else if (action.payload.status === HTTP_STATUS.CONFLICT) {
        state.errorValidation = action.payload.responseData.data;
      }
    });
  },
});

export const {
  setErrorValidation,
  setFocusStyle,
  setValueInput,
} = signupSlice.actions;

export default signupSlice.reducer;
