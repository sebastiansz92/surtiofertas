import {Dispatch} from 'react';
import {LoginRequest} from '../../../../domain/entity/structure/user.model';
import {LoginInterface} from '../../../../domain/interactors/interfaces/user/login.interface';
import {
  loginUser,
  setErrorValidation,
  setInputValue,
  setFocusInputStyle,
  setUserToken,
} from './login.slice';

export function createReduxLoginInterface(
  dispatch: Dispatch<{}>
): LoginInterface {
  return {
    setFocusStyle(iconBg: string, input: string): void {
      dispatch(setFocusInputStyle({iconBg, input}));
    },
    loginUser(user: LoginRequest): void {
      dispatch(loginUser(user));
    },
    setErrorValidation(error: string | undefined) {
      dispatch(setErrorValidation(error));
    },
    setInputValue(key: string, value: string): void {
      dispatch(setInputValue({key, value}));
    },

    setUserToken(token: string | undefined): void {
      dispatch(setUserToken(token));
    },
  };
}
