import {Dispatch} from 'react';
import {SignupRequest} from '../../../../domain/entity/structure/signup.model';
import {SingupInterface} from '../../../../domain/interactors/interfaces/signup.interface';
import {
  setFocusStyle,
  setErrorValidation,
  setValueInput,
  signupUser,
} from '../user/signup.slice';

export function createReduxSignupInterface(
  dispatch: Dispatch<{}>
): SingupInterface {
  return {
    setFocusStyle(iconBg: string, input: string) {
      dispatch(setFocusStyle({iconBg, input}));
    },
    signupUser(user: SignupRequest): void {
      dispatch(signupUser(user));
    },
    setErrorValidation(error: string | undefined) {
      dispatch(setErrorValidation(error));
    },
    setValueInput(key: string, value: string): void {
      dispatch(setValueInput({key, value}));
    },
  };
}
