import {HTTP_STATUS} from './../../../constants/api.constants';
import {FocusStyleParam} from './../../../../domain/entity/structure/signup.model';
import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {LoginRequest} from '../../../../domain/entity/structure/user.model';
import UserApi from '../../../api/UserApi';
import {KeyValue} from '../../../../domain/entity/structure/common.model';
import StorageData from '../../../StorageData';
import {ERROR} from '../../../constants/messages.responses';

export interface LoginState {
  email: string;
  emailIconBg: string | undefined;
  password: string;
  passwordIconBg: string | undefined;
  pendingLogin: boolean;
  errorValidation: string | undefined;
  userToken: string | undefined;
  reloadUserInfo: boolean | undefined;
}

const userApi = new UserApi();
const asyncStorage = new StorageData();

export const loginUser = createAsyncThunk(
  'login/authUser',
  async (user: LoginRequest) => {
    const response = await userApi.loginUser(user);
    const responseData: any = {
      message: null,
      key: null,
    };
    let message: string | undefined;
    if (response.status === HTTP_STATUS.OK) {
      await asyncStorage.storeData(
        'userToken',
        response.headers.get('Authorization')
      );
      message = response.headers.get('Authorization') || undefined;
      responseData.message = message;
      responseData.key = 'userToken';
    } else if (
      response.status === HTTP_STATUS.UNAUTHORIZED ||
      response.status === HTTP_STATUS.FORBIDDEN
    ) {
      message = ERROR.INCORRECT_LOGIN;
      responseData.message = message;
      responseData.key = 'errorValidation';
    }
    return {
      responseData: responseData,
      status: response.status,
    };
  }
);

const initialState: LoginState = {
  email: '',
  password: '',
  pendingLogin: false,
  emailIconBg: undefined,
  passwordIconBg: undefined,
  errorValidation: undefined,
  userToken: undefined,
  reloadUserInfo: undefined,
};

const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    setFocusInputStyle: (
      state: any,
      action: PayloadAction<FocusStyleParam>
    ) => {
      state[action.payload.input] = action.payload.iconBg;
    },
    setErrorValidation: (state, action: PayloadAction<string | undefined>) => {
      state.errorValidation = action.payload;
    },
    setInputValue: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
    setUserToken: (state, action: PayloadAction<string | undefined>) => {
      state.userToken = action.payload;
    },
    reloadUserinfoAction: (state, action: PayloadAction<boolean>) => {
      state.reloadUserInfo = action.payload;
    },
    resetReloadUserInfoAction: (state, action: any) => {
      state.reloadUserInfo = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loginUser.pending, (state) => {
      state.pendingLogin = true;
    });
    builder.addCase(loginUser.fulfilled, (state: any, action) => {
      state.pendingLogin = false;
      state[action.payload.responseData.key] =
        action.payload.responseData.message;
    });
  },
});

export const {
  setErrorValidation,
  setFocusInputStyle,
  setInputValue,
  setUserToken,
  reloadUserinfoAction,
  resetReloadUserInfoAction,
} = loginSlice.actions;

export default loginSlice.reducer;
