import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {KeyValue} from '../../../../domain/entity/structure/common.model';
import {
  FocusStyleParam,
  ResponseOTPValidate,
  ValidateCodeRequest,
} from '../../../../domain/entity/structure/validate-code.model';
import UserApi from '../../../api/UserApi';

export interface validateCodeState {
  code: string;
  errorValidation: string | undefined;
  pin0Value: string;
  pin1Value: string;
  pin2Value: string;
  pin3Value: string;
  pinTarget: number;
  pinValue: string;
  recoveryResponse: ResponseOTPValidate | undefined;
}

const userApi = new UserApi();

export const validateCodeUser = createAsyncThunk(
  'user-validateCode/validateCode',
  async (OtpCode: ValidateCodeRequest) => {
    const response = await userApi.validateOTPCode(OtpCode);
    const respJson = await response.json();
    return {
      responseData: {
        message: respJson.data,
        status: response.status,
        userId: response?.headers?.get('userid') || undefined,
      } as ResponseOTPValidate,
    };
  }
);

const initialState: validateCodeState = {
  code: '',
  errorValidation: undefined,
  pin0Value: '',
  pin1Value: '',
  pin2Value: '',
  pin3Value: '',
  pinTarget: 0,
  pinValue: '',
  recoveryResponse: undefined,
};

const validateCodeSlice = createSlice({
  name: 'user-validateCode',
  initialState,
  reducers: {
    setFocusStyle: (state: any, action: PayloadAction<FocusStyleParam>) => {
      state[action.payload.input] = action.payload.iconBg;
    },
    setErrorValidation: (state, action: PayloadAction<string | undefined>) => {
      state.errorValidation = action.payload;
    },
    setRecoveryResponse: (
      state,
      action: PayloadAction<ResponseOTPValidate | undefined>
    ) => {
      state.recoveryResponse = action.payload;
    },
    resetPinValues: (state, action: PayloadAction<string>) => {
      state.pin0Value = action.payload;
      state.pin1Value = action.payload;
      state.pin2Value = action.payload;
      state.pin3Value = action.payload;
      state.pinValue = action.payload;
    },
    resetPinTarget: (state, action: PayloadAction<number>) => {
      state.pinTarget = action.payload;
    },
    setValueInput: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
    asignNextPinInputFocus: (state: any, action: PayloadAction<number>) => {
      state.pinTarget = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(validateCodeUser.fulfilled, (state, action) => {
      state.recoveryResponse = action.payload.responseData;
    });
  },
});

export const {
  setErrorValidation,
  setFocusStyle,
  setValueInput,
  setRecoveryResponse,
  resetPinValues,
  resetPinTarget,
  asignNextPinInputFocus,
} = validateCodeSlice.actions;

export default validateCodeSlice.reducer;
