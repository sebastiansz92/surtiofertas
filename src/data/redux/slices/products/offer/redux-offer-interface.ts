import {Dispatch} from 'react';
import {OfferInterface} from '../../../../../domain/interactors/interfaces/product/offer.interface';
import {getMarketOffers} from './offer.slice';

export function createReduxOfferInterface(
  dispatch: Dispatch<{}>
): OfferInterface {
  return {
    getMarketOffers(): void {
      dispatch(getMarketOffers());
    },
  };
}
