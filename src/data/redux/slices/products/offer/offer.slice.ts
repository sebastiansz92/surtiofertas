import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {
  ReduxOfferReponse,
  OfferContent,
} from '../../../../../domain/entity/structure/offer.model';
import ProductsApi from '../../../../api/ProductApi';

const productsApi = new ProductsApi();

export interface OfferState {
  offerProducts: Array<OfferContent>;
  pendingOffer: boolean;
}

export const getMarketOffers = createAsyncThunk(
  'offer/getMarketOffers',
  async () => {
    const response = await productsApi.getCurrentOffers();
    const respJson = await response.json();
    return {
      responseData: respJson.data,
      status: response.status,
    } as ReduxOfferReponse;
  }
);

const initialState: OfferState = {
  offerProducts: [],
  pendingOffer: false,
};

const offerSlice = createSlice({
  name: 'offer',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getMarketOffers.pending, (state) => {
      state.pendingOffer = true;
    });
    builder.addCase(
      getMarketOffers.fulfilled,
      (state, action: PayloadAction<ReduxOfferReponse>) => {
        state.pendingOffer = false;
        state.offerProducts = action.payload.responseData.content;
      }
    );
  },
});
export default offerSlice.reducer;
