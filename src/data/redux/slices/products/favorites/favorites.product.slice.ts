import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AddOrRemoveFavoriteRequest} from '../../../../../domain/entity/structure/favorite-product.model';
import ProductsApi from '../../../../api/ProductApi';
import {HTTP_STATUS} from '../../../../constants/api.constants';

const productsApi = new ProductsApi();

export interface FavoriteProductsState {
  productTargetId: number | undefined;
  favoriteProductsByUser: Array<any>;
  recentOrdersByUser: Array<any>;
  isProductFavorite: boolean | undefined;
}

const initialState: FavoriteProductsState = {
  productTargetId: undefined,
  favoriteProductsByUser: [],
  recentOrdersByUser: [],
  isProductFavorite: undefined,
};

export const getFavoriteProductsByUser = createAsyncThunk(
  'favorite/getFavoriteProductsByUser',
  async () => {
    const response = await productsApi.getFavoriteProductsByUser();
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return respJson.data;
    }
  }
);

export const validateFavorite = createAsyncThunk(
  'favorite/validateFavorite',
  async (product: AddOrRemoveFavoriteRequest) => {
    const response = await productsApi.validateFavorite(product);
    if (response.status === HTTP_STATUS.INTERNAL_ERROR) {
      return false;
    } else {
      return response.status !== HTTP_STATUS.CONFLICT;
    }
  }
);

export const addOrRemoveFavorite = createAsyncThunk(
  'favorite/addOrRemoveFavorite',
  async (product: any) => {
    const response = await productsApi.addOrRemoveFavoriteProduct(
      product.product
    );
    if (response.status === HTTP_STATUS.OK) {
      return !product.isFavorite;
    }
  }
);

const favoriteProductSlice = createSlice({
  name: 'favorite',
  initialState,
  reducers: {
    setProductTarget: (state, action: PayloadAction<number | undefined>) => {
      state.productTargetId = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      validateFavorite.fulfilled,
      (state, action: PayloadAction<boolean | undefined>) => {
        state.isProductFavorite = action.payload;
      }
    );
    builder.addCase(addOrRemoveFavorite.fulfilled, (state, action) => {
      state.isProductFavorite = action.payload;
    });
    builder.addCase(getFavoriteProductsByUser.fulfilled, (state, action) => {
      state.favoriteProductsByUser = action.payload;
    });
  },
});

export const {setProductTarget} = favoriteProductSlice.actions;

export default favoriteProductSlice.reducer;
