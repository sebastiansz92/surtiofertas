import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {
  BusinessCategoryDataFormated,
  CategorySupperTabs,
  GroupedProductsInBusinessCategoryFormated,
} from '../../../../../domain/entity/structure/category-product.model';
import {KeyValue} from '../../../../../domain/entity/structure/common.model';
import ProductsApi from '../../../../api/ProductApi';
import {HTTP_STATUS} from '../../../../constants/api.constants';

const productsApi = new ProductsApi();

export interface CategoryProductsState {
  id: number;
  name: string;
  description: string;
  categoryimage64: string;
  categorySupperData: Array<any>;
  allCategoriesProducts: Array<any>;
  businessCategoryData: BusinessCategoryDataFormated[];
  businessTabsData: CategorySupperTabs[];
  scrollOffsetData: Array<any>;
  productsCategoryData: Array<any>;
  productsCategorySearchData: Array<any> | undefined;
  search: string;
  searchIconBg: string | undefined;
}

const initialState: CategoryProductsState = {
  id: 0,
  name: '',
  description: '',
  categoryimage64: '',
  categorySupperData: [],
  allCategoriesProducts: [],
  businessCategoryData: [],
  businessTabsData: [],
  scrollOffsetData: [],
  productsCategoryData: [],
  productsCategorySearchData: undefined,
  search: '',
  searchIconBg: undefined,
};

export const getProductsBySupperCategory = createAsyncThunk(
  'category/getProductsBySupperCategory',
  async () => {
    const response = await productsApi.getProductsBySupperCategory();
    const respJson = await response.json();
    return {
      responseData: respJson.data,
      status: response.status,
    };
  }
);

export const getAllCategoriesProduct = createAsyncThunk(
  'category/getAllCategoriesProduct',
  async () => {
    const response = await productsApi.getAllCategoriesProduct();
    const respJson = await response.json();
    return {
      responseData: respJson.data,
      status: response.status,
    };
  }
);

export const getProductsByCategory = createAsyncThunk(
  'category/getProductsByCategory',
  async (category: number) => {
    const response = await productsApi.getProductsByCategory(category);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        responseData: respJson.data,
        status: response.status,
      };
    } else {
      return {
        responseData: [],
        status: response.status,
      };
    }
  }
);

export const getProductsBySearch = createAsyncThunk(
  'category/getProductsBySearch',
  async (search: string) => {
    const response = await productsApi.getProductsBySearch(search);
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        responseData: respJson.data,
        status: response.status,
      };
    } else {
      return {
        responseData: [],
        status: response.status,
      };
    }
  }
);

const categoryProductSlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    updateTabsBusinessCategory: (
      state,
      action: PayloadAction<CategorySupperTabs[]>
    ) => {
      state.businessTabsData = action.payload;
    },

    updateOffsetSectionData: (state, action: PayloadAction<any[]>) => {
      state.scrollOffsetData = action.payload;
    },

    updateBusinessCategoryData: (
      state,
      action: PayloadAction<BusinessCategoryDataFormated[]>
    ) => {
      state.businessCategoryData = action.payload;
    },

    updateProductsByCategoryData: (
      state,
      action: PayloadAction<GroupedProductsInBusinessCategoryFormated[]>
    ) => {
      state.productsCategoryData = action.payload;
    },
    updateProductsBySearchData: (
      state,
      action: PayloadAction<GroupedProductsInBusinessCategoryFormated[]>
    ) => {
      state.productsCategorySearchData = action.payload;
    },
    setInputValue: (state: any, action: PayloadAction<KeyValue>) => {
      state[action.payload.key] = action.payload.value;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getProductsBySupperCategory.fulfilled, (state, action) => {
      state.categorySupperData = action.payload.responseData;
    });
    builder.addCase(getAllCategoriesProduct.fulfilled, (state, action) => {
      state.allCategoriesProducts = action.payload.responseData;
    });
  },
});

export const {
  updateTabsBusinessCategory,
  updateBusinessCategoryData,
  updateOffsetSectionData,
  updateProductsByCategoryData,
  updateProductsBySearchData,
  setInputValue,
} = categoryProductSlice.actions;

export default categoryProductSlice.reducer;
