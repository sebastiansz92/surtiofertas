import {Dispatch} from 'react';
import {
  BusinessCategoryDataFormated,
  CategorySupperTabs,
  GroupedProductsInBusinessCategoryFormated,
} from '../../../../../domain/entity/structure/category-product.model';
import {CategoryProductInterface} from '../../../../../domain/interactors/interfaces/product/category-product.interface';
import * as category from './category.product.slice';

export function createReduxCategoryProductInterface(
  dispatch: Dispatch<{}>
): CategoryProductInterface {
  return {
    async getProductsGroupedBySupperCategory(): Promise<any> {
      return dispatch(category.getProductsBySupperCategory());
    },

    updateTabsBusinessCategory(tabsData: CategorySupperTabs[]): void {
      dispatch(category.updateTabsBusinessCategory(tabsData));
    },

    updateCategorySupperData(
      businessCategoryData: BusinessCategoryDataFormated[]
    ): void {
      dispatch(category.updateBusinessCategoryData(businessCategoryData));
    },

    updateOffsetSectionData(offsetData: Array<any>): void {
      dispatch(category.updateOffsetSectionData(offsetData));
    },
    getAllCategoriesProduct(): void {
      dispatch(category.getAllCategoriesProduct());
    },

    async getProductsByCategory(categoryId: number): Promise<any> {
      return dispatch(category.getProductsByCategory(categoryId));
    },

    async getProductsBySearch(search: string): Promise<any> {
      return dispatch(category.getProductsBySearch(search));
    },

    updateProductsByCategory(
      productCategoryData: GroupedProductsInBusinessCategoryFormated[]
    ): void {
      dispatch(category.updateProductsByCategoryData(productCategoryData));
    },

    updateProductsBySearch(
      productCategorySearchData: GroupedProductsInBusinessCategoryFormated[]
    ): void {
      dispatch(category.updateProductsBySearchData(productCategorySearchData));
    },

    setInputValue(key: string, value: string): void {
      dispatch(category.setInputValue({key, value}));
    },
  };
}
