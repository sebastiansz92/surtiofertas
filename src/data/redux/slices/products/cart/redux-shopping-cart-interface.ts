import {Dispatch} from 'react';
import {
  AddCarProductRequestData,
  ShoppingCartDataState,
} from '../../../../../domain/entity/structure/category-product.model';
import {ShoppingCartInterface} from '../../../../../domain/interactors/interfaces/product/shopping-cart.interface';
import {
  addProductToCar,
  getProductsInCart,
  removeAllProductsInCar,
  removeProductInCart,
  setShopppinCartData,
} from './shoping-cart.slice';

export function createReduxShoppingCartInterface(
  dispatch: Dispatch<{}>
): ShoppingCartInterface {
  return {
    addProductToCar(request: AddCarProductRequestData): void {
      dispatch(addProductToCar(request));
    },

    async getAllProductsInCar(): Promise<any> {
      return dispatch(getProductsInCart());
    },

    setShopppinCartData(data: ShoppingCartDataState): void {
      dispatch(setShopppinCartData(data));
    },

    async removeProductInCar(productId: number): Promise<any> {
      return dispatch(removeProductInCart(productId));
    },

    async removeAllProductsInCar(): Promise<any> {
      return dispatch(removeAllProductsInCar());
    },
  };
}
