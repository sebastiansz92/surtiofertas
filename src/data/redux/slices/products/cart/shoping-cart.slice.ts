import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {
  AddCarProductRequestData,
  ShoppingCartDataState,
} from '../../../../../domain/entity/structure/category-product.model';
import {
  GenerateInvoiceRequest,
  GenerateInvoiceResponse,
} from '../../../../../domain/entity/structure/invoice.model';
import InvoiceApi from '../../../../api/InvoiceApi';
import ProductsApi from '../../../../api/ProductApi';
import {HTTP_STATUS} from '../../../../constants/api.constants';

const productsApi = new ProductsApi();
const invoiceApi = new InvoiceApi();

export interface ShoppingCartState {
  productWasAddedToCart: boolean | undefined;
  shoppingCartData: ShoppingCartDataState | undefined;
  showProductRemovedMessage: boolean;
  showProductsRemovedMessage: boolean;
  responseGenerateInvoice: GenerateInvoiceResponse | undefined;
}

const initialState: ShoppingCartState = {
  productWasAddedToCart: undefined,
  shoppingCartData: undefined,
  showProductRemovedMessage: false,
  showProductsRemovedMessage: false,
  responseGenerateInvoice: undefined,
};

export const addProductToCar = createAsyncThunk(
  'shopingcart/addProductToCar',
  async (request: AddCarProductRequestData) => {
    let productRequest = {
      ...request.product,
      quantity: request.quantity,
    };
    const response = await productsApi.addProductToCar(
      productRequest,
      request.rewrite || false
    );
    if (response.status === HTTP_STATUS.CREATED) {
      return {status: true};
    } else {
      return {status: false};
    }
  }
);

export const getProductsInCart = createAsyncThunk(
  'shopingcart/getProductsInCart',
  async () => {
    const response = await productsApi.getProductsInCart();
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      return {
        status: response.status,
        data: respJson.data,
      };
    } else {
      return null;
    }
  }
);

export const removeProductInCart = createAsyncThunk(
  'shopingcart/removeProductInCart',
  async (productId: number) => {
    const response = await productsApi.removeProductInCart(productId);
    if (response.status === HTTP_STATUS.NOT_CONTENT) {
      return true;
    } else {
      return false;
    }
  }
);

export const removeAllProductsInCar = createAsyncThunk(
  'shopingcart/removeAllProductsInCar',
  async () => {
    const response = await productsApi.removeAllProductsInCar();
    if (response.status === HTTP_STATUS.NOT_CONTENT) {
      return true;
    } else {
      return false;
    }
  }
);

export const generateInvoice = createAsyncThunk(
  'shopingcart/generateInvoice',
  async (invoiceRequest: GenerateInvoiceRequest) => {
    const response = await invoiceApi.generateInvoice(invoiceRequest);
    return {
      responseData: await response.json(),
      status: response.status,
    };
  }
);

const shopingCartSlice = createSlice({
  name: 'shopingcart',
  initialState,
  reducers: {
    resetShopingCartState: () => initialState,
    setShopppinCartData: (
      state,
      action: PayloadAction<ShoppingCartDataState>
    ) => {
      state.shoppingCartData = action.payload;
    },
    resetShowMessageRemoved: (state, action) => {
      state.showProductRemovedMessage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      addProductToCar.fulfilled,
      (state, action: PayloadAction<{status: boolean}>) => {
        state.productWasAddedToCart = action.payload.status;
      }
    );
    builder.addCase(
      removeProductInCart.fulfilled,
      (state, action: PayloadAction<boolean>) => {
        state.showProductRemovedMessage = action.payload;
      }
    );
    builder.addCase(
      removeAllProductsInCar.fulfilled,
      (state, action: PayloadAction<boolean>) => {
        state.showProductsRemovedMessage = action.payload;
      }
    );
    builder.addCase(generateInvoice.fulfilled, (state, action) => {
      state.responseGenerateInvoice = action.payload;
    });
  },
});

export const {
  resetShopingCartState,
  setShopppinCartData,
  resetShowMessageRemoved,
} = shopingCartSlice.actions;

export default shopingCartSlice.reducer;
