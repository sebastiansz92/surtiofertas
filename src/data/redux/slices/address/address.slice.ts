import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {LatLng} from 'react-native-maps';
import {
  AddressDecodeResponse,
  AddressResponse,
  AddressValues,
} from '../../../../domain/entity/structure/address.model';
import AddressApi from '../../../address/AddressApi';
import MapsApi from '../../../address/MapsApi';
import {HTTP_STATUS} from '../../../constants/api.constants';

const mapsApi = new MapsApi();
const addressApi = new AddressApi();

export interface AddressState {
  addresses: Array<any>;
  currentAddressDecode: AddressValues | undefined;
  currentAddressByUser: AddressValues | undefined;
  addressList: AddressValues[];
  defaultAddressWillUpdated: boolean | undefined;
  addressCreatedOrUpdated: boolean | undefined;
  errorValidation: string | undefined;
  addressUpdated: boolean;
}

const setAddressResponse = (dataResponse: Array<any>) => {
  return dataResponse.map((elm) => {
    return {
      address: elm.direction,
      position: {
        latitude: elm.coordinates.coordinates.coordinates[1],
        longitude: elm.coordinates.coordinates.coordinates[0],
      },
      id: elm.id,
      isDefault: elm.isDefault,
      name: elm.name,
      details: elm.details,
    } as AddressValues;
  });
};

export const getAddressByCoords = createAsyncThunk(
  'address/getAddressByCoords',
  async (position: LatLng) => {
    const response = await mapsApi.getAddressByCoords(position);
    const respJson = await response.json();
    return {
      responseData: {
        address: respJson.results[0].formatted_address,
        position: position,
      },
      status: response.status,
    } as AddressDecodeResponse;
  }
);
export const setDefaultAddressByUser = createAsyncThunk(
  'address/setDefaultAddressByUser',
  async (addressId: number) => {
    const response = await addressApi.setDefaultAddressByUser(addressId);
    return response.status === HTTP_STATUS.NOT_CONTENT;
  }
);

export const disabledAddress = createAsyncThunk(
  'address/disableAddressbyUser',
  async (addressId: number) => {
    const response = await addressApi.disableAddressByUser(addressId);
    const responseUserAddress = await addressApi.getAddressesByUser();
    if (responseUserAddress.status === HTTP_STATUS.OK) {
      const respJson = await responseUserAddress.json();
      const dataResponse = respJson.data as Array<any>;
      return {
        isDelete: response.status === HTTP_STATUS.NOT_CONTENT,
        addresList: setAddressResponse(dataResponse),
      };
    }
  }
);

export const getDefaultAddressByUser = createAsyncThunk(
  'address/getDefaultAddressByUser',
  async () => {
    const response = await addressApi.getDefaultUserAddress();
    const respJson = await response.json();
    const dataResponse = respJson.data;
    if (response.status === HTTP_STATUS.OK) {
      return {
        responseData: {
          address: dataResponse.direction,
          position: {
            latitude: dataResponse.coordinates.coordinates.coordinates[1],
            longitude: dataResponse.coordinates.coordinates.coordinates[0],
          },
        },
        status: response.status,
      } as AddressResponse;
    } else {
      return {
        responseData: {},
        status: response.status,
      } as AddressResponse;
    }
  }
);

export const updateAddress = createAsyncThunk(
  'address/updateAddress',
  async (address: AddressValues) => {
    const response = address.id
      ? await addressApi.updateAddressByUser(address)
      : await addressApi.AddAddressByUser(address);
    if (response.status !== HTTP_STATUS.CREATED) {
      const respJson = await response.json();
      return {
        status: response.status,
        data: respJson,
      };
    } else {
      return {
        status: response.status,
        data: response.status === HTTP_STATUS.CREATED,
      };
    }
  }
);

export const getAddressesByUser = createAsyncThunk(
  'address/getAddressesByUser',
  async () => {
    const response = await addressApi.getAddressesByUser();
    if (response.status === HTTP_STATUS.OK) {
      const respJson = await response.json();
      const dataResponse = respJson.data as Array<any>;
      return {
        responseData: setAddressResponse(dataResponse),
      };
    }
  }
);

const initialState: AddressState = {
  addresses: [],
  currentAddressDecode: undefined,
  currentAddressByUser: undefined,
  addressList: [],
  defaultAddressWillUpdated: undefined,
  addressCreatedOrUpdated: undefined,
  errorValidation: undefined,
  addressUpdated: false,
};

const addressSlice = createSlice({
  name: 'address',
  initialState,
  reducers: {
    resetCurrentState: () => initialState,
    resetErrorValidationState: (state, action) => {
      state.errorValidation = action.payload;
    },
    setErrorValidation: (state, action) => {
      state.errorValidation = action.payload;
    },
    resetAdressUpdateState: (state, action) => {
      state.addressCreatedOrUpdated = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      getAddressByCoords.fulfilled,
      (state, action: PayloadAction<AddressDecodeResponse>) => {
        state.currentAddressDecode = action.payload.responseData;
      }
    );
    builder.addCase(
      getDefaultAddressByUser.fulfilled,
      (state, action: PayloadAction<AddressResponse>) => {
        state.currentAddressByUser = action.payload.responseData;
        state.addressUpdated = true;
      }
    );
    builder.addCase(getAddressesByUser.fulfilled, (state, action) => {
      if (action?.payload?.responseData) {
        state.addressList = action.payload.responseData;
      }
    });
    builder.addCase(
      setDefaultAddressByUser.fulfilled,
      (state, action: PayloadAction<boolean>) => {
        state.defaultAddressWillUpdated = action.payload;
      }
    );
    builder.addCase(updateAddress.fulfilled, (state, action) => {
      if (
        action.payload.status === HTTP_STATUS.CREATED ||
        action.payload.status === HTTP_STATUS.OK
      ) {
        state.addressCreatedOrUpdated = action.payload.data;
      } else {
        state.errorValidation = action.payload.data.data;
      }
    });
    builder.addCase(disabledAddress.fulfilled, (state, action) => {
      state.addressCreatedOrUpdated = action?.payload?.isDelete;
      state.addressList = action?.payload?.addresList || [];
    });
  },
});

export const {
  resetCurrentState,
  resetErrorValidationState,
  setErrorValidation,
  resetAdressUpdateState,
} = addressSlice.actions;

export default addressSlice.reducer;
