import {Dispatch} from 'react';
import {LatLng} from 'react-native-maps';
import {AddressValues} from '../../../../domain/entity/structure/address.model';
import {AddressInterface} from '../../../../domain/interactors/interfaces/address/address.interface';
import {
  getAddressByCoords,
  getAddressesByUser,
  resetCurrentState,
  getDefaultAddressByUser,
  setDefaultAddressByUser,
  updateAddress,
  setErrorValidation,
  resetErrorValidationState,
  disabledAddress,
} from './address.slice';

export function createReduxAddressInterface(
  dispatch: Dispatch<{}>
): AddressInterface {
  return {
    getDefaultUserAddress(): void {
      dispatch(getDefaultAddressByUser());
    },
    getAddressesByUser(): void {
      dispatch(getAddressesByUser());
    },
    setDefaultAddress(position: LatLng): void {
      dispatch(getAddressByCoords(position));
    },
    setDefaultUserAddress(addressId: number): void {
      dispatch(setDefaultAddressByUser(addressId));
    },
    resetCurrentState(): void {
      dispatch(resetCurrentState());
    },
    resetErrorValidationState(): void {
      dispatch(resetErrorValidationState(undefined));
    },
    updateAddress(address: AddressValues): void {
      dispatch(updateAddress(address));
    },
    setErrorValidation(error: string): void {
      dispatch(setErrorValidation(error));
    },

    disabledAddress(id:number):void{
      dispatch(disabledAddress(id));
    }
  };
}
