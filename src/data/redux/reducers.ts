import loginSlice, {LoginState} from './slices/user/login.slice';
import {combineReducers} from '@reduxjs/toolkit';
import welcomeSlice from './slices/welcome.slice';
import signupSlice, {SignupState} from './slices/user/signup.slice';
import recoverySlice, {RecoveryState} from './slices/user/recovery.slice';
import validateCodeSlice, {
  validateCodeState,
} from './slices/user/validate-code.slice';
import offerSlice, {OfferState} from './slices/products/offer/offer.slice';
import updatepasswordSlice, {
  UpdatePasswordState,
} from './slices/user/updatepassword.slice';
import addressSlice, {AddressState} from './slices/address/address.slice';
import categoryProductSlice, {
  CategoryProductsState,
} from './slices/products/category/category.product.slice';
import layoutSlice, {LayoutState} from './slices/layout.slice';
import favoritesProductSlice, {
  FavoriteProductsState,
} from './slices/products/favorites/favorites.product.slice';
import shopingCartSlice, {
  ShoppingCartState,
} from './slices/products/cart/shoping-cart.slice';

import invoiceSlice, {InvoiceState} from './slices/invoice/invoice.slice';

export type StoreState = {
  login: LoginState;
  signup: SignupState;
  recovery: RecoveryState;
  validateCode: validateCodeState;
  offer: OfferState;
  updatePassword: UpdatePasswordState;
  address: AddressState;
  categoryProduct: CategoryProductsState;
  layout: LayoutState;
  favorite: FavoriteProductsState;
  shoppingCart: ShoppingCartState;
  invoice: InvoiceState;
};

const rootReducer = combineReducers({
  welcome: welcomeSlice,
  login: loginSlice,
  signup: signupSlice,
  recovery: recoverySlice,
  validateCode: validateCodeSlice,
  offer: offerSlice,
  updatePassword: updatepasswordSlice,
  address: addressSlice,
  categoryProduct: categoryProductSlice,
  layout: layoutSlice,
  favorite: favoritesProductSlice,
  shoppingCart: shopingCartSlice,
  invoice: invoiceSlice,
});
export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
