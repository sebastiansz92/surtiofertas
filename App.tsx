import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {Provider} from 'react-redux';
import store from './src/data/redux/store';
import {createStackNavigator} from '@react-navigation/stack';
import RootScreen from './src/presentation/views/screens/RootScreen';
import DialogComponent from './src/presentation/views/components/DialogComponent';
import {Platform} from 'react-native';
import LoaderComponent from './src/presentation/views/components/Loader.component';

// import StorageData from './src/data/async-storage';
// const asyncStorage = new StorageData();
// asyncStorage.removeItem('userToken');

const App = () => {
  const Stack = createStackNavigator();

  return (
    <>
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
              cardStyle: {backgroundColor: 'transparent'},
              cardOverlayEnabled: true,
              cardStyleInterpolator: ({current: {progress}}) => ({
                cardStyle: {
                  opacity: progress.interpolate({
                    inputRange: [0, 0.5, 0.9, 1],
                    outputRange: [0, 0.25, 0.7, 1],
                  }),
                },
                overlayStyle: {
                  opacity: progress.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 0.5],
                    extrapolate: 'clamp',
                  }),
                },
              }),
            }}
            headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
            mode="modal">
            <Stack.Screen
              name="Root"
              options={{headerShown: false}}
              component={RootScreen}
            />
            <Stack.Screen
              name="DialogModal"
              options={{headerShown: false}}
              component={DialogComponent}
            />
          </Stack.Navigator>
        </NavigationContainer>
        <LoaderComponent />
      </Provider>
    </>
  );
};

export default App;
