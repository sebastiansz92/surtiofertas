module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  endOfLine: "auto",
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: "es5",
};
